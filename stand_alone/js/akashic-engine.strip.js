require = function() {
    function r(e, n, t) {
        function o(i, f) {
            if (!n[i]) {
                if (!e[i]) {
                    var c = "function" == typeof require && require;
                    if (!f && c) return c(i, !0);
                    if (u) return u(i, !0);
                    var a = new Error("Cannot find module '" + i + "'");
                    throw a.code = "MODULE_NOT_FOUND", a;
                }
                var p = n[i] = {
                    exports: {}
                };
                e[i][0].call(p.exports, function(r) {
                    var n = e[i][1][r];
                    return o(n || r);
                }, p, p.exports, r, e, n, t);
            }
            return n[i].exports;
        }
        for (var u = "function" == typeof require && require, i = 0; i < t.length; i++) o(t[i]);
        return o;
    }
    return r;
}()({
    "@akashic/akashic-engine": [ function(require, module, exports) {
        (function() {
            "use strict";
            var g, __extends = this && this.__extends || function(d, b) {
                function __() {
                    this.constructor = d;
                }
                for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
                d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
            };
            !function(g) {
                var AssetLoadErrorType;
                !function(AssetLoadErrorType) {
                    AssetLoadErrorType[AssetLoadErrorType.Unspecified = 0] = "Unspecified", AssetLoadErrorType[AssetLoadErrorType.RetryLimitExceeded = 1] = "RetryLimitExceeded", 
                    AssetLoadErrorType[AssetLoadErrorType.NetworkError = 2] = "NetworkError", AssetLoadErrorType[AssetLoadErrorType.ClientError = 3] = "ClientError", 
                    AssetLoadErrorType[AssetLoadErrorType.ServerError = 4] = "ServerError";
                }(AssetLoadErrorType = g.AssetLoadErrorType || (g.AssetLoadErrorType = {}));
            }(g || (g = {}));
            var g;
            !function(g) {
                var ExceptionFactory;
                !function(ExceptionFactory) {
                    function createAssertionError(message, cause) {
                        var e = new Error(message);
                        return e.name = "AssertionError", e.cause = cause, e;
                    }
                    function createTypeMismatchError(methodName, expected, actual, cause) {
                        var message = "Type mismatch on " + methodName + ", expected type is " + expected;
                        if (arguments.length > 2) try {
                            var actualString;
                            actualString = actual && actual.constructor && actual.constructor.name ? actual.constructor.name : typeof actual, 
                            message += ", actual type is " + (actualString.length > 40 ? actualString.substr(0, 40) : actualString);
                        } catch (ex) {}
                        message += ".";
                        var e = new Error(message);
                        return e.name = "TypeMismatchError", e.cause = cause, e.expected = expected, e.actual = actual, 
                        e;
                    }
                    function createAssetLoadError(message, retriable, type, cause) {
                        void 0 === retriable && (retriable = !0), void 0 === type && (type = g.AssetLoadErrorType.Unspecified);
                        var e = new Error(message);
                        return e.name = "AssetLoadError", e.cause = cause, e.retriable = retriable, e.type = type, 
                        e;
                    }
                    ExceptionFactory.createAssertionError = createAssertionError, ExceptionFactory.createTypeMismatchError = createTypeMismatchError, 
                    ExceptionFactory.createAssetLoadError = createAssetLoadError;
                }(ExceptionFactory = g.ExceptionFactory || (g.ExceptionFactory = {}));
            }(g || (g = {}));
            var g;
            !function(g) {
                var ResourceFactory = function() {
                    function ResourceFactory() {}
                    return ResourceFactory.prototype.createSurfaceAtlas = function(width, height) {
                        return new g.SurfaceAtlas(this.createSurface(width, height));
                    }, ResourceFactory.prototype.createTrimmedSurface = function(targetSurface, targetArea) {
                        var area = targetArea || {
                            x: 0,
                            y: 0,
                            width: targetSurface.width,
                            height: targetSurface.height
                        }, surface = this.createSurface(area.width, area.height), renderer = surface.renderer();
                        return renderer.begin(), renderer.drawImage(targetSurface, area.x, area.y, area.width, area.height, 0, 0), 
                        renderer.end(), surface;
                    }, ResourceFactory;
                }();
                g.ResourceFactory = ResourceFactory;
            }(g || (g = {}));
            var g;
            !function(g) {
                var RequireCachedValue = function() {
                    function RequireCachedValue(value) {
                        this._value = value;
                    }
                    return RequireCachedValue.prototype._cachedValue = function() {
                        return this._value;
                    }, RequireCachedValue;
                }();
                g.RequireCachedValue = RequireCachedValue;
            }(g || (g = {}));
            var g;
            !function(g) {
                var RandomGenerator = function() {
                    function RandomGenerator(seed) {
                        this.seed = seed, this[0] = this;
                    }
                    return RandomGenerator;
                }();
                g.RandomGenerator = RandomGenerator;
            }(g || (g = {}));
            var g;
            !function(g) {
                var Asset = function() {
                    function Asset(id, path) {
                        this.id = id, this.originalPath = path, this.path = this._assetPathFilter(path), 
                        this.onDestroyed = new g.Trigger();
                    }
                    return Asset.prototype.destroy = function() {
                        this.onDestroyed.fire(this), this.id = void 0, this.originalPath = void 0, this.path = void 0, 
                        this.onDestroyed.destroy(), this.onDestroyed = void 0;
                    }, Asset.prototype.destroyed = function() {
                        return void 0 === this.id;
                    }, Asset.prototype.inUse = function() {
                        return !1;
                    }, Asset.prototype._assetPathFilter = function(path) {
                        return path;
                    }, Asset;
                }();
                g.Asset = Asset;
                var ImageAsset = function(_super) {
                    function ImageAsset(id, assetPath, width, height) {
                        var _this = _super.call(this, id, assetPath) || this;
                        return _this.width = width, _this.height = height, _this;
                    }
                    return __extends(ImageAsset, _super), ImageAsset;
                }(Asset);
                g.ImageAsset = ImageAsset;
                var VideoAsset = function(_super) {
                    function VideoAsset(id, assetPath, width, height, system, loop, useRealSize) {
                        var _this = _super.call(this, id, assetPath, width, height) || this;
                        return _this.realWidth = 0, _this.realHeight = 0, _this._system = system, _this._loop = loop, 
                        _this._useRealSize = useRealSize, _this;
                    }
                    return __extends(VideoAsset, _super), VideoAsset.prototype.play = function(loop) {
                        return this.getPlayer().play(this), this.getPlayer();
                    }, VideoAsset.prototype.stop = function() {
                        this.getPlayer().stop();
                    }, VideoAsset.prototype.destroy = function() {
                        this._system = void 0, _super.prototype.destroy.call(this);
                    }, VideoAsset;
                }(ImageAsset);
                g.VideoAsset = VideoAsset;
                var AudioAsset = function(_super) {
                    function AudioAsset(id, assetPath, duration, system, loop, hint) {
                        var _this = _super.call(this, id, assetPath) || this;
                        return _this.duration = duration, _this.loop = loop, _this.hint = hint, _this._system = system, 
                        _this.data = void 0, _this;
                    }
                    return __extends(AudioAsset, _super), AudioAsset.prototype.play = function() {
                        var player = this._system.createPlayer();
                        return player.play(this), this._lastPlayedPlayer = player, player;
                    }, AudioAsset.prototype.stop = function() {
                        for (var players = this._system.findPlayers(this), i = 0; i < players.length; ++i) players[i].stop();
                    }, AudioAsset.prototype.inUse = function() {
                        return this._system.findPlayers(this).length > 0;
                    }, AudioAsset.prototype.destroy = function() {
                        this._system && this.stop(), this.data = void 0, this._system = void 0, this._lastPlayedPlayer = void 0, 
                        _super.prototype.destroy.call(this);
                    }, AudioAsset;
                }(Asset);
                g.AudioAsset = AudioAsset;
                var TextAsset = function(_super) {
                    function TextAsset(id, assetPath) {
                        var _this = _super.call(this, id, assetPath) || this;
                        return _this.data = void 0, _this;
                    }
                    return __extends(TextAsset, _super), TextAsset.prototype.destroy = function() {
                        this.data = void 0, _super.prototype.destroy.call(this);
                    }, TextAsset;
                }(Asset);
                g.TextAsset = TextAsset;
                var ScriptAsset = function(_super) {
                    function ScriptAsset() {
                        return null !== _super && _super.apply(this, arguments) || this;
                    }
                    return __extends(ScriptAsset, _super), ScriptAsset.prototype.destroy = function() {
                        this.script = void 0, _super.prototype.destroy.call(this);
                    }, ScriptAsset;
                }(Asset);
                g.ScriptAsset = ScriptAsset;
            }(g || (g = {}));
            var g;
            !function(g) {
                function normalizeAudioSystemConfMap(confMap) {
                    confMap = confMap || {};
                    var systemDefaults = {
                        music: {
                            loop: !0,
                            hint: {
                                streaming: !0
                            }
                        },
                        sound: {
                            loop: !1,
                            hint: {
                                streaming: !1
                            }
                        }
                    };
                    for (var key in systemDefaults) key in confMap || (confMap[key] = systemDefaults[key]);
                    return confMap;
                }
                var AssetLoadingInfo = function() {
                    function AssetLoadingInfo(asset, handler) {
                        this.asset = asset, this.handlers = [ handler ], this.errorCount = 0, this.loading = !1;
                    }
                    return AssetLoadingInfo;
                }(), AssetManager = function() {
                    function AssetManager(game, conf, audioSystemConfMap, moduleMainScripts) {
                        this.game = game, this.configuration = this._normalize(conf || {}, normalizeAudioSystemConfMap(audioSystemConfMap)), 
                        this._assets = {}, this._liveAssetVirtualPathTable = {}, this._liveAbsolutePathTable = {}, 
                        this._moduleMainScripts = moduleMainScripts ? moduleMainScripts : {}, this._refCounts = {}, 
                        this._loadings = {};
                    }
                    return AssetManager.prototype.destroy = function() {
                        for (var assetIds = Object.keys(this._refCounts), i = 0; i < assetIds.length; ++i) this._releaseAsset(assetIds[i]);
                        this.game = void 0, this.configuration = void 0, this._assets = void 0, this._liveAssetVirtualPathTable = void 0, 
                        this._liveAbsolutePathTable = void 0, this._refCounts = void 0, this._loadings = void 0;
                    }, AssetManager.prototype.destroyed = function() {
                        return void 0 === this.game;
                    }, AssetManager.prototype.retryLoad = function(asset) {
                        if (!this._loadings.hasOwnProperty(asset.id)) throw g.ExceptionFactory.createAssertionError("AssetManager#retryLoad: invalid argument.");
                        var loadingInfo = this._loadings[asset.id];
                        if (loadingInfo.errorCount > AssetManager.MAX_ERROR_COUNT) {
                            if (!this.configuration[asset.id]) return;
                            throw g.ExceptionFactory.createAssertionError("AssetManager#retryLoad: too many retrying.");
                        }
                        loadingInfo.loading || (loadingInfo.loading = !0, asset._load(this));
                    }, AssetManager.prototype.globalAssetIds = function() {
                        var ret = [], conf = this.configuration;
                        for (var p in conf) conf.hasOwnProperty(p) && conf[p].global && ret.push(p);
                        return ret;
                    }, AssetManager.prototype.requestAsset = function(assetIdOrConf, handler) {
                        var loadingInfo, assetId = "string" == typeof assetIdOrConf ? assetIdOrConf : assetIdOrConf.id, waiting = !1;
                        if (this._assets.hasOwnProperty(assetId)) ++this._refCounts[assetId], handler._onAssetLoad(this._assets[assetId]); else if (this._loadings.hasOwnProperty(assetId)) loadingInfo = this._loadings[assetId], 
                        loadingInfo.handlers.push(handler), ++this._refCounts[assetId], waiting = !0; else {
                            var a = this._createAssetFor(assetIdOrConf);
                            loadingInfo = new AssetLoadingInfo(a, handler), this._loadings[assetId] = loadingInfo, 
                            this._refCounts[assetId] = 1, waiting = !0, loadingInfo.loading = !0, a._load(this);
                        }
                        return waiting;
                    }, AssetManager.prototype.unrefAsset = function(assetOrId) {
                        var assetId = "string" == typeof assetOrId ? assetOrId : assetOrId.id;
                        --this._refCounts[assetId] > 0 || this._releaseAsset(assetId);
                    }, AssetManager.prototype.requestAssets = function(assetIdOrConfs, handler) {
                        for (var waitingCount = 0, i = 0, len = assetIdOrConfs.length; len > i; ++i) this.requestAsset(assetIdOrConfs[i], handler) && ++waitingCount;
                        return waitingCount;
                    }, AssetManager.prototype.unrefAssets = function(assetOrIds) {
                        for (var i = 0, len = assetOrIds.length; len > i; ++i) this.unrefAsset(assetOrIds[i]);
                    }, AssetManager.prototype._normalize = function(configuration, audioSystemConfMap) {
                        var ret = {};
                        if (!(configuration instanceof Object)) throw g.ExceptionFactory.createAssertionError("AssetManager#_normalize: invalid arguments.");
                        for (var p in configuration) if (configuration.hasOwnProperty(p)) {
                            var conf = Object.create(configuration[p]);
                            if (!conf.path) throw g.ExceptionFactory.createAssertionError("AssetManager#_normalize: No path given for: " + p);
                            if (!conf.virtualPath) throw g.ExceptionFactory.createAssertionError("AssetManager#_normalize: No virtualPath given for: " + p);
                            if (!conf.type) throw g.ExceptionFactory.createAssertionError("AssetManager#_normalize: No type given for: " + p);
                            if ("image" === conf.type) {
                                if ("number" != typeof conf.width) throw g.ExceptionFactory.createAssertionError("AssetManager#_normalize: wrong width given for the image asset: " + p);
                                if ("number" != typeof conf.height) throw g.ExceptionFactory.createAssertionError("AssetManager#_normalize: wrong height given for the image asset: " + p);
                            }
                            if ("audio" === conf.type) {
                                void 0 === conf.duration && (conf.duration = 0);
                                var audioSystemConf = audioSystemConfMap[conf.systemId];
                                void 0 === conf.loop && (conf.loop = !!audioSystemConf && !!audioSystemConf.loop), 
                                void 0 === conf.hint && (conf.hint = audioSystemConf ? audioSystemConf.hint : {});
                            }
                            if ("video" === conf.type && !conf.useRealSize) {
                                if ("number" != typeof conf.width) throw g.ExceptionFactory.createAssertionError("AssetManager#_normalize: wrong width given for the video asset: " + p);
                                if ("number" != typeof conf.height) throw g.ExceptionFactory.createAssertionError("AssetManager#_normalize: wrong height given for the video asset: " + p);
                                conf.useRealSize = !1;
                            }
                            conf.global || (conf.global = !1), ret[p] = conf;
                        }
                        return ret;
                    }, AssetManager.prototype._createAssetFor = function(idOrConf) {
                        var id, uri, conf;
                        if ("string" == typeof idOrConf) id = idOrConf, conf = this.configuration[id], uri = this.configuration[id].path; else {
                            var dynConf = idOrConf;
                            id = dynConf.id, conf = dynConf, uri = dynConf.uri;
                        }
                        var resourceFactory = this.game.resourceFactory;
                        if (!conf) throw g.ExceptionFactory.createAssertionError("AssetManager#_createAssetFor: unknown asset ID: " + id);
                        switch (conf.type) {
                          case "image":
                            return resourceFactory.createImageAsset(id, uri, conf.width, conf.height);

                          case "audio":
                            var system = conf.systemId ? this.game.audio[conf.systemId] : this.game.audio[this.game.defaultAudioSystemId];
                            return resourceFactory.createAudioAsset(id, uri, conf.duration, system, conf.loop, conf.hint);

                          case "text":
                            return resourceFactory.createTextAsset(id, uri);

                          case "script":
                            return resourceFactory.createScriptAsset(id, uri);

                          case "video":
                            return resourceFactory.createVideoAsset(id, uri, conf.width, conf.height, new g.VideoSystem(), conf.loop, conf.useRealSize);

                          default:
                            throw g.ExceptionFactory.createAssertionError("AssertionError#_createAssetFor: unknown asset type " + conf.type + " for asset ID: " + id);
                        }
                    }, AssetManager.prototype._releaseAsset = function(assetId) {
                        var path, asset = this._assets[assetId] || this._loadings[assetId] && this._loadings[assetId].asset;
                        if (asset) if (path = asset.path, asset.inUse()) if (asset instanceof g.AudioAsset) asset._system.requestDestroy(asset); else {
                            if (!(asset instanceof g.VideoAsset)) throw g.ExceptionFactory.createAssertionError("AssetManager#unrefAssets: Unsupported in-use " + asset.id);
                            asset.destroy();
                        } else asset.destroy();
                        if (delete this._refCounts[assetId], delete this._loadings[assetId], delete this._assets[assetId], 
                        this.configuration[assetId]) {
                            var virtualPath = this.configuration[assetId].virtualPath;
                            virtualPath && this._liveAssetVirtualPathTable.hasOwnProperty(virtualPath) && delete this._liveAssetVirtualPathTable[virtualPath], 
                            path && this._liveAbsolutePathTable.hasOwnProperty(path) && delete this._liveAbsolutePathTable[path];
                        }
                    }, AssetManager.prototype._countLoadingAsset = function() {
                        return Object.keys(this._loadings).length;
                    }, AssetManager.prototype._onAssetError = function(asset, error) {
                        if (!this.destroyed() && !asset.destroyed()) {
                            var loadingInfo = this._loadings[asset.id], hs = loadingInfo.handlers;
                            loadingInfo.loading = !1, ++loadingInfo.errorCount, loadingInfo.errorCount > AssetManager.MAX_ERROR_COUNT && error.retriable && (error = g.ExceptionFactory.createAssetLoadError("Retry limit exceeded", !1, g.AssetLoadErrorType.RetryLimitExceeded, error)), 
                            error.retriable || delete this._loadings[asset.id];
                            for (var i = 0; i < hs.length; ++i) hs[i]._onAssetError(asset, error, this);
                        }
                    }, AssetManager.prototype._onAssetLoad = function(asset) {
                        if (!this.destroyed() && !asset.destroyed()) {
                            var loadingInfo = this._loadings[asset.id];
                            if (loadingInfo.loading = !1, delete this._loadings[asset.id], this._assets[asset.id] = asset, 
                            this.configuration[asset.id]) {
                                var virtualPath = this.configuration[asset.id].virtualPath;
                                if (this._liveAssetVirtualPathTable.hasOwnProperty(virtualPath)) {
                                    if (this._liveAssetVirtualPathTable[virtualPath].path !== asset.path) throw g.ExceptionFactory.createAssertionError("AssetManager#_onAssetLoad(): duplicated asset path");
                                } else this._liveAssetVirtualPathTable[virtualPath] = asset;
                                this._liveAbsolutePathTable.hasOwnProperty(asset.path) || (this._liveAbsolutePathTable[asset.path] = virtualPath);
                            }
                            for (var hs = loadingInfo.handlers, i = 0; i < hs.length; ++i) hs[i]._onAssetLoad(asset);
                        }
                    }, AssetManager;
                }();
                AssetManager.MAX_ERROR_COUNT = 3, g.AssetManager = AssetManager;
            }(g || (g = {}));
            var g;
            !function(g) {
                function _require(game, path, currentModule) {
                    var targetScriptAsset, resolvedPath, liveAssetVirtualPathTable = game._assetManager._liveAssetVirtualPathTable, moduleMainScripts = game._assetManager._moduleMainScripts;
                    if (-1 === path.indexOf("/") && game._assetManager._assets.hasOwnProperty(path) && (targetScriptAsset = game._assetManager._assets[path], 
                    resolvedPath = game._assetManager._liveAbsolutePathTable[targetScriptAsset.path]), 
                    /^\.\/|^\.\.\/|^\//.test(path)) {
                        if (currentModule) {
                            if (!currentModule._virtualDirname) throw g.ExceptionFactory.createAssertionError("g._require: require from DynamicAsset is not supported");
                            resolvedPath = g.PathUtil.resolvePath(currentModule._virtualDirname, path);
                        } else {
                            if (!/^\.\//.test(path)) throw g.ExceptionFactory.createAssertionError("g._require: entry point path must start with './'");
                            resolvedPath = path.substring(2);
                        }
                        if (game._scriptCaches.hasOwnProperty(resolvedPath)) return game._scriptCaches[resolvedPath]._cachedValue();
                        if (game._scriptCaches.hasOwnProperty(resolvedPath + ".js")) return game._scriptCaches[resolvedPath + ".js"]._cachedValue();
                        targetScriptAsset || (targetScriptAsset = g.Util.findAssetByPathAsFile(resolvedPath, liveAssetVirtualPathTable)), 
                        targetScriptAsset || (targetScriptAsset = g.Util.findAssetByPathAsDirectory(resolvedPath, liveAssetVirtualPathTable));
                    } else if (moduleMainScripts[path] && (resolvedPath = moduleMainScripts[path], targetScriptAsset = game._assetManager._liveAssetVirtualPathTable[resolvedPath]), 
                    !targetScriptAsset) {
                        var dirs = currentModule ? currentModule.paths : [];
                        dirs.push("node_modules");
                        for (var i = 0; i < dirs.length; ++i) {
                            var dir = dirs[i];
                            if (resolvedPath = g.PathUtil.resolvePath(dir, path), targetScriptAsset = g.Util.findAssetByPathAsFile(resolvedPath, liveAssetVirtualPathTable)) break;
                            if (targetScriptAsset = g.Util.findAssetByPathAsDirectory(resolvedPath, liveAssetVirtualPathTable)) break;
                        }
                    }
                    if (targetScriptAsset) {
                        if (game._scriptCaches.hasOwnProperty(resolvedPath)) return game._scriptCaches[resolvedPath]._cachedValue();
                        if (targetScriptAsset instanceof g.ScriptAsset) {
                            var context = new g.ScriptAssetContext(game, targetScriptAsset);
                            return game._scriptCaches[resolvedPath] = context, context._executeScript(currentModule);
                        }
                        if (targetScriptAsset instanceof g.TextAsset && targetScriptAsset && ".json" === g.PathUtil.resolveExtname(path)) {
                            var cache = game._scriptCaches[resolvedPath] = new g.RequireCachedValue(JSON.parse(targetScriptAsset.data));
                            return cache._cachedValue();
                        }
                    }
                    throw g.ExceptionFactory.createAssertionError("g._require: can not find module: " + path);
                }
                g._require = _require;
                var Module = function() {
                    function Module(game, id, path) {
                        var _this = this, dirname = g.PathUtil.resolveDirname(path), virtualPath = game._assetManager._liveAbsolutePathTable[path], virtualDirname = virtualPath ? g.PathUtil.resolveDirname(virtualPath) : void 0, _g = Object.create(g, {
                            game: {
                                value: game,
                                enumerable: !0
                            },
                            filename: {
                                value: path,
                                enumerable: !0
                            },
                            dirname: {
                                value: dirname,
                                enumerable: !0
                            },
                            module: {
                                value: this,
                                writable: !0,
                                enumerable: !0,
                                configurable: !0
                            }
                        });
                        this.id = id, this.filename = path, this.exports = {}, this.parent = null, this.loaded = !1, 
                        this.children = [], this.paths = virtualDirname ? g.PathUtil.makeNodeModulePaths(virtualDirname) : [], 
                        this._dirname = dirname, this._virtualDirname = virtualDirname, this._g = _g, this.require = function(path) {
                            return "g" === path ? _g : g._require(game, path, _this);
                        };
                    }
                    return Module;
                }();
                g.Module = Module;
            }(g || (g = {}));
            var g;
            !function(g) {
                var ScriptAssetContext = function() {
                    function ScriptAssetContext(game, asset) {
                        this._game = game, this._asset = asset, this._module = new g.Module(game, asset.path, asset.path), 
                        this._g = this._module._g, this._started = !1;
                    }
                    return ScriptAssetContext.prototype._cachedValue = function() {
                        if (!this._started) throw g.ExceptionFactory.createAssertionError("ScriptAssetContext#_cachedValue: not executed yet.");
                        return this._module.exports;
                    }, ScriptAssetContext.prototype._executeScript = function(currentModule) {
                        return this._started ? this._module.exports : (currentModule && (this._module.parent = currentModule, 
                        currentModule.children.push(this._module)), this._started = !0, this._asset.execute(this._g), 
                        this._module.loaded = !0, this._module.exports);
                    }, ScriptAssetContext;
                }();
                g.ScriptAssetContext = ScriptAssetContext;
            }(g || (g = {}));
            var g;
            !function(g) {
                var PlainMatrix = function() {
                    function PlainMatrix(widthOrSrc, height, scaleX, scaleY, angle) {
                        void 0 === widthOrSrc ? (this._modified = !1, this._matrix = [ 1, 0, 0, 1, 0, 0 ]) : "number" == typeof widthOrSrc ? (this._modified = !1, 
                        this._matrix = new Array(6), this.update(widthOrSrc, height, scaleX, scaleY, angle, 0, 0)) : (this._modified = widthOrSrc._modified, 
                        this._matrix = [ widthOrSrc._matrix[0], widthOrSrc._matrix[1], widthOrSrc._matrix[2], widthOrSrc._matrix[3], widthOrSrc._matrix[4], widthOrSrc._matrix[5] ]);
                    }
                    return PlainMatrix.prototype.update = function(width, height, scaleX, scaleY, angle, x, y) {
                        var r = angle * Math.PI / 180, _cos = Math.cos(r), _sin = Math.sin(r), a = _cos * scaleX, b = _sin * scaleX, c = _sin * scaleY, d = _cos * scaleY, w = width / 2, h = height / 2;
                        this._matrix[0] = a, this._matrix[1] = b, this._matrix[2] = -c, this._matrix[3] = d, 
                        this._matrix[4] = -a * w + c * h + w + x, this._matrix[5] = -b * w - d * h + h + y;
                    }, PlainMatrix.prototype.updateByInverse = function(width, height, scaleX, scaleY, angle, x, y) {
                        var r = angle * Math.PI / 180, _cos = Math.cos(r), _sin = Math.sin(r), a = _cos / scaleX, b = _sin / scaleY, c = _sin / scaleX, d = _cos / scaleY, w = width / 2, h = height / 2;
                        this._matrix[0] = a, this._matrix[1] = -b, this._matrix[2] = c, this._matrix[3] = d, 
                        this._matrix[4] = -a * (w + x) - c * (h + y) + w, this._matrix[5] = b * (w + x) - d * (h + y) + h;
                    }, PlainMatrix.prototype.multiply = function(matrix) {
                        var m1 = this._matrix, m2 = matrix._matrix, m10 = m1[0], m11 = m1[1], m12 = m1[2], m13 = m1[3];
                        m1[0] = m10 * m2[0] + m12 * m2[1], m1[1] = m11 * m2[0] + m13 * m2[1], m1[2] = m10 * m2[2] + m12 * m2[3], 
                        m1[3] = m11 * m2[2] + m13 * m2[3], m1[4] = m10 * m2[4] + m12 * m2[5] + m1[4], m1[5] = m11 * m2[4] + m13 * m2[5] + m1[5];
                    }, PlainMatrix.prototype.multiplyNew = function(matrix) {
                        var ret = this.clone();
                        return ret.multiply(matrix), ret;
                    }, PlainMatrix.prototype.reset = function(x, y) {
                        this._matrix[0] = 1, this._matrix[1] = 0, this._matrix[2] = 0, this._matrix[3] = 1, 
                        this._matrix[4] = x || 0, this._matrix[5] = y || 0;
                    }, PlainMatrix.prototype.clone = function() {
                        return new PlainMatrix(this);
                    }, PlainMatrix.prototype.multiplyInverseForPoint = function(point) {
                        var m = this._matrix, _id = 1 / (m[0] * m[3] + m[2] * -m[1]);
                        return {
                            x: m[3] * _id * point.x + -m[2] * _id * point.y + (m[5] * m[2] - m[4] * m[3]) * _id,
                            y: m[0] * _id * point.y + -m[1] * _id * point.x + (-m[5] * m[0] + m[4] * m[1]) * _id
                        };
                    }, PlainMatrix.prototype.scale = function(x, y) {
                        var m = this._matrix;
                        m[0] *= x, m[1] *= y, m[2] *= x, m[3] *= y, m[4] *= x, m[5] *= y;
                    }, PlainMatrix.prototype.multiplyPoint = function(point) {
                        var m = this._matrix, x = m[0] * point.x + m[2] * point.y + m[4], y = m[1] * point.x + m[3] * point.y + m[5];
                        return {
                            x: x,
                            y: y
                        };
                    }, PlainMatrix;
                }();
                g.PlainMatrix = PlainMatrix;
            }(g || (g = {}));
            var g;
            !function(g) {
                var Util;
                !function(Util) {
                    function distance(p1x, p1y, p2x, p2y) {
                        return Math.sqrt(Math.pow(p1x - p2x, 2) + Math.pow(p1y - p2y, 2));
                    }
                    function distanceBetweenOffsets(p1, p2) {
                        return Util.distance(p1.x, p1.y, p2.x, p2.y);
                    }
                    function distanceBetweenAreas(p1, p2) {
                        return Util.distance(p1.x + p1.width / 2, p1.y + p1.height / 2, p2.x + p2.width / 2, p2.y + p2.height / 2);
                    }
                    function createMatrix(width, height, scaleX, scaleY, angle) {
                        return void 0 === width ? new g.PlainMatrix() : new g.PlainMatrix(width, height, scaleX, scaleY, angle);
                    }
                    function createSpriteFromE(scene, e, camera) {
                        var oldX = e.x, oldY = e.y, x = 0, y = 0, width = e.width, height = e.height, boundingRect = e.calculateBoundingRect(camera);
                        if (!boundingRect) throw g.ExceptionFactory.createAssertionError("Util#createSpriteFromE: camera must look e");
                        width = boundingRect.right - boundingRect.left, height = boundingRect.bottom - boundingRect.top, 
                        boundingRect.left < e.x && (x = e.x - boundingRect.left), boundingRect.top < e.y && (y = e.y - boundingRect.top), 
                        e.moveTo(x, y), e._matrix && (e._matrix._modified = !0);
                        var surface = scene.game.resourceFactory.createSurface(Math.ceil(width), Math.ceil(height)), renderer = surface.renderer();
                        renderer.begin(), e.render(renderer, camera), renderer.end();
                        var s = new g.Sprite({
                            scene: scene,
                            src: surface,
                            width: width,
                            height: height
                        });
                        return s.moveTo(boundingRect.left, boundingRect.top), e.moveTo(oldX, oldY), e._matrix && (e._matrix._modified = !0), 
                        s;
                    }
                    function createSpriteFromScene(toScene, fromScene, camera) {
                        var surface = toScene.game.resourceFactory.createSurface(Math.ceil(fromScene.game.width), Math.ceil(fromScene.game.height)), renderer = surface.renderer();
                        renderer.begin();
                        for (var children = fromScene.children, i = 0; i < children.length; ++i) children[i].render(renderer, camera);
                        return renderer.end(), new g.Sprite({
                            scene: toScene,
                            src: surface,
                            width: fromScene.game.width,
                            height: fromScene.game.height
                        });
                    }
                    function asSurface(src) {
                        if (!src) return src;
                        if (src instanceof g.Surface) return src;
                        if (src instanceof g.ImageAsset) return src.asSurface();
                        throw g.ExceptionFactory.createTypeMismatchError("Util#asSurface", "ImageAsset|Surface", src);
                    }
                    function findAssetByPathAsFile(resolvedPath, liveAssetPathTable) {
                        return liveAssetPathTable.hasOwnProperty(resolvedPath) ? liveAssetPathTable[resolvedPath] : liveAssetPathTable.hasOwnProperty(resolvedPath + ".js") ? liveAssetPathTable[resolvedPath + ".js"] : void 0;
                    }
                    function findAssetByPathAsDirectory(resolvedPath, liveAssetPathTable) {
                        var path;
                        if (path = resolvedPath + "/package.json", liveAssetPathTable.hasOwnProperty(path) && liveAssetPathTable[path] instanceof g.TextAsset) {
                            var pkg = JSON.parse(liveAssetPathTable[path].data);
                            if (pkg && "string" == typeof pkg.main) {
                                var asset = Util.findAssetByPathAsFile(g.PathUtil.resolvePath(resolvedPath, pkg.main), liveAssetPathTable);
                                if (asset) return asset;
                            }
                        }
                        return path = resolvedPath + "/index.js", liveAssetPathTable.hasOwnProperty(path) ? liveAssetPathTable[path] : void 0;
                    }
                    function charCodeAt(str, idx) {
                        var code = str.charCodeAt(idx);
                        if (code >= 55296 && 56319 >= code) {
                            var hi = code, low = str.charCodeAt(idx + 1);
                            return hi << 16 | low;
                        }
                        return code >= 56320 && 57343 >= code ? null : code;
                    }
                    function setupAnimatingHandler(animatingHandler, surface) {
                        surface.isDynamic && (surface.animatingStarted.add(animatingHandler._onAnimatingStarted, animatingHandler), 
                        surface.animatingStopped.add(animatingHandler._onAnimatingStopped, animatingHandler), 
                        surface.isPlaying() && animatingHandler._onAnimatingStarted());
                    }
                    function migrateAnimatingHandler(animatingHandler, beforeSurface, afterSurface) {
                        animatingHandler._onAnimatingStopped(), !beforeSurface.destroyed() && beforeSurface.isDynamic && (beforeSurface.animatingStarted.remove(animatingHandler._onAnimatingStarted, animatingHandler), 
                        beforeSurface.animatingStopped.remove(animatingHandler._onAnimatingStopped, animatingHandler)), 
                        afterSurface.isDynamic && (afterSurface.animatingStarted.add(animatingHandler._onAnimatingStarted, animatingHandler), 
                        afterSurface.animatingStopped.add(animatingHandler._onAnimatingStopped, animatingHandler), 
                        afterSurface.isPlaying() && animatingHandler._onAnimatingStarted());
                    }
                    Util.distance = distance, Util.distanceBetweenOffsets = distanceBetweenOffsets, 
                    Util.distanceBetweenAreas = distanceBetweenAreas, Util.createMatrix = createMatrix, 
                    Util.createSpriteFromE = createSpriteFromE, Util.createSpriteFromScene = createSpriteFromScene, 
                    Util.asSurface = asSurface, Util.findAssetByPathAsFile = findAssetByPathAsFile, 
                    Util.findAssetByPathAsDirectory = findAssetByPathAsDirectory, Util.charCodeAt = charCodeAt, 
                    Util.setupAnimatingHandler = setupAnimatingHandler, Util.migrateAnimatingHandler = migrateAnimatingHandler;
                }(Util = g.Util || (g.Util = {}));
            }(g || (g = {}));
            var g;
            !function(g) {
                var Collision;
                !function(Collision) {
                    function intersect(x1, y1, width1, height1, x2, y2, width2, height2) {
                        return x2 + width2 >= x1 && x1 + width1 >= x2 && y2 + height2 >= y1 && y1 + height1 >= y2;
                    }
                    function intersectAreas(t1, t2) {
                        return Collision.intersect(t1.x, t1.y, t1.width, t1.height, t2.x, t2.y, t2.width, t2.height);
                    }
                    function within(t1x, t1y, t2x, t2y, distance) {
                        return void 0 === distance && (distance = 1), distance >= g.Util.distance(t1x, t1y, t2x, t2y);
                    }
                    function withinAreas(t1, t2, distance) {
                        return void 0 === distance && (distance = 1), distance >= g.Util.distanceBetweenAreas(t1, t2);
                    }
                    Collision.intersect = intersect, Collision.intersectAreas = intersectAreas, Collision.within = within, 
                    Collision.withinAreas = withinAreas;
                }(Collision = g.Collision || (g.Collision = {}));
            }(g || (g = {}));
            var g;
            !function(g) {
                var Trigger = function() {
                    function Trigger() {
                        this._handlers = [], this.length = 0;
                    }
                    return Trigger.prototype.add = function(paramsOrFunc, owner) {
                        if ("function" == typeof paramsOrFunc) this._handlers.push({
                            func: paramsOrFunc,
                            owner: owner,
                            once: !1,
                            name: void 0
                        }); else {
                            var params = paramsOrFunc;
                            "number" == typeof params.index ? this._handlers.splice(params.index, 0, {
                                func: params.func,
                                owner: params.owner,
                                once: !1,
                                name: params.name
                            }) : this._handlers.push({
                                func: params.func,
                                owner: params.owner,
                                once: !1,
                                name: params.name
                            });
                        }
                        this.length = this._handlers.length;
                    }, Trigger.prototype.addOnce = function(paramsOrFunc, owner) {
                        if ("function" == typeof paramsOrFunc) this._handlers.push({
                            func: paramsOrFunc,
                            owner: owner,
                            once: !0,
                            name: void 0
                        }); else {
                            var params = paramsOrFunc;
                            "number" == typeof params.index ? this._handlers.splice(params.index, 0, {
                                func: params.func,
                                owner: params.owner,
                                once: !0,
                                name: params.name
                            }) : this._handlers.push({
                                func: params.func,
                                owner: params.owner,
                                once: !0,
                                name: params.name
                            });
                        }
                        this.length = this._handlers.length;
                    }, Trigger.prototype.handle = function(owner, func, name) {
                        this.add(func ? {
                            owner: owner,
                            func: func,
                            name: name
                        } : {
                            func: owner
                        });
                    }, Trigger.prototype.fire = function(arg) {
                        if (this._handlers && this._handlers.length) {
                            for (var handlers = this._handlers.concat(), i = 0; i < handlers.length; i++) {
                                var handler = handlers[i];
                                if (handler.func.call(handler.owner, arg) || handler.once) {
                                    var index = this._handlers.indexOf(handler);
                                    -1 !== index && this._handlers.splice(index, 1);
                                }
                            }
                            this._handlers && (this.length = this._handlers.length);
                        }
                    }, Trigger.prototype.contains = function(paramsOrFunc, owner) {
                        for (var condition = "function" == typeof paramsOrFunc ? {
                            func: paramsOrFunc,
                            owner: owner
                        } : paramsOrFunc, i = 0; i < this._handlers.length; i++) if (this._comparePartial(condition, this._handlers[i])) return !0;
                        return !1;
                    }, Trigger.prototype.remove = function(paramsOrFunc, owner) {
                        for (var condition = "function" == typeof paramsOrFunc ? {
                            func: paramsOrFunc,
                            owner: owner
                        } : paramsOrFunc, i = 0; i < this._handlers.length; i++) {
                            var handler = this._handlers[i];
                            if (condition.func === handler.func && condition.owner === handler.owner && condition.name === handler.name) return this._handlers.splice(i, 1), 
                            void --this.length;
                        }
                    }, Trigger.prototype.removeAll = function(params) {
                        var handlers = [];
                        if (params) for (var i = 0; i < this._handlers.length; i++) {
                            var handler = this._handlers[i];
                            this._comparePartial(params, handler) || handlers.push(handler);
                        }
                        this._handlers = handlers, this.length = this._handlers.length;
                    }, Trigger.prototype.destroy = function() {
                        this._handlers = null, this.length = null;
                    }, Trigger.prototype.destroyed = function() {
                        return null === this._handlers;
                    }, Trigger.prototype._comparePartial = function(target, compare) {
                        return void 0 !== target.func && target.func !== compare.func ? !1 : void 0 !== target.owner && target.owner !== compare.owner ? !1 : void 0 !== target.name && target.name !== compare.name ? !1 : !0;
                    }, Trigger;
                }();
                g.Trigger = Trigger;
                var ChainTrigger = function(_super) {
                    function ChainTrigger(chain, filter, filterOwner) {
                        var _this = _super.call(this) || this;
                        return _this.chain = chain, _this.filter = filter, _this.filterOwner = filterOwner, 
                        _this._isActivated = !1, _this;
                    }
                    return __extends(ChainTrigger, _super), ChainTrigger.prototype.add = function(paramsOrHandler, owner) {
                        _super.prototype.add.call(this, paramsOrHandler, owner), this._isActivated || (this.chain.add(this._onChainTriggerFired, this), 
                        this._isActivated = !0);
                    }, ChainTrigger.prototype.addOnce = function(paramsOrHandler, owner) {
                        _super.prototype.addOnce.call(this, paramsOrHandler, owner), this._isActivated || (this.chain.add(this._onChainTriggerFired, this), 
                        this._isActivated = !0);
                    }, ChainTrigger.prototype.remove = function(paramsOrFunc, owner) {
                        _super.prototype.remove.call(this, paramsOrFunc, owner), 0 === this.length && this._isActivated && (this.chain.remove(this._onChainTriggerFired, this), 
                        this._isActivated = !1);
                    }, ChainTrigger.prototype.removeAll = function(params) {
                        _super.prototype.removeAll.call(this, params), 0 === this.length && this._isActivated && (this.chain.remove(this._onChainTriggerFired, this), 
                        this._isActivated = !1);
                    }, ChainTrigger.prototype.destroy = function() {
                        _super.prototype.destroy.call(this), this.chain.remove(this._onChainTriggerFired, this), 
                        this.filter = null, this.filterOwner = null, this._isActivated = !1;
                    }, ChainTrigger.prototype._onChainTriggerFired = function(args) {
                        (!this.filter || this.filter.call(this.filterOwner, args)) && this.fire(args);
                    }, ChainTrigger;
                }(Trigger);
                g.ChainTrigger = ChainTrigger;
            }(g || (g = {}));
            var g;
            !function(g) {
                var Timer = function() {
                    function Timer(interval, fps) {
                        this.interval = interval, this._scaledInterval = Math.round(interval * fps), this.elapsed = new g.Trigger(), 
                        this._scaledElapsed = 0;
                    }
                    return Timer.prototype.tick = function() {
                        for (this._scaledElapsed += 1e3; this._scaledElapsed >= this._scaledInterval && this.elapsed; ) this.elapsed.fire(), 
                        this._scaledElapsed -= this._scaledInterval;
                    }, Timer.prototype.canDelete = function() {
                        return !this.elapsed || 0 === this.elapsed.length;
                    }, Timer.prototype.destroy = function() {
                        this.interval = void 0, this.elapsed.destroy(), this.elapsed = void 0, this._scaledInterval = 0, 
                        this._scaledElapsed = 0;
                    }, Timer.prototype.destroyed = function() {
                        return void 0 === this.elapsed;
                    }, Timer;
                }();
                g.Timer = Timer;
            }(g || (g = {}));
            var g;
            !function(g) {
                var TimerIdentifier = function() {
                    function TimerIdentifier(timer, handler, handlerOwner, fired, firedOwner) {
                        this._timer = timer, this._handler = handler, this._handlerOwner = handlerOwner, 
                        this._fired = fired, this._firedOwner = firedOwner, this._timer.elapsed.add(this._fire, this);
                    }
                    return TimerIdentifier.prototype.destroy = function() {
                        this._timer.elapsed.remove(this._fire, this), this._timer = void 0, this._handler = void 0, 
                        this._handlerOwner = void 0, this._fired = void 0, this._firedOwner = void 0;
                    }, TimerIdentifier.prototype.destroyed = function() {
                        return void 0 === this._timer;
                    }, TimerIdentifier.prototype._fire = function() {
                        this._handler.call(this._handlerOwner), this._fired && this._fired.call(this._firedOwner, this);
                    }, TimerIdentifier;
                }();
                g.TimerIdentifier = TimerIdentifier;
                var TimerManager = function() {
                    function TimerManager(trigger, fps) {
                        this._timers = [], this._trigger = trigger, this._identifiers = [], this._fps = fps, 
                        this._registered = !1;
                    }
                    return TimerManager.prototype.destroy = function() {
                        for (var i = 0; i < this._identifiers.length; ++i) this._identifiers[i].destroy();
                        for (var i = 0; i < this._timers.length; ++i) this._timers[i].destroy();
                        this._timers = void 0, this._trigger = void 0, this._identifiers = void 0, this._fps = void 0;
                    }, TimerManager.prototype.destroyed = function() {
                        return void 0 === this._timers;
                    }, TimerManager.prototype.createTimer = function(interval) {
                        if (this._registered || (this._trigger.add(this._tick, this), this._registered = !0), 
                        0 > interval) throw g.ExceptionFactory.createAssertionError("TimerManager#createTimer: invalid interval");
                        1 > interval && (interval = 1);
                        for (var acceptableMargin = Math.min(1e3, interval * this._fps), i = 0; i < this._timers.length; ++i) if (this._timers[i].interval === interval && this._timers[i]._scaledElapsed < acceptableMargin) return this._timers[i];
                        var timer = new g.Timer(interval, this._fps);
                        return this._timers.push(timer), timer;
                    }, TimerManager.prototype.deleteTimer = function(timer) {
                        if (timer.canDelete()) {
                            var index = this._timers.indexOf(timer);
                            if (0 > index) throw g.ExceptionFactory.createAssertionError("TimerManager#deleteTimer: can not find timer");
                            if (this._timers.splice(index, 1), timer.destroy(), !this._timers.length) {
                                if (!this._registered) throw g.ExceptionFactory.createAssertionError("TimerManager#deleteTimer: handler is not handled");
                                this._trigger.remove(this._tick, this), this._registered = !1;
                            }
                        }
                    }, TimerManager.prototype.setTimeout = function(handler, milliseconds, owner) {
                        var timer = this.createTimer(milliseconds), identifier = new TimerIdentifier(timer, handler, owner, this._onTimeoutFired, this);
                        return this._identifiers.push(identifier), identifier;
                    }, TimerManager.prototype.clearTimeout = function(identifier) {
                        this._clear(identifier);
                    }, TimerManager.prototype.setInterval = function(handler, interval, owner) {
                        var timer = this.createTimer(interval), identifier = new TimerIdentifier(timer, handler, owner);
                        return this._identifiers.push(identifier), identifier;
                    }, TimerManager.prototype.clearInterval = function(identifier) {
                        this._clear(identifier);
                    }, TimerManager.prototype._tick = function() {
                        for (var timers = this._timers.concat(), i = 0; i < timers.length; ++i) timers[i].tick();
                    }, TimerManager.prototype._onTimeoutFired = function(identifier) {
                        var index = this._identifiers.indexOf(identifier);
                        if (0 > index) throw g.ExceptionFactory.createAssertionError("TimerManager#_onTimeoutFired: can not find identifier");
                        this._identifiers.splice(index, 1);
                        var timer = identifier._timer;
                        identifier.destroy(), this.deleteTimer(timer);
                    }, TimerManager.prototype._clear = function(identifier) {
                        var index = this._identifiers.indexOf(identifier);
                        if (0 > index) throw g.ExceptionFactory.createAssertionError("TimerManager#_clear: can not find identifier");
                        if (identifier.destroyed()) throw g.ExceptionFactory.createAssertionError("TimerManager#_clear: invalid identifier");
                        this._identifiers.splice(index, 1);
                        var timer = identifier._timer;
                        identifier.destroy(), this.deleteTimer(timer);
                    }, TimerManager;
                }();
                g.TimerManager = TimerManager;
            }(g || (g = {}));
            var g;
            !function(g) {
                var AudioPlayer = function() {
                    function AudioPlayer(system) {
                        this.played = new g.Trigger(), this.stopped = new g.Trigger(), this.currentAudio = void 0, 
                        this.volume = system.volume, this._muted = system._muted, this._playbackRate = system._playbackRate, 
                        this._system = system;
                    }
                    return AudioPlayer.prototype.play = function(audio) {
                        this.currentAudio = audio, this.played.fire({
                            player: this,
                            audio: audio
                        });
                    }, AudioPlayer.prototype.stop = function() {
                        var audio = this.currentAudio;
                        audio && (this.currentAudio = void 0, this.stopped.fire({
                            player: this,
                            audio: audio
                        }));
                    }, AudioPlayer.prototype.canHandleStopped = function() {
                        return !0;
                    }, AudioPlayer.prototype.changeVolume = function(volume) {
                        this.volume = volume;
                    }, AudioPlayer.prototype._changeMuted = function(muted) {
                        this._muted = muted;
                    }, AudioPlayer.prototype._changePlaybackRate = function(rate) {
                        this._playbackRate = rate;
                    }, AudioPlayer.prototype._supportsPlaybackRate = function() {
                        return !1;
                    }, AudioPlayer.prototype._onVolumeChanged = function() {}, AudioPlayer;
                }();
                g.AudioPlayer = AudioPlayer;
            }(g || (g = {}));
            var g;
            !function(g) {
                var AudioSystem = function() {
                    function AudioSystem(id, game) {
                        var audioSystemManager = game._audioSystemManager;
                        this.id = id, this.game = game, this._volume = 1, this._destroyRequestedAssets = {}, 
                        this._muted = audioSystemManager._muted, this._playbackRate = audioSystemManager._playbackRate;
                    }
                    return Object.defineProperty(AudioSystem.prototype, "volume", {
                        get: function() {
                            return this._volume;
                        },
                        set: function(value) {
                            if (0 > value || value > 1 || isNaN(value) || "number" != typeof value) throw g.ExceptionFactory.createAssertionError("AudioSystem#volume: expected: 0.0-1.0, actual: " + value);
                            this._volume = value, this._onVolumeChanged();
                        },
                        enumerable: !0,
                        configurable: !0
                    }), AudioSystem.prototype.requestDestroy = function(asset) {
                        this._destroyRequestedAssets[asset.id] = asset;
                    }, AudioSystem.prototype._reset = function() {
                        this.stopAll(), this._volume = 1, this._destroyRequestedAssets = {}, this._muted = this.game._audioSystemManager._muted, 
                        this._playbackRate = this.game._audioSystemManager._playbackRate;
                    }, AudioSystem.prototype._setMuted = function(value) {
                        var before = this._muted;
                        this._muted = !!value, this._muted !== before && this._onMutedChanged();
                    }, AudioSystem.prototype._setPlaybackRate = function(value) {
                        if (0 > value || isNaN(value) || "number" != typeof value) throw g.ExceptionFactory.createAssertionError("AudioSystem#playbackRate: expected: greater or equal to 0.0, actual: " + value);
                        var before = this._playbackRate;
                        this._playbackRate = value, this._playbackRate !== before && this._onPlaybackRateChanged();
                    }, AudioSystem;
                }();
                g.AudioSystem = AudioSystem;
                var MusicAudioSystem = function(_super) {
                    function MusicAudioSystem(id, game) {
                        var _this = _super.call(this, id, game) || this;
                        return _this._player = void 0, _this._suppressingAudio = void 0, _this;
                    }
                    return __extends(MusicAudioSystem, _super), Object.defineProperty(MusicAudioSystem.prototype, "player", {
                        get: function() {
                            return this._player || (this._player = this.game.resourceFactory.createAudioPlayer(this), 
                            this._player.played.handle(this, this._onPlayerPlayed), this._player.stopped.handle(this, this._onPlayerStopped)), 
                            this._player;
                        },
                        set: function(v) {
                            this._player = v;
                        },
                        enumerable: !0,
                        configurable: !0
                    }), MusicAudioSystem.prototype.findPlayers = function(asset) {
                        return this.player.currentAudio && this.player.currentAudio.id === asset.id ? [ this.player ] : [];
                    }, MusicAudioSystem.prototype.createPlayer = function() {
                        return this.player;
                    }, MusicAudioSystem.prototype.stopAll = function() {
                        this._player && this._player.stop();
                    }, MusicAudioSystem.prototype._reset = function() {
                        _super.prototype._reset.call(this), this._player && (this._player.played.remove({
                            owner: this,
                            func: this._onPlayerPlayed
                        }), this._player.stopped.remove({
                            owner: this,
                            func: this._onPlayerStopped
                        })), this._player = void 0, this._suppressingAudio = void 0;
                    }, MusicAudioSystem.prototype._onVolumeChanged = function() {
                        this.player.changeVolume(this._volume);
                    }, MusicAudioSystem.prototype._onMutedChanged = function() {
                        this.player._changeMuted(this._muted);
                    }, MusicAudioSystem.prototype._onPlaybackRateChanged = function() {
                        var player = this.player;
                        player._changePlaybackRate(this._playbackRate), player._supportsPlaybackRate() || this._onUnsupportedPlaybackRateChanged();
                    }, MusicAudioSystem.prototype._onUnsupportedPlaybackRateChanged = function() {
                        if (1 === this._playbackRate && this._suppressingAudio) {
                            var audio = this._suppressingAudio;
                            this._suppressingAudio = void 0, audio.destroyed() || this.player.play(audio);
                        }
                    }, MusicAudioSystem.prototype._onPlayerPlayed = function(e) {
                        if (e.player !== this._player) throw g.ExceptionFactory.createAssertionError("MusicAudioSystem#_onPlayerPlayed: unexpected audio player");
                        e.player._supportsPlaybackRate() || 1 !== this._playbackRate && (e.player.stop(), 
                        this._suppressingAudio = e.audio);
                    }, MusicAudioSystem.prototype._onPlayerStopped = function(e) {
                        this._destroyRequestedAssets[e.audio.id] && (delete this._destroyRequestedAssets[e.audio.id], 
                        e.audio.destroy());
                    }, MusicAudioSystem;
                }(AudioSystem);
                g.MusicAudioSystem = MusicAudioSystem;
                var SoundAudioSystem = function(_super) {
                    function SoundAudioSystem(id, game) {
                        var _this = _super.call(this, id, game) || this;
                        return _this.players = [], _this;
                    }
                    return __extends(SoundAudioSystem, _super), SoundAudioSystem.prototype.createPlayer = function() {
                        var player = this.game.resourceFactory.createAudioPlayer(this);
                        return player.canHandleStopped() && this.players.push(player), player.played.handle(this, this._onPlayerPlayed), 
                        player.stopped.handle(this, this._onPlayerStopped), player;
                    }, SoundAudioSystem.prototype.findPlayers = function(asset) {
                        for (var ret = [], i = 0; i < this.players.length; ++i) this.players[i].currentAudio && this.players[i].currentAudio.id === asset.id && ret.push(this.players[i]);
                        return ret;
                    }, SoundAudioSystem.prototype.stopAll = function() {
                        for (var players = this.players.concat(), i = 0; i < players.length; ++i) players[i].stop();
                    }, SoundAudioSystem.prototype._reset = function() {
                        _super.prototype._reset.call(this);
                        for (var i = 0; i < this.players.length; ++i) {
                            var player = this.players[i];
                            player.played.remove({
                                owner: this,
                                func: this._onPlayerPlayed
                            }), player.stopped.remove({
                                owner: this,
                                func: this._onPlayerStopped
                            });
                        }
                        this.players = [];
                    }, SoundAudioSystem.prototype._onMutedChanged = function() {
                        for (var players = this.players, i = 0; i < players.length; ++i) players[i]._changeMuted(this._muted);
                    }, SoundAudioSystem.prototype._onPlaybackRateChanged = function() {
                        for (var players = this.players, i = 0; i < players.length; ++i) players[i]._changePlaybackRate(this._playbackRate);
                    }, SoundAudioSystem.prototype._onPlayerPlayed = function(e) {
                        e.player._supportsPlaybackRate() || 1 !== this._playbackRate && e.player.stop();
                    }, SoundAudioSystem.prototype._onPlayerStopped = function(e) {
                        var index = this.players.indexOf(e.player);
                        0 > index || (e.player.stopped.remove({
                            owner: this,
                            func: this._onPlayerStopped
                        }), this.players.splice(index, 1), this._destroyRequestedAssets[e.audio.id] && (delete this._destroyRequestedAssets[e.audio.id], 
                        e.audio.destroy()));
                    }, SoundAudioSystem.prototype._onVolumeChanged = function() {
                        for (var i = 0; i < this.players.length; ++i) this.players[i].changeVolume(this._volume);
                    }, SoundAudioSystem;
                }(AudioSystem);
                g.SoundAudioSystem = SoundAudioSystem;
            }(g || (g = {}));
            var g;
            !function(g) {
                var VideoPlayer = function() {
                    function VideoPlayer(loop) {
                        this._loop = !!loop, this.played = new g.Trigger(), this.stopped = new g.Trigger(), 
                        this.currentVideo = void 0, this.volume = 1;
                    }
                    return VideoPlayer.prototype.play = function(videoAsset) {
                        this.currentVideo = videoAsset, this.played.fire({
                            player: this,
                            video: videoAsset
                        }), videoAsset.asSurface().animatingStarted.fire();
                    }, VideoPlayer.prototype.stop = function() {
                        var videoAsset = this.currentVideo;
                        this.stopped.fire({
                            player: this,
                            video: videoAsset
                        }), videoAsset.asSurface().animatingStopped.fire();
                    }, VideoPlayer.prototype.changeVolume = function(volume) {
                        this.volume = volume;
                    }, VideoPlayer;
                }();
                g.VideoPlayer = VideoPlayer;
            }(g || (g = {}));
            var g;
            !function(g) {
                var VideoSystem = function() {
                    function VideoSystem() {}
                    return VideoSystem;
                }();
                g.VideoSystem = VideoSystem;
            }(g || (g = {}));
            var g;
            !function(g) {
                var Object2D = function() {
                    function Object2D(param) {
                        param ? (this.x = param.x || 0, this.y = param.y || 0, this.width = param.width || 0, 
                        this.height = param.height || 0, this.opacity = "opacity" in param ? param.opacity : 1, 
                        this.scaleX = "scaleX" in param ? param.scaleX : 1, this.scaleY = "scaleY" in param ? param.scaleY : 1, 
                        this.angle = param.angle || 0, this.compositeOperation = param.compositeOperation, 
                        this._matrix = void 0) : (this.x = 0, this.y = 0, this.width = 0, this.height = 0, 
                        this.opacity = 1, this.scaleX = 1, this.scaleY = 1, this.angle = 0, this.compositeOperation = void 0, 
                        this._matrix = void 0);
                    }
                    return Object2D.prototype.moveTo = function(posOrX, y) {
                        if ("number" == typeof posOrX && "number" != typeof y) throw g.ExceptionFactory.createAssertionError("Object2D#moveTo: arguments must be CommonOffset or pair of x and y as a number.");
                        "number" == typeof posOrX ? (this.x = posOrX, this.y = y) : (this.x = posOrX.x, 
                        this.y = posOrX.y);
                    }, Object2D.prototype.moveBy = function(x, y) {
                        this.x += x, this.y += y;
                    }, Object2D.prototype.resizeTo = function(sizeOrWidth, height) {
                        if ("number" == typeof sizeOrWidth && "number" != typeof height) throw g.ExceptionFactory.createAssertionError("Object2D#resizeTo: arguments must be CommonSize or pair of width and height as a number.");
                        "number" == typeof sizeOrWidth ? (this.width = sizeOrWidth, this.height = height) : (this.width = sizeOrWidth.width, 
                        this.height = sizeOrWidth.height);
                    }, Object2D.prototype.resizeBy = function(width, height) {
                        this.width += width, this.height += height;
                    }, Object2D.prototype.scale = function(scale) {
                        this.scaleX = scale, this.scaleY = scale;
                    }, Object2D.prototype.getMatrix = function() {
                        if (this._matrix) {
                            if (!this._matrix._modified) return this._matrix;
                        } else this._matrix = g.Util.createMatrix();
                        return this._updateMatrix(), this._matrix._modified = !1, this._matrix;
                    }, Object2D.prototype._updateMatrix = function() {
                        this.angle || 1 !== this.scaleX || 1 !== this.scaleY ? this._matrix.update(this.width, this.height, this.scaleX, this.scaleY, this.angle, this.x, this.y) : this._matrix.reset(this.x, this.y);
                    }, Object2D;
                }();
                g.Object2D = Object2D;
            }(g || (g = {}));
            var g;
            !function(g) {
                var E = function(_super) {
                    function E(param) {
                        var _this = _super.call(this, param) || this;
                        if (_this.children = void 0, _this.parent = void 0, _this._touchable = !1, _this.state = 0, 
                        _this._hasTouchableChildren = !1, _this._update = void 0, _this._message = void 0, 
                        _this._pointDown = void 0, _this._pointMove = void 0, _this._pointUp = void 0, _this._targetCameras = void 0, 
                        _this.tag = param.tag, _this.shaderProgram = param.shaderProgram, _this.local = param.scene.local !== g.LocalTickMode.NonLocal || !!param.local, 
                        param.children) for (var i = 0; i < param.children.length; ++i) _this.append(param.children[i]);
                        return param.parent && param.parent.append(_this), param.targetCameras && (_this.targetCameras = param.targetCameras), 
                        "touchable" in param && (_this.touchable = param.touchable), param.hidden && _this.hide(), 
                        _this.id = param.id, param.scene.register(_this), _this;
                    }
                    return __extends(E, _super), Object.defineProperty(E.prototype, "update", {
                        get: function() {
                            return this._update || (this._update = new g.ChainTrigger(this.scene.update)), this._update;
                        },
                        enumerable: !0,
                        configurable: !0
                    }), Object.defineProperty(E.prototype, "message", {
                        get: function() {
                            return this._message || (this._message = new g.ChainTrigger(this.scene.message)), 
                            this._message;
                        },
                        enumerable: !0,
                        configurable: !0
                    }), Object.defineProperty(E.prototype, "pointDown", {
                        get: function() {
                            return this._pointDown || (this._pointDown = new g.ChainTrigger(this.scene.pointDownCapture, this._isTargetOperation, this)), 
                            this._pointDown;
                        },
                        enumerable: !0,
                        configurable: !0
                    }), Object.defineProperty(E.prototype, "pointUp", {
                        get: function() {
                            return this._pointUp || (this._pointUp = new g.ChainTrigger(this.scene.pointUpCapture, this._isTargetOperation, this)), 
                            this._pointUp;
                        },
                        enumerable: !0,
                        configurable: !0
                    }), Object.defineProperty(E.prototype, "pointMove", {
                        get: function() {
                            return this._pointMove || (this._pointMove = new g.ChainTrigger(this.scene.pointMoveCapture, this._isTargetOperation, this)), 
                            this._pointMove;
                        },
                        enumerable: !0,
                        configurable: !0
                    }), Object.defineProperty(E.prototype, "targetCameras", {
                        get: function() {
                            return this._targetCameras || (this._targetCameras = []);
                        },
                        set: function(v) {
                            this._targetCameras = v;
                        },
                        enumerable: !0,
                        configurable: !0
                    }), Object.defineProperty(E.prototype, "touchable", {
                        get: function() {
                            return this._touchable;
                        },
                        set: function(v) {
                            this._touchable !== v && (this._touchable = v, v ? this._enableTouchPropagation() : this._disableTouchPropagation());
                        },
                        enumerable: !0,
                        configurable: !0
                    }), E.prototype.render = function(renderer, camera) {
                        if (this.state &= -5, !(1 & this.state)) {
                            var cams = this._targetCameras;
                            if (!(cams && cams.length > 0) || camera && -1 !== cams.indexOf(camera)) {
                                if (8 & this.state) {
                                    renderer.translate(this.x, this.y);
                                    var goDown = this.renderSelf(renderer, camera);
                                    if (goDown && this.children) for (var children = this.children, len = children.length, i = 0; len > i; ++i) children[i].render(renderer, camera);
                                    return void renderer.translate(-this.x, -this.y);
                                }
                                renderer.save(), this.angle || 1 !== this.scaleX || 1 !== this.scaleY ? renderer.transform(this.getMatrix()._matrix) : renderer.translate(this.x, this.y), 
                                1 !== this.opacity && renderer.opacity(this.opacity), void 0 !== this.compositeOperation && renderer.setCompositeOperation(this.compositeOperation), 
                                void 0 !== this.shaderProgram && renderer.isSupportedShaderProgram() && renderer.setShaderProgram(this.shaderProgram);
                                var goDown = this.renderSelf(renderer, camera);
                                if (goDown && this.children) for (var children = this.children, i = 0; i < children.length; ++i) children[i].render(renderer, camera);
                                renderer.restore();
                            }
                        }
                    }, E.prototype.renderSelf = function(renderer, camera) {
                        return !0;
                    }, E.prototype.game = function() {
                        return this.scene.game;
                    }, E.prototype.append = function(e) {
                        this.insertBefore(e, void 0);
                    }, E.prototype.insertBefore = function(e, target) {
                        e.parent && e.remove(), this.children || (this.children = []), e.parent = this;
                        var index = -1;
                        void 0 !== target && (index = this.children.indexOf(target)) > -1 ? this.children.splice(index, 0, e) : this.children.push(e), 
                        (e._touchable || e._hasTouchableChildren) && (this._hasTouchableChildren = !0, this._enableTouchPropagation()), 
                        this.modified(!0);
                    }, E.prototype.remove = function(e) {
                        if (void 0 === e) return void this.parent.remove(this);
                        var index = this.children ? this.children.indexOf(e) : -1;
                        if (0 > index) throw g.ExceptionFactory.createAssertionError("E#remove: invalid child");
                        this.children[index].parent = void 0, this.children.splice(index, 1), (e._touchable || e._hasTouchableChildren) && (this._findTouchableChildren(this) || (this._hasTouchableChildren = !1, 
                        this._disableTouchPropagation())), this.modified(!0);
                    }, E.prototype.destroy = function() {
                        if (this.parent && this.remove(), this.children) {
                            for (;this.children.length; ) this.children[this.children.length - 1].destroy();
                            this.children = void 0;
                        }
                        this._update && (this._update.destroy(), this._update = void 0), this._message && (this._message.destroy(), 
                        this._message = void 0), this._pointDown && (this._pointDown.destroy(), this._pointDown = void 0), 
                        this._pointMove && (this._pointMove.destroy(), this._pointMove = void 0), this._pointUp && (this._pointUp.destroy(), 
                        this._pointUp = void 0), this.scene.unregister(this);
                    }, E.prototype.destroyed = function() {
                        return void 0 === this.scene;
                    }, E.prototype.modified = function(isBubbling) {
                        this._matrix && (this._matrix._modified = !0), this.angle || 1 !== this.scaleX || 1 !== this.scaleY || 1 !== this.opacity || void 0 !== this.compositeOperation || void 0 !== this.shaderProgram ? this.state &= -9 : this.state |= 8, 
                        4 & this.state || (this.state |= 4, this.parent && this.parent.modified(!0));
                    }, E.prototype.shouldFindChildrenByPoint = function(point) {
                        return !0;
                    }, E.prototype.findPointSourceByPoint = function(point, m, force, camera) {
                        if (!(1 & this.state)) {
                            var cams = this._targetCameras;
                            if (!(cams && cams.length > 0) || camera && -1 !== cams.indexOf(camera)) {
                                m = m ? m.multiplyNew(this.getMatrix()) : this.getMatrix().clone();
                                var p = m.multiplyInverseForPoint(point);
                                if ((this._hasTouchableChildren || force && this.children && this.children.length) && this.shouldFindChildrenByPoint(p)) for (var i = this.children.length - 1; i >= 0; --i) {
                                    var child = this.children[i];
                                    if (force || child._touchable || child._hasTouchableChildren) {
                                        var target = child.findPointSourceByPoint(point, m, force, camera);
                                        if (target) return target;
                                    }
                                }
                                if (force || this._touchable) return 0 <= p.x && this.width > p.x && 0 <= p.y && this.height > p.y ? {
                                    target: this,
                                    point: p
                                } : void 0;
                            }
                        }
                    }, E.prototype.visible = function() {
                        return 1 !== (1 & this.state);
                    }, E.prototype.show = function() {
                        1 & this.state && (this.state &= -2, this.parent && this.parent.modified(!0));
                    }, E.prototype.hide = function() {
                        1 & this.state || (this.state |= 1, this.parent && this.parent.modified(!0));
                    }, E.prototype.calculateBoundingRect = function(c) {
                        return this._calculateBoundingRect(void 0, c);
                    }, E.prototype._calculateBoundingRect = function(m, c) {
                        var matrix = this.getMatrix();
                        if (m && (matrix = m.multiplyNew(matrix)), this.visible() && (!c || this._targetCameras && -1 !== this._targetCameras.indexOf(c))) {
                            for (var thisBoundingRect = {
                                left: 0,
                                right: this.width,
                                top: 0,
                                bottom: this.height
                            }, targetCoordinates = [ {
                                x: thisBoundingRect.left,
                                y: thisBoundingRect.top
                            }, {
                                x: thisBoundingRect.left,
                                y: thisBoundingRect.bottom
                            }, {
                                x: thisBoundingRect.right,
                                y: thisBoundingRect.top
                            }, {
                                x: thisBoundingRect.right,
                                y: thisBoundingRect.bottom
                            } ], convertedPoint = matrix.multiplyPoint(targetCoordinates[0]), result = {
                                left: convertedPoint.x,
                                right: convertedPoint.x,
                                top: convertedPoint.y,
                                bottom: convertedPoint.y
                            }, i = 1; i < targetCoordinates.length; ++i) convertedPoint = matrix.multiplyPoint(targetCoordinates[i]), 
                            result.left > convertedPoint.x && (result.left = convertedPoint.x), result.right < convertedPoint.x && (result.right = convertedPoint.x), 
                            result.top > convertedPoint.y && (result.top = convertedPoint.y), result.bottom < convertedPoint.y && (result.bottom = convertedPoint.y);
                            if (void 0 !== this.children) for (var i = 0; i < this.children.length; ++i) {
                                var nowResult = this.children[i]._calculateBoundingRect(matrix, c);
                                nowResult && (result.left > nowResult.left && (result.left = nowResult.left), result.right < nowResult.right && (result.right = nowResult.right), 
                                result.top > nowResult.top && (result.top = nowResult.top), result.bottom < nowResult.bottom && (result.bottom = nowResult.bottom));
                            }
                            return result;
                        }
                    }, E.prototype._enableTouchPropagation = function() {
                        for (var p = this.parent; p instanceof E && !p._hasTouchableChildren; ) p._hasTouchableChildren = !0, 
                        p = p.parent;
                    }, E.prototype._disableTouchPropagation = function() {
                        for (var p = this.parent; p instanceof E && p._hasTouchableChildren && !this._findTouchableChildren(p); ) p._hasTouchableChildren = !1, 
                        p = p.parent;
                    }, E.prototype._isTargetOperation = function(e) {
                        return 1 & this.state ? !1 : e instanceof g.PointEvent ? this._touchable && e.target === this : !1;
                    }, E.prototype._findTouchableChildren = function(e) {
                        if (e.children) for (var i = 0; i < e.children.length; ++i) {
                            if (e.children[i].touchable) return e.children[i];
                            var tmp = this._findTouchableChildren(e.children[i]);
                            if (tmp) return tmp;
                        }
                    }, E;
                }(g.Object2D);
                g.E = E;
            }(g || (g = {}));
            var g;
            !function(g) {
                var CacheableE = function(_super) {
                    function CacheableE(param) {
                        var _this = _super.call(this, param) || this;
                        return _this._shouldRenderChildren = !0, _this._cache = void 0, _this._renderer = void 0, 
                        _this._renderedCamera = void 0, _this;
                    }
                    return __extends(CacheableE, _super), CacheableE.prototype.invalidate = function() {
                        this.state &= -3, this.modified();
                    }, CacheableE.prototype.renderSelf = function(renderer, camera) {
                        var padding = CacheableE.PADDING;
                        if (this._renderedCamera !== camera && (this.state &= -3, this._renderedCamera = camera), 
                        !(2 & this.state)) {
                            this._cacheSize = this.calculateCacheSize();
                            var w = Math.ceil(this._cacheSize.width) + 2 * padding, h = Math.ceil(this._cacheSize.height) + 2 * padding, isNew = !this._cache || this._cache.width < w || this._cache.height < h;
                            isNew && (this._cache && !this._cache.destroyed() && this._cache.destroy(), this._cache = this.scene.game.resourceFactory.createSurface(w, h), 
                            this._renderer = this._cache.renderer());
                            var cacheRenderer = this._renderer;
                            cacheRenderer.begin(), isNew || cacheRenderer.clear(), cacheRenderer.save(), cacheRenderer.translate(padding, padding), 
                            this.renderCache(cacheRenderer, camera), cacheRenderer.restore(), this.state |= 2, 
                            cacheRenderer.end();
                        }
                        return this._cache && this._cacheSize.width > 0 && this._cacheSize.height > 0 && (renderer.translate(-padding, -padding), 
                        this.renderSelfFromCache(renderer), renderer.translate(padding, padding)), this._shouldRenderChildren;
                    }, CacheableE.prototype.renderSelfFromCache = function(renderer) {
                        renderer.drawImage(this._cache, 0, 0, this._cacheSize.width + CacheableE.PADDING, this._cacheSize.height + CacheableE.PADDING, 0, 0);
                    }, CacheableE.prototype.destroy = function() {
                        this._cache && !this._cache.destroyed() && this._cache.destroy(), this._cache = void 0, 
                        _super.prototype.destroy.call(this);
                    }, CacheableE.prototype.calculateCacheSize = function() {
                        return {
                            width: this.width,
                            height: this.height
                        };
                    }, CacheableE;
                }(g.E);
                CacheableE.PADDING = 1, g.CacheableE = CacheableE;
            }(g || (g = {}));
            var g;
            !function(g) {
                var StorageRegion;
                !function(StorageRegion) {
                    StorageRegion[StorageRegion.Slots = 1] = "Slots", StorageRegion[StorageRegion.Scores = 2] = "Scores", 
                    StorageRegion[StorageRegion.Counts = 3] = "Counts", StorageRegion[StorageRegion.Values = 4] = "Values";
                }(StorageRegion = g.StorageRegion || (g.StorageRegion = {}));
                var StorageOrder;
                !function(StorageOrder) {
                    StorageOrder[StorageOrder.Asc = 0] = "Asc", StorageOrder[StorageOrder.Desc = 1] = "Desc";
                }(StorageOrder = g.StorageOrder || (g.StorageOrder = {}));
                var StorageCondition;
                !function(StorageCondition) {
                    StorageCondition[StorageCondition.Equal = 1] = "Equal", StorageCondition[StorageCondition.GreaterThan = 2] = "GreaterThan", 
                    StorageCondition[StorageCondition.LessThan = 3] = "LessThan";
                }(StorageCondition = g.StorageCondition || (g.StorageCondition = {}));
                var StorageCountsOperation;
                !function(StorageCountsOperation) {
                    StorageCountsOperation[StorageCountsOperation.Incr = 1] = "Incr", StorageCountsOperation[StorageCountsOperation.Decr = 2] = "Decr";
                }(StorageCountsOperation = g.StorageCountsOperation || (g.StorageCountsOperation = {}));
                var StorageValueStore = function() {
                    function StorageValueStore(keys, values) {
                        this._keys = keys, this._values = values;
                    }
                    return StorageValueStore.prototype.get = function(keyOrIndex) {
                        if (void 0 === this._values) return [];
                        if ("number" == typeof keyOrIndex) return this._values[keyOrIndex];
                        var index = this._keys.indexOf(keyOrIndex);
                        if (-1 !== index) return this._values[index];
                        for (var i = 0; i < this._keys.length; ++i) {
                            var target = this._keys[i];
                            if (target.region === keyOrIndex.region && target.regionKey === keyOrIndex.regionKey && target.userId === keyOrIndex.userId && target.gameId === keyOrIndex.gameId) return this._values[i];
                        }
                        return [];
                    }, StorageValueStore.prototype.getOne = function(keyOrIndex) {
                        var values = this.get(keyOrIndex);
                        if (values) return values[0];
                    }, StorageValueStore;
                }();
                g.StorageValueStore = StorageValueStore;
                var StorageLoader = function() {
                    function StorageLoader(storage, keys, serialization) {
                        this._loaded = !1, this._storage = storage, this._valueStore = new StorageValueStore(keys), 
                        this._handler = void 0, this._valueStoreSerialization = serialization;
                    }
                    return StorageLoader.prototype._load = function(handler) {
                        this._handler = handler, this._storage._load && this._storage._load.call(this._storage, this._valueStore._keys, this, this._valueStoreSerialization);
                    }, StorageLoader.prototype._onLoaded = function(values, serialization) {
                        this._valueStore._values = values, this._loaded = !0, serialization && (this._valueStoreSerialization = serialization), 
                        this._handler && this._handler._onStorageLoaded();
                    }, StorageLoader.prototype._onError = function(error) {
                        this._handler && this._handler._onStorageLoadError(error);
                    }, StorageLoader;
                }();
                g.StorageLoader = StorageLoader;
                var Storage = function() {
                    function Storage(game) {
                        this._game = game;
                    }
                    return Storage.prototype.write = function(key, value, option) {
                        this._write && this._write(key, value, option);
                    }, Storage.prototype.requestValuesForJoinPlayer = function(keys) {
                        this._requestedKeysForJoinPlayer = keys;
                    }, Storage.prototype._createLoader = function(keys, serialization) {
                        return new StorageLoader(this, keys, serialization);
                    }, Storage.prototype._registerWrite = function(write) {
                        this._write = write;
                    }, Storage.prototype._registerLoad = function(load) {
                        this._load = load;
                    }, Storage;
                }();
                g.Storage = Storage;
            }(g || (g = {}));
            var g;
            !function(g) {
                var SceneAssetHolder = function() {
                    function SceneAssetHolder(param) {
                        this.waitingAssetsCount = param.assetIds.length, this._scene = param.scene, this._assetManager = param.assetManager, 
                        this._assetIds = param.assetIds, this._assets = [], this._handler = param.handler, 
                        this._handlerOwner = param.handlerOwner || null, this._direct = !!param.direct, 
                        this._requested = !1;
                    }
                    return SceneAssetHolder.prototype.request = function() {
                        return 0 === this.waitingAssetsCount ? !1 : this._requested ? !0 : (this._requested = !0, 
                        this._assetManager.requestAssets(this._assetIds, this), !0);
                    }, SceneAssetHolder.prototype.destroy = function() {
                        this._requested && this._assetManager.unrefAssets(this._assets), this.waitingAssetsCount = 0, 
                        this._scene = void 0, this._assetIds = void 0, this._handler = void 0, this._requested = !1;
                    }, SceneAssetHolder.prototype.destroyed = function() {
                        return !this._scene;
                    }, SceneAssetHolder.prototype.callHandler = function() {
                        this._handler.call(this._handlerOwner);
                    }, SceneAssetHolder.prototype._onAssetError = function(asset, error, assetManager) {
                        if (!this.destroyed() && !this._scene.destroyed()) {
                            var failureInfo = {
                                asset: asset,
                                error: error,
                                cancelRetry: !1
                            };
                            this._scene.assetLoadFailed.fire(failureInfo), error.retriable && !failureInfo.cancelRetry ? this._assetManager.retryLoad(asset) : this._assetManager.configuration[asset.id] && this._scene.game.terminateGame(), 
                            this._scene.assetLoadCompleted.fire(asset);
                        }
                    }, SceneAssetHolder.prototype._onAssetLoad = function(asset) {
                        if (!this.destroyed() && !this._scene.destroyed()) {
                            if (this._scene.assets[asset.id] = asset, this._scene.assetLoaded.fire(asset), this._scene.assetLoadCompleted.fire(asset), 
                            this._assets.push(asset), --this.waitingAssetsCount, this.waitingAssetsCount < 0) throw g.ExceptionFactory.createAssertionError("SceneAssetHolder#_onAssetLoad: broken waitingAssetsCount");
                            this.waitingAssetsCount > 0 || (this._direct ? this.callHandler() : this._scene.game._callSceneAssetHolderHandler(this));
                        }
                    }, SceneAssetHolder;
                }();
                g.SceneAssetHolder = SceneAssetHolder;
                var SceneState;
                !function(SceneState) {
                    SceneState[SceneState.Destroyed = 0] = "Destroyed", SceneState[SceneState.Standby = 1] = "Standby", 
                    SceneState[SceneState.Active = 2] = "Active", SceneState[SceneState.Deactive = 3] = "Deactive", 
                    SceneState[SceneState.BeforeDestroyed = 4] = "BeforeDestroyed";
                }(SceneState = g.SceneState || (g.SceneState = {}));
                var SceneLoadState;
                !function(SceneLoadState) {
                    SceneLoadState[SceneLoadState.Initial = 0] = "Initial", SceneLoadState[SceneLoadState.Ready = 1] = "Ready", 
                    SceneLoadState[SceneLoadState.ReadyFired = 2] = "ReadyFired", SceneLoadState[SceneLoadState.LoadedFired = 3] = "LoadedFired";
                }(SceneLoadState = g.SceneLoadState || (g.SceneLoadState = {}));
                var Scene = function() {
                    function Scene(param) {
                        var game, local, tickGenerationMode, assetIds;
                        game = param.game, assetIds = param.assetIds, param.storageKeys ? (this._storageLoader = game.storage._createLoader(param.storageKeys, param.storageValuesSerialization), 
                        this.storageValues = this._storageLoader._valueStore) : (this._storageLoader = void 0, 
                        this.storageValues = void 0), local = void 0 === param.local ? g.LocalTickMode.NonLocal : param.local === !1 ? g.LocalTickMode.NonLocal : param.local === !0 ? g.LocalTickMode.FullLocal : param.local, 
                        tickGenerationMode = void 0 !== param.tickGenerationMode ? param.tickGenerationMode : g.TickGenerationMode.ByClock, 
                        this.name = param.name, assetIds || (assetIds = []), this.game = game, this.local = local, 
                        this.tickGenerationMode = tickGenerationMode, this.loaded = new g.Trigger(), this._ready = new g.Trigger(), 
                        this.assets = {}, this._loaded = !1, this._prefetchRequested = !1, this._loadingState = SceneLoadState.Initial, 
                        this.update = new g.Trigger(), this._timer = new g.TimerManager(this.update, this.game.fps), 
                        this.assetLoaded = new g.Trigger(), this.assetLoadFailed = new g.Trigger(), this.assetLoadCompleted = new g.Trigger(), 
                        this.message = new g.Trigger(), this.pointDownCapture = new g.Trigger(), this.pointMoveCapture = new g.Trigger(), 
                        this.pointUpCapture = new g.Trigger(), this.operation = new g.Trigger(), this.children = [], 
                        this.state = SceneState.Standby, this.stateChanged = new g.Trigger(), this._assetHolders = [], 
                        this._sceneAssetHolder = new SceneAssetHolder({
                            scene: this,
                            assetManager: this.game._assetManager,
                            assetIds: assetIds,
                            handler: this._onSceneAssetsLoad,
                            handlerOwner: this,
                            direct: !0
                        });
                    }
                    return Scene.prototype.modified = function(isBubbling) {
                        this.game.modified = !0;
                    }, Scene.prototype.destroy = function() {
                        this.state = SceneState.BeforeDestroyed, this.stateChanged.fire(this.state);
                        var gameDb = this.game.db;
                        for (var p in gameDb) gameDb.hasOwnProperty(p) && gameDb[p].scene === this && gameDb[p].destroy();
                        var gameDb = this.game._localDb;
                        for (var p in gameDb) gameDb.hasOwnProperty(p) && gameDb[p].scene === this && gameDb[p].destroy();
                        this._timer.destroy(), this.update.destroy(), this.message.destroy(), this.pointDownCapture.destroy(), 
                        this.pointMoveCapture.destroy(), this.pointUpCapture.destroy(), this.operation.destroy(), 
                        this.loaded.destroy(), this.assetLoaded.destroy(), this.assetLoadFailed.destroy(), 
                        this.assetLoadCompleted.destroy(), this.assets = {};
                        for (var i = 0; i < this._assetHolders.length; ++i) this._assetHolders[i].destroy();
                        this._sceneAssetHolder.destroy(), this._storageLoader = void 0, this.game = void 0, 
                        this.state = SceneState.Destroyed, this.stateChanged.fire(this.state), this.stateChanged.destroy();
                    }, Scene.prototype.destroyed = function() {
                        return void 0 === this.game;
                    }, Scene.prototype.createTimer = function(interval) {
                        return this._timer.createTimer(interval);
                    }, Scene.prototype.deleteTimer = function(timer) {
                        this._timer.deleteTimer(timer);
                    }, Scene.prototype.setInterval = function(handler, interval, owner) {
                        var t = this._timer;
                        return "number" == typeof handler ? (this.game.logger.warn("[deprecated] Scene#setInterval(): this arguments ordering is now deprecated. Specify the function first."), 
                        null != owner ? t.setInterval(owner, handler, interval) : t.setInterval(interval, handler, null)) : t.setInterval(handler, interval, owner);
                    }, Scene.prototype.clearInterval = function(identifier) {
                        this._timer.clearInterval(identifier);
                    }, Scene.prototype.setTimeout = function(handler, milliseconds, owner) {
                        var t = this._timer;
                        return "number" == typeof handler ? (this.game.logger.warn("[deprecated] Scene#setTimeout(): this arguments ordering is now deprecated. Specify the function first."), 
                        null != owner ? t.setTimeout(owner, handler, milliseconds) : t.setTimeout(milliseconds, handler, null)) : t.setTimeout(handler, milliseconds, owner);
                    }, Scene.prototype.clearTimeout = function(identifier) {
                        this._timer.clearTimeout(identifier);
                    }, Scene.prototype.isCurrentScene = function() {
                        return this.game.scene() === this;
                    }, Scene.prototype.gotoScene = function(next, toPush) {
                        if (!this.isCurrentScene()) throw g.ExceptionFactory.createAssertionError("Scene#gotoScene: this scene is not the current scene");
                        toPush ? this.game.pushScene(next) : this.game.replaceScene(next);
                    }, Scene.prototype.end = function() {
                        if (!this.isCurrentScene()) throw g.ExceptionFactory.createAssertionError("Scene#end: this scene is not the current scene");
                        this.game.popScene();
                    }, Scene.prototype.register = function(e) {
                        this.game.register(e), e.scene = this;
                    }, Scene.prototype.unregister = function(e) {
                        e.scene = void 0, this.game.unregister(e);
                    }, Scene.prototype.append = function(e) {
                        this.insertBefore(e, void 0);
                    }, Scene.prototype.insertBefore = function(e, target) {
                        e.parent && e.remove(), e.parent = this;
                        var index = -1;
                        void 0 !== target && (index = this.children.indexOf(target)) > -1 ? this.children.splice(index, 0, e) : this.children.push(e), 
                        this.modified(!0);
                    }, Scene.prototype.remove = function(e) {
                        var index = this.children.indexOf(e);
                        -1 !== index && (this.children[index].parent = void 0, this.children.splice(index, 1), 
                        this.modified(!0));
                    }, Scene.prototype.findPointSourceByPoint = function(point, force, camera) {
                        var mayConsumeLocalTick = this.local !== g.LocalTickMode.NonLocal, children = this.children, m = void 0;
                        camera && camera instanceof g.Camera2D && (m = camera.getMatrix());
                        for (var i = children.length - 1; i >= 0; --i) {
                            var ret = children[i].findPointSourceByPoint(point, m, force, camera);
                            if (ret) return ret.local = ret.target.local || mayConsumeLocalTick, ret;
                        }
                        return {
                            target: void 0,
                            point: void 0,
                            local: mayConsumeLocalTick
                        };
                    }, Scene.prototype.prefetch = function() {
                        this._loaded || this._prefetchRequested || (this._prefetchRequested = !0, this._sceneAssetHolder.request());
                    }, Scene.prototype.serializeStorageValues = function() {
                        return this._storageLoader ? this._storageLoader._valueStoreSerialization : void 0;
                    }, Scene.prototype.requestAssets = function(assetIds, handler) {
                        if (this._loadingState < SceneLoadState.ReadyFired) throw g.ExceptionFactory.createAssertionError("Scene#requestAsset(): can be called after loaded.");
                        var holder = new SceneAssetHolder({
                            scene: this,
                            assetManager: this.game._assetManager,
                            assetIds: assetIds,
                            handler: handler
                        });
                        this._assetHolders.push(holder), holder.request();
                    }, Scene.prototype._activate = function() {
                        this.state = SceneState.Active, this.stateChanged.fire(this.state);
                    }, Scene.prototype._deactivate = function() {
                        this.state = SceneState.Deactive, this.stateChanged.fire(this.state);
                    }, Scene.prototype._needsLoading = function() {
                        return this._sceneAssetHolder.waitingAssetsCount > 0 || this._storageLoader && !this._storageLoader._loaded;
                    }, Scene.prototype._load = function() {
                        if (!this._loaded) {
                            this._loaded = !0;
                            var needsWait = this._sceneAssetHolder.request();
                            this._storageLoader && (this._storageLoader._load(this), needsWait = !0), needsWait || this._notifySceneReady();
                        }
                    }, Scene.prototype._onSceneAssetsLoad = function() {
                        this._loaded && (!this._storageLoader || this._storageLoader._loaded) && this._notifySceneReady();
                    }, Scene.prototype._onStorageLoadError = function(error) {
                        this.game.terminateGame();
                    }, Scene.prototype._onStorageLoaded = function() {
                        0 === this._sceneAssetHolder.waitingAssetsCount && this._notifySceneReady();
                    }, Scene.prototype._notifySceneReady = function() {
                        this._loadingState = SceneLoadState.Ready, this.game._fireSceneReady(this);
                    }, Scene.prototype._fireReady = function() {
                        this._ready.fire(this), this._loadingState = SceneLoadState.ReadyFired;
                    }, Scene.prototype._fireLoaded = function() {
                        this.loaded.fire(this), this._loadingState = SceneLoadState.LoadedFired;
                    }, Scene;
                }();
                g.Scene = Scene;
            }(g || (g = {}));
            var g;
            !function(g) {
                var LoadingScene = function(_super) {
                    function LoadingScene(param) {
                        var _this = this;
                        return param.local = !0, _this = _super.call(this, param) || this, _this.targetReset = new g.Trigger(), 
                        _this.targetReady = new g.Trigger(), _this.targetAssetLoaded = new g.Trigger(), 
                        _this._explicitEnd = !!param.explicitEnd, _this._targetScene = void 0, _this;
                    }
                    return __extends(LoadingScene, _super), LoadingScene.prototype.destroy = function() {
                        this._clearTargetScene(), _super.prototype.destroy.call(this);
                    }, LoadingScene.prototype.reset = function(targetScene) {
                        this._clearTargetScene(), this._targetScene = targetScene, this._loadingState < g.SceneLoadState.LoadedFired ? this.loaded.addOnce(this._doReset, this) : this._doReset();
                    }, LoadingScene.prototype.getTargetWaitingAssetsCount = function() {
                        return this._targetScene ? this._targetScene._sceneAssetHolder.waitingAssetsCount : 0;
                    }, LoadingScene.prototype.end = function() {
                        if (!this._targetScene || this._targetScene._loadingState < g.SceneLoadState.Ready) {
                            var state = this._targetScene ? g.SceneLoadState[this._targetScene._loadingState] : "(no scene)", msg = "LoadingScene#end(): the target scene is in invalid state: " + state;
                            throw g.ExceptionFactory.createAssertionError(msg);
                        }
                        this.game.popScene(!0), this.game._fireSceneLoaded(this._targetScene), this._clearTargetScene();
                    }, LoadingScene.prototype._clearTargetScene = function() {
                        this._targetScene && (this._targetScene._ready.removeAll({
                            owner: this
                        }), this._targetScene.assetLoaded.removeAll({
                            owner: this
                        }), this._targetScene = void 0);
                    }, LoadingScene.prototype._doReset = function() {
                        this.targetReset.fire(this._targetScene), this._targetScene._loadingState < g.SceneLoadState.ReadyFired ? (this._targetScene._ready.add(this._fireTriggerOnTargetReady, this), 
                        this._targetScene.assetLoaded.add(this._fireTriggerOnTargetAssetLoad, this), this._targetScene._load()) : this._fireTriggerOnTargetReady(this._targetScene);
                    }, LoadingScene.prototype._fireTriggerOnTargetAssetLoad = function(asset) {
                        this.targetAssetLoaded.fire(asset);
                    }, LoadingScene.prototype._fireTriggerOnTargetReady = function(scene) {
                        this.targetReady.fire(scene), this._explicitEnd || this.end();
                    }, LoadingScene;
                }(g.Scene);
                g.LoadingScene = LoadingScene;
            }(g || (g = {}));
            var g;
            !function(g) {
                var CameraCancellingE = function(_super) {
                    function CameraCancellingE(param) {
                        var _this = _super.call(this, param) || this;
                        return _this._canceller = new g.Object2D(), _this;
                    }
                    return __extends(CameraCancellingE, _super), CameraCancellingE.prototype.renderSelf = function(renderer, camera) {
                        if (!this.children) return !1;
                        if (camera) {
                            var c = camera, canceller = this._canceller;
                            (c.x !== canceller.x || c.y !== canceller.y || c.angle !== canceller.angle || c.scaleX !== canceller.scaleX || c.scaleY !== canceller.scaleY) && (canceller.x = c.x, 
                            canceller.y = c.y, canceller.angle = c.angle, canceller.scaleX = c.scaleX, canceller.scaleY = c.scaleY, 
                            canceller._matrix && (canceller._matrix._modified = !0)), renderer.save(), renderer.transform(canceller.getMatrix()._matrix);
                        }
                        for (var children = this.children, i = 0; i < children.length; ++i) children[i].render(renderer, camera);
                        return camera && renderer.restore(), !1;
                    }, CameraCancellingE;
                }(g.E), DefaultLoadingScene = function(_super) {
                    function DefaultLoadingScene(param) {
                        var _this = _super.call(this, {
                            game: param.game,
                            name: "akashic:default-loading-scene"
                        }) || this;
                        return _this._barWidth = Math.min(param.game.width, Math.max(100, param.game.width / 2)), 
                        _this._barHeight = 5, _this._gauge = void 0, _this._gaugeUpdateCount = 0, _this._totalWaitingAssetCount = 0, 
                        _this.loaded.handle(_this, _this._onLoaded), _this.targetReset.handle(_this, _this._onTargetReset), 
                        _this.targetAssetLoaded.handle(_this, _this._onTargetAssetLoaded), _this;
                    }
                    return __extends(DefaultLoadingScene, _super), DefaultLoadingScene.prototype._onLoaded = function() {
                        var gauge;
                        return this.append(new CameraCancellingE({
                            scene: this,
                            children: [ new g.FilledRect({
                                scene: this,
                                width: this.game.width,
                                height: this.game.height,
                                cssColor: "rgba(0, 0, 0, 0.8)",
                                children: [ new g.FilledRect({
                                    scene: this,
                                    x: (this.game.width - this._barWidth) / 2,
                                    y: (this.game.height - this._barHeight) / 2,
                                    width: this._barWidth,
                                    height: this._barHeight,
                                    cssColor: "gray",
                                    children: [ gauge = new g.FilledRect({
                                        scene: this,
                                        width: 0,
                                        height: this._barHeight,
                                        cssColor: "white"
                                    }) ]
                                }) ]
                            }) ]
                        })), gauge.update.handle(this, this._onUpdateGuage), this._gauge = gauge, !0;
                    }, DefaultLoadingScene.prototype._onUpdateGuage = function() {
                        var BLINK_RANGE = 50, BLINK_PER_SEC = 2 / 3;
                        ++this._gaugeUpdateCount;
                        var c = Math.round(255 - BLINK_RANGE + Math.sin(this._gaugeUpdateCount / this.game.fps * BLINK_PER_SEC * (2 * Math.PI)) * BLINK_RANGE);
                        this._gauge.cssColor = "rgb(" + c + "," + c + "," + c + ")", this._gauge.modified();
                    }, DefaultLoadingScene.prototype._onTargetReset = function(targetScene) {
                        this._gauge && (this._gauge.width = 0, this._gauge.modified()), this._totalWaitingAssetCount = targetScene._sceneAssetHolder.waitingAssetsCount;
                    }, DefaultLoadingScene.prototype._onTargetAssetLoaded = function(asset) {
                        var waitingAssetsCount = this._targetScene._sceneAssetHolder.waitingAssetsCount;
                        this._gauge.width = Math.ceil((1 - waitingAssetsCount / this._totalWaitingAssetCount) * this._barWidth), 
                        this._gauge.modified();
                    }, DefaultLoadingScene;
                }(g.LoadingScene);
                g.DefaultLoadingScene = DefaultLoadingScene;
            }(g || (g = {}));
            var g;
            !function(g) {
                var Sprite = function(_super) {
                    function Sprite(param) {
                        var _this = _super.call(this, param) || this;
                        return _this.surface = g.Util.asSurface(param.src), "width" in param || (_this.width = _this.surface.width), 
                        "height" in param || (_this.height = _this.surface.height), _this.srcWidth = "srcWidth" in param ? param.srcWidth : _this.width, 
                        _this.srcHeight = "srcHeight" in param ? param.srcHeight : _this.height, _this.srcX = param.srcX || 0, 
                        _this.srcY = param.srcY || 0, _this._stretchMatrix = void 0, _this._beforeSurface = _this.surface, 
                        g.Util.setupAnimatingHandler(_this, _this.surface), _this._invalidateSelf(), _this;
                    }
                    return __extends(Sprite, _super), Sprite.prototype._onUpdate = function() {
                        this.modified();
                    }, Sprite.prototype._onAnimatingStarted = function() {
                        this.update.contains(this._onUpdate, this) || this.update.add(this._onUpdate, this);
                    }, Sprite.prototype._onAnimatingStopped = function() {
                        this.destroyed() || this.update.remove(this._onUpdate, this);
                    }, Sprite.prototype.renderSelf = function(renderer, camera) {
                        return this.srcWidth <= 0 || this.srcHeight <= 0 ? !0 : (this._stretchMatrix && (renderer.save(), 
                        renderer.transform(this._stretchMatrix._matrix)), renderer.drawImage(this.surface, this.srcX, this.srcY, this.srcWidth, this.srcHeight, 0, 0), 
                        this._stretchMatrix && renderer.restore(), !0);
                    }, Sprite.prototype.invalidate = function() {
                        this._invalidateSelf(), this.modified();
                    }, Sprite.prototype.destroy = function(destroySurface) {
                        this.surface && !this.surface.destroyed() && (destroySurface ? this.surface.destroy() : this.surface.isDynamic && (this.surface.animatingStarted.remove(this._onAnimatingStarted, this), 
                        this.surface.animatingStopped.remove(this._onAnimatingStopped, this))), this.surface = void 0, 
                        _super.prototype.destroy.call(this);
                    }, Sprite.prototype._invalidateSelf = function() {
                        this.width === this.srcWidth && this.height === this.srcHeight ? this._stretchMatrix = void 0 : (this._stretchMatrix = g.Util.createMatrix(), 
                        this._stretchMatrix.scale(this.width / this.srcWidth, this.height / this.srcHeight)), 
                        this.surface !== this._beforeSurface && (g.Util.migrateAnimatingHandler(this, this._beforeSurface, this.surface), 
                        this._beforeSurface = this.surface);
                    }, Sprite;
                }(g.E);
                g.Sprite = Sprite;
            }(g || (g = {}));
            var g;
            !function(g) {
                var FrameSprite = function(_super) {
                    function FrameSprite(param) {
                        var _this = _super.call(this, param) || this;
                        return _this._lastUsedIndex = 0, _this.frameNumber = param.frameNumber || 0, _this.frames = "frames" in param ? param.frames : [ 0 ], 
                        _this.interval = param.interval, _this._timer = void 0, _this.loop = null != param.loop ? param.loop : !0, 
                        _this.finished = new g.Trigger(), _this._modifiedSelf(), _this;
                    }
                    return __extends(FrameSprite, _super), FrameSprite.createBySprite = function(sprite, width, height) {
                        var frameSprite = new FrameSprite({
                            scene: sprite.scene,
                            src: sprite.surface,
                            width: void 0 === width ? sprite.width : width,
                            height: void 0 === height ? sprite.height : height
                        });
                        return frameSprite.srcHeight = void 0 === height ? sprite.srcHeight : height, frameSprite.srcWidth = void 0 === width ? sprite.srcWidth : width, 
                        frameSprite;
                    }, FrameSprite.prototype.start = function() {
                        void 0 === this.interval && (this.interval = 1e3 / this.game().fps), this._timer && this._free(), 
                        this._timer = this.scene.createTimer(this.interval), this._timer.elapsed.add(this._onElapsed, this);
                    }, FrameSprite.prototype.destroy = function(destroySurface) {
                        this.stop(), _super.prototype.destroy.call(this, destroySurface);
                    }, FrameSprite.prototype.stop = function() {
                        this._timer && this._free();
                    }, FrameSprite.prototype.modified = function(isBubbling) {
                        this._modifiedSelf(isBubbling), _super.prototype.modified.call(this, isBubbling);
                    }, FrameSprite.prototype._onElapsed = function() {
                        this.frameNumber === this.frames.length - 1 ? this.loop ? this.frameNumber = 0 : (this.stop(), 
                        this.finished.fire()) : this.frameNumber++, this.modified();
                    }, FrameSprite.prototype._free = function() {
                        this._timer && (this._timer.elapsed.remove(this._onElapsed, this), this._timer.canDelete() && this.scene.deleteTimer(this._timer), 
                        this._timer = void 0);
                    }, FrameSprite.prototype._changeFrame = function() {
                        var frame = this.frames[this.frameNumber], sep = Math.floor(this.surface.width / this.srcWidth);
                        this.srcX = frame % sep * this.srcWidth, this.srcY = Math.floor(frame / sep) * this.srcHeight, 
                        this._lastUsedIndex = frame;
                    }, FrameSprite.prototype._modifiedSelf = function(isBubbling) {
                        this._lastUsedIndex !== this.frames[this.frameNumber] && this._changeFrame();
                    }, FrameSprite;
                }(g.Sprite);
                g.FrameSprite = FrameSprite;
            }(g || (g = {}));
            var g;
            !function(g) {
                var EventType;
                !function(EventType) {
                    EventType[EventType.Unknown = 0] = "Unknown", EventType[EventType.Join = 1] = "Join", 
                    EventType[EventType.Leave = 2] = "Leave", EventType[EventType.Timestamp = 3] = "Timestamp", 
                    EventType[EventType.Seed = 4] = "Seed", EventType[EventType.PointDown = 5] = "PointDown", 
                    EventType[EventType.PointMove = 6] = "PointMove", EventType[EventType.PointUp = 7] = "PointUp", 
                    EventType[EventType.Message = 8] = "Message", EventType[EventType.Operation = 9] = "Operation";
                }(EventType = g.EventType || (g.EventType = {}));
                var PointEvent = function() {
                    function PointEvent(pointerId, target, point, player, local, priority) {
                        this.priority = priority, this.local = local, this.player = player, this.pointerId = pointerId, 
                        this.target = target, this.point = point;
                    }
                    return PointEvent;
                }();
                g.PointEvent = PointEvent;
                var PointDownEvent = function(_super) {
                    function PointDownEvent(pointerId, target, point, player, local, priority) {
                        var _this = _super.call(this, pointerId, target, point, player, local, priority) || this;
                        return _this.type = EventType.PointDown, _this;
                    }
                    return __extends(PointDownEvent, _super), PointDownEvent;
                }(PointEvent);
                g.PointDownEvent = PointDownEvent;
                var PointUpEvent = function(_super) {
                    function PointUpEvent(pointerId, target, point, prevDelta, startDelta, player, local, priority) {
                        var _this = _super.call(this, pointerId, target, point, player, local, priority) || this;
                        return _this.type = EventType.PointUp, _this.prevDelta = prevDelta, _this.startDelta = startDelta, 
                        _this;
                    }
                    return __extends(PointUpEvent, _super), PointUpEvent;
                }(PointEvent);
                g.PointUpEvent = PointUpEvent;
                var PointMoveEvent = function(_super) {
                    function PointMoveEvent(pointerId, target, point, prevDelta, startDelta, player, local, priority) {
                        var _this = _super.call(this, pointerId, target, point, player, local, priority) || this;
                        return _this.type = EventType.PointMove, _this.prevDelta = prevDelta, _this.startDelta = startDelta, 
                        _this;
                    }
                    return __extends(PointMoveEvent, _super), PointMoveEvent;
                }(PointEvent);
                g.PointMoveEvent = PointMoveEvent;
                var MessageEvent = function() {
                    function MessageEvent(data, player, local, priority) {
                        this.type = EventType.Message, this.priority = priority, this.local = local, this.player = player, 
                        this.data = data;
                    }
                    return MessageEvent;
                }();
                g.MessageEvent = MessageEvent;
                var OperationEvent = function() {
                    function OperationEvent(code, data, player, local, priority) {
                        this.type = EventType.Operation, this.priority = priority, this.local = local, this.player = player, 
                        this.code = code, this.data = data;
                    }
                    return OperationEvent;
                }();
                g.OperationEvent = OperationEvent;
                var JoinEvent = function() {
                    function JoinEvent(player, storageValues, priority) {
                        this.type = EventType.Join, this.priority = priority, this.player = player, this.storageValues = storageValues;
                    }
                    return JoinEvent;
                }();
                g.JoinEvent = JoinEvent;
                var LeaveEvent = function() {
                    function LeaveEvent(player, priority) {
                        this.type = EventType.Leave, this.priority = priority, this.player = player;
                    }
                    return LeaveEvent;
                }();
                g.LeaveEvent = LeaveEvent;
                var TimestampEvent = function() {
                    function TimestampEvent(timestamp, player, priority) {
                        this.type = EventType.Timestamp, this.priority = priority, this.player = player, 
                        this.timestamp = timestamp;
                    }
                    return TimestampEvent;
                }();
                g.TimestampEvent = TimestampEvent;
                var SeedEvent = function() {
                    function SeedEvent(generator, priority) {
                        this.type = EventType.Seed, this.priority = priority, this.generator = generator;
                    }
                    return SeedEvent;
                }();
                g.SeedEvent = SeedEvent;
            }(g || (g = {}));
            var g;
            !function(g) {
                var LogLevel;
                !function(LogLevel) {
                    LogLevel[LogLevel.Error = 0] = "Error", LogLevel[LogLevel.Warn = 1] = "Warn", LogLevel[LogLevel.Info = 2] = "Info", 
                    LogLevel[LogLevel.Debug = 3] = "Debug";
                }(LogLevel = g.LogLevel || (g.LogLevel = {}));
                var Logger = function() {
                    function Logger(game) {
                        this.game = game, this.logging = new g.Trigger();
                    }
                    return Logger.prototype.destroy = function() {
                        this.game = void 0, this.logging.destroy(), this.logging = void 0;
                    }, Logger.prototype.destroyed = function() {
                        return !this.game;
                    }, Logger.prototype.error = function(message, cause) {
                        this.logging.fire({
                            game: this.game,
                            level: LogLevel.Error,
                            message: message,
                            cause: cause
                        });
                    }, Logger.prototype.warn = function(message, cause) {
                        this.logging.fire({
                            game: this.game,
                            level: LogLevel.Warn,
                            message: message,
                            cause: cause
                        });
                    }, Logger.prototype.info = function(message, cause) {
                        this.logging.fire({
                            game: this.game,
                            level: LogLevel.Info,
                            message: message,
                            cause: cause
                        });
                    }, Logger.prototype.debug = function(message, cause) {
                        this.logging.fire({
                            game: this.game,
                            level: LogLevel.Debug,
                            message: message,
                            cause: cause
                        });
                    }, Logger;
                }();
                g.Logger = Logger;
            }(g || (g = {}));
            var g;
            !function(g) {
                var Game = function() {
                    function Game(gameConfiguration, resourceFactory, assetBase, selfId, operationPluginViewInfo) {
                        gameConfiguration = this._normalizeConfiguration(gameConfiguration), this.fps = gameConfiguration.fps, 
                        this.width = gameConfiguration.width, this.height = gameConfiguration.height, this.renderers = [], 
                        this.scenes = [], this.random = null, this.age = 0, this.assetBase = assetBase || "", 
                        this.resourceFactory = resourceFactory, this.selfId = selfId || void 0, this.playId = void 0, 
                        this._audioSystemManager = new g.AudioSystemManager(this), this.audio = {
                            music: new g.MusicAudioSystem("music", this),
                            sound: new g.SoundAudioSystem("sound", this)
                        }, this.defaultAudioSystemId = "sound", this.storage = new g.Storage(this), this.assets = {}, 
                        this.surfaceAtlasSet = void 0, this.join = new g.Trigger(), this.leave = new g.Trigger(), 
                        this.seed = new g.Trigger(), this._eventTriggerMap = {}, this._eventTriggerMap[g.EventType.Join] = this.join, 
                        this._eventTriggerMap[g.EventType.Leave] = this.leave, this._eventTriggerMap[g.EventType.Seed] = this.seed, 
                        this._eventTriggerMap[g.EventType.Message] = void 0, this._eventTriggerMap[g.EventType.PointDown] = void 0, 
                        this._eventTriggerMap[g.EventType.PointMove] = void 0, this._eventTriggerMap[g.EventType.PointUp] = void 0, 
                        this._eventTriggerMap[g.EventType.Operation] = void 0, this.resized = new g.Trigger(), 
                        this.skippingChanged = new g.Trigger(), this.isLastTickLocal = !0, this.lastOmittedLocalTickCount = 0, 
                        this._loaded = new g.Trigger(), this._started = new g.Trigger(), this.isLoaded = !1, 
                        this.snapshotRequest = new g.Trigger(), this.external = {}, this.logger = new g.Logger(this), 
                        this._main = gameConfiguration.main, this._mainParameter = void 0, this._configuration = gameConfiguration, 
                        this._assetManager = new g.AssetManager(this, gameConfiguration.assets, gameConfiguration.audio, gameConfiguration.moduleMainScripts);
                        var operationPluginsField = gameConfiguration.operationPlugins || [];
                        this._operationPluginManager = new g.OperationPluginManager(this, operationPluginViewInfo, operationPluginsField), 
                        this._operationPluginOperated = new g.Trigger(), this._operationPluginManager.operated.add(this._operationPluginOperated.fire, this._operationPluginOperated), 
                        this._sceneChanged = new g.Trigger(), this._sceneChanged.add(this._updateEventTriggers, this), 
                        this._initialScene = new g.Scene({
                            game: this,
                            assetIds: this._assetManager.globalAssetIds(),
                            local: !0,
                            name: "akashic:initial-scene"
                        }), this._initialScene.loaded.add(this._onInitialSceneLoaded, this), this._reset({
                            age: 0
                        });
                    }
                    return Object.defineProperty(Game.prototype, "focusingCamera", {
                        get: function() {
                            return this._focusingCamera;
                        },
                        set: function(c) {
                            c !== this._focusingCamera && (this.modified && this.render(this._focusingCamera), 
                            this._focusingCamera = c);
                        },
                        enumerable: !0,
                        configurable: !0
                    }), Game.prototype.pushScene = function(scene) {
                        this._sceneChangeRequests.push({
                            type: 0,
                            scene: scene
                        });
                    }, Game.prototype.replaceScene = function(scene, preserveCurrent) {
                        this._sceneChangeRequests.push({
                            type: 1,
                            scene: scene,
                            preserveCurrent: preserveCurrent
                        });
                    }, Game.prototype.popScene = function(preserveCurrent) {
                        this._sceneChangeRequests.push({
                            type: 2,
                            preserveCurrent: preserveCurrent
                        });
                    }, Game.prototype.scene = function() {
                        return this.scenes.length ? this.scenes[this.scenes.length - 1] : void 0;
                    }, Game.prototype.tick = function(advanceAge, omittedTickCount) {
                        var scene = void 0;
                        if (this._isTerminated) return !1;
                        if (this.isLastTickLocal = !advanceAge, this.lastOmittedLocalTickCount = omittedTickCount || 0, 
                        this.scenes.length) {
                            if (scene = this.scenes[this.scenes.length - 1], this.events.length) {
                                var events = this.events;
                                this.events = [];
                                for (var i = 0; i < events.length; ++i) {
                                    var trigger = this._eventTriggerMap[events[i].type];
                                    trigger && trigger.fire(events[i]);
                                }
                            }
                            scene.update.fire(), advanceAge && ++this.age;
                        }
                        return this._sceneChangeRequests.length ? (this._flushSceneChangeRequests(), scene !== this.scenes[this.scenes.length - 1]) : !1;
                    }, Game.prototype.render = function(camera) {
                        camera || (camera = this.focusingCamera);
                        for (var renderers = this.renderers, i = 0; i < renderers.length; ++i) renderers[i].draw(this, camera);
                        this.modified = !1;
                    }, Game.prototype.findPointSource = function(point, camera) {
                        return camera || (camera = this.focusingCamera), this.scene().findPointSourceByPoint(point, !1, camera);
                    }, Game.prototype.register = function(e) {
                        if (e.local) {
                            if (void 0 === e.id) e.id = --this._localIdx; else {
                                if (e.id > 0) throw g.ExceptionFactory.createAssertionError("Game#register: invalid local id: " + e.id);
                                if (this._localDb.hasOwnProperty(String(e.id))) throw g.ExceptionFactory.createAssertionError("Game#register: conflicted id: " + e.id);
                                this._localIdx > e.id && (this._localIdx = e.id);
                            }
                            this._localDb[e.id] = e;
                        } else {
                            if (void 0 === e.id) e.id = ++this._idx; else {
                                if (e.id < 0) throw g.ExceptionFactory.createAssertionError("Game#register: invalid non-local id: " + e.id);
                                if (this.db.hasOwnProperty(String(e.id))) throw g.ExceptionFactory.createAssertionError("Game#register: conflicted id: " + e.id);
                                this._idx < e.id && (this._idx = e.id);
                            }
                            this.db[e.id] = e;
                        }
                    }, Game.prototype.unregister = function(e) {
                        e.local ? delete this._localDb[e.id] : delete this.db[e.id];
                    }, Game.prototype.leaveGame = function() {
                        this._leaveGame();
                    }, Game.prototype.terminateGame = function() {
                        this._leaveGame(), this._isTerminated = !0, this._terminateGame();
                    }, Game.prototype._fireSceneReady = function(scene) {
                        this._sceneChangeRequests.push({
                            type: 3,
                            scene: scene
                        });
                    }, Game.prototype._fireSceneLoaded = function(scene) {
                        scene._loadingState < g.SceneLoadState.LoadedFired && this._sceneChangeRequests.push({
                            type: 4,
                            scene: scene
                        });
                    }, Game.prototype._callSceneAssetHolderHandler = function(assetHolder) {
                        this._sceneChangeRequests.push({
                            type: 5,
                            assetHolder: assetHolder
                        });
                    }, Game.prototype._normalizeConfiguration = function(gameConfiguration) {
                        if (!gameConfiguration) throw g.ExceptionFactory.createAssertionError("Game#_normalizeConfiguration: invalid arguments");
                        if ("assets" in gameConfiguration || (gameConfiguration.assets = {}), "fps" in gameConfiguration || (gameConfiguration.fps = 30), 
                        "number" != typeof gameConfiguration.fps) throw g.ExceptionFactory.createAssertionError("Game#_normalizeConfiguration: fps must be given as a number");
                        if (!(0 <= gameConfiguration.fps && gameConfiguration.fps <= 60)) throw g.ExceptionFactory.createAssertionError("Game#_normalizeConfiguration: fps must be a number in (0, 60].");
                        if ("number" != typeof gameConfiguration.width) throw g.ExceptionFactory.createAssertionError("Game#_normalizeConfiguration: width must be given as a number");
                        if ("number" != typeof gameConfiguration.height) throw g.ExceptionFactory.createAssertionError("Game#_normalizeConfiguration: height must be given as a number");
                        return gameConfiguration;
                    }, Game.prototype._setAudioPlaybackRate = function(playbackRate) {
                        this._audioSystemManager._setPlaybackRate(playbackRate);
                    }, Game.prototype._setMuted = function(muted) {
                        this._audioSystemManager._setMuted(muted);
                    }, Game.prototype._decodeOperationPluginOperation = function(code, op) {
                        var plugins = this._operationPluginManager.plugins;
                        return plugins[code] && plugins[code].decode ? plugins[code].decode(op) : op;
                    }, Game.prototype._reset = function(param) {
                        if (this._operationPluginManager.stopAll(), this.scene()) {
                            for (;this.scene() !== this._initialScene; ) this.popScene(), this._flushSceneChangeRequests();
                            this.isLoaded || this.scenes.pop();
                        }
                        switch (param && (void 0 !== param.age && (this.age = param.age), void 0 !== param.randGen && (this.random = param.randGen)), 
                        this._audioSystemManager._reset(), this._loaded.removeAll({
                            func: this._start,
                            owner: this
                        }), this.join.removeAll(), this.leave.removeAll(), this.seed.removeAll(), this.resized.removeAll(), 
                        this.skippingChanged.removeAll(), this._idx = 0, this._localIdx = 0, this._cameraIdx = 0, 
                        this.db = {}, this._localDb = {}, this.events = [], this.modified = !0, this.loadingScene = void 0, 
                        this._focusingCamera = void 0, this._scriptCaches = {}, this.snapshotRequest.removeAll(), 
                        this._sceneChangeRequests = [], this._isTerminated = !1, this.vars = {}, this.surfaceAtlasSet && this.surfaceAtlasSet.destroy(), 
                        this.surfaceAtlasSet = new g.SurfaceAtlasSet({
                            game: this
                        }), this._configuration.defaultLoadingScene) {
                          case "none":
                            this._defaultLoadingScene = new g.LoadingScene({
                                game: this
                            });
                            break;

                          default:
                            this._defaultLoadingScene = new g.DefaultLoadingScene({
                                game: this
                            });
                        }
                    }, Game.prototype._destroy = function() {
                        if (this._operationPluginManager.destroy(), this.scene()) for (;this.scene() !== this._initialScene; ) this.popScene(), 
                        this._flushSceneChangeRequests();
                        this._initialScene.destroy(), this.loadingScene && !this.loadingScene.destroyed() && this.loadingScene.destroy(), 
                        this._defaultLoadingScene.destroyed() || this._defaultLoadingScene.destroy(), this.db = void 0, 
                        this.renderers = void 0, this.scenes = void 0, this.random = void 0, this.events = void 0, 
                        this.join.destroy(), this.join = void 0, this.leave.destroy(), this.leave = void 0, 
                        this.seed.destroy(), this.seed = void 0, this.modified = !1, this.age = 0, this.assets = void 0, 
                        this.isLoaded = !1, this.loadingScene = void 0, this.assetBase = "", this.selfId = void 0;
                        for (var audioSystemIds = Object.keys(this.audio), i = 0; i < audioSystemIds.length; ++i) this.audio[audioSystemIds[i]].stopAll();
                        this.audio = void 0, this.defaultAudioSystemId = void 0, this.logger.destroy(), 
                        this.logger = void 0, this.snapshotRequest.destroy(), this.snapshotRequest = void 0, 
                        this.resourceFactory = void 0, this.storage = void 0, this.playId = void 0, this.operationPlugins = void 0, 
                        this.resized.destroy(), this.resized = void 0, this.skippingChanged.destroy(), this.skippingChanged = void 0, 
                        this._eventTriggerMap = void 0, this._initialScene = void 0, this._defaultLoadingScene = void 0, 
                        this._sceneChanged.destroy(), this._sceneChanged = void 0, this._scriptCaches = void 0, 
                        this._loaded.destroy(), this._loaded = void 0, this._started.destroy(), this._started = void 0, 
                        this._main = void 0, this._mainParameter = void 0, this._assetManager.destroy(), 
                        this._assetManager = void 0, this._audioSystemManager._game = void 0, this._audioSystemManager = void 0, 
                        this._operationPluginManager = void 0, this._operationPluginOperated.destroy(), 
                        this._operationPluginOperated = void 0, this._idx = 0, this._localDb = {}, this._localIdx = 0, 
                        this._cameraIdx = 0, this._isTerminated = !0, this._focusingCamera = void 0, this._configuration = void 0, 
                        this._sceneChangeRequests = [], this.surfaceAtlasSet.destroy(), this.surfaceAtlasSet = void 0;
                    }, Game.prototype._loadAndStart = function(param) {
                        this._mainParameter = param || {}, this.isLoaded ? this._start() : (this._loaded.add(this._start, this), 
                        this.pushScene(this._initialScene), this._flushSceneChangeRequests());
                    }, Game.prototype._startLoadingGlobalAssets = function() {
                        if (this.isLoaded) throw g.ExceptionFactory.createAssertionError("Game#_startLoadingGlobalAssets: already loaded.");
                        this.pushScene(this._initialScene), this._flushSceneChangeRequests();
                    }, Game.prototype._updateEventTriggers = function(scene) {
                        return this.modified = !0, scene ? (this._eventTriggerMap[g.EventType.Message] = scene.message, 
                        this._eventTriggerMap[g.EventType.PointDown] = scene.pointDownCapture, this._eventTriggerMap[g.EventType.PointMove] = scene.pointMoveCapture, 
                        this._eventTriggerMap[g.EventType.PointUp] = scene.pointUpCapture, this._eventTriggerMap[g.EventType.Operation] = scene.operation, 
                        void scene._activate()) : (this._eventTriggerMap[g.EventType.Message] = void 0, 
                        this._eventTriggerMap[g.EventType.PointDown] = void 0, this._eventTriggerMap[g.EventType.PointMove] = void 0, 
                        this._eventTriggerMap[g.EventType.PointUp] = void 0, void (this._eventTriggerMap[g.EventType.Operation] = void 0));
                    }, Game.prototype._onInitialSceneLoaded = function() {
                        this._initialScene.loaded.remove(this._onInitialSceneLoaded, this), this.assets = this._initialScene.assets, 
                        this.isLoaded = !0, this._loaded.fire();
                    }, Game.prototype._terminateGame = function() {}, Game.prototype._flushSceneChangeRequests = function() {
                        do {
                            var reqs = this._sceneChangeRequests;
                            this._sceneChangeRequests = [];
                            for (var i = 0; i < reqs.length; ++i) {
                                var req = reqs[i];
                                switch (req.type) {
                                  case 0:
                                    var oldScene = this.scene();
                                    oldScene && oldScene._deactivate(), this._doPushScene(req.scene);
                                    break;

                                  case 1:
                                    this._doPopScene(req.preserveCurrent, !1), this._doPushScene(req.scene);
                                    break;

                                  case 2:
                                    this._doPopScene(req.preserveCurrent, !0);
                                    break;

                                  case 3:
                                    req.scene.destroyed() || req.scene._fireReady();
                                    break;

                                  case 4:
                                    req.scene.destroyed() || req.scene._fireLoaded();
                                    break;

                                  case 5:
                                    req.assetHolder.callHandler();
                                    break;

                                  default:
                                    throw g.ExceptionFactory.createAssertionError("Game#_flushSceneChangeRequests: unknown scene change request.");
                                }
                            }
                        } while (this._sceneChangeRequests.length > 0);
                    }, Game.prototype._doPopScene = function(preserveCurrent, fireSceneChanged) {
                        var scene = this.scenes.pop();
                        if (scene === this._initialScene) throw g.ExceptionFactory.createAssertionError("Game#_doPopScene: invalid call; attempting to pop the initial scene");
                        preserveCurrent || scene.destroy(), fireSceneChanged && this._sceneChanged.fire(this.scene());
                    }, Game.prototype._start = function() {
                        if (this._operationPluginManager.initialize(), this.operationPlugins = this._operationPluginManager.plugins, 
                        !this._main) {
                            if (this._mainParameter.snapshot) {
                                if (!this.assets.snapshotLoader) throw g.ExceptionFactory.createAssertionError("Game#_start: global asset 'snapshotLoader' not found.");
                                var loader = g._require(this, "snapshotLoader");
                                loader(this._mainParameter.snapshot), this._flushSceneChangeRequests();
                            } else {
                                if (!this.assets.mainScene) throw g.ExceptionFactory.createAssertionError("Game#_start: global asset 'mainScene' not found.");
                                var mainScene = g._require(this, "mainScene")();
                                this.pushScene(mainScene), this._flushSceneChangeRequests();
                            }
                            return void this._started.fire();
                        }
                        var mainFun = g._require(this, this._main);
                        if (!mainFun || "function" != typeof mainFun) throw g.ExceptionFactory.createAssertionError("Game#_start: Entry point '" + this._main + "' not found.");
                        mainFun(this._mainParameter), this._flushSceneChangeRequests(), this._started.fire();
                    }, Game.prototype._doPushScene = function(scene, loadingScene) {
                        if (loadingScene || (loadingScene = this.loadingScene || this._defaultLoadingScene), 
                        this.scenes.push(scene), scene._needsLoading() && scene._loadingState < g.SceneLoadState.LoadedFired) {
                            if (this._defaultLoadingScene._needsLoading()) throw g.ExceptionFactory.createAssertionError("Game#_doPushScene: _defaultLoadingScene must not depend on any assets/storages.");
                            this._doPushScene(loadingScene, this._defaultLoadingScene), loadingScene.reset(scene);
                        } else this._sceneChanged.fire(scene), scene._loaded || (scene._load(), this._fireSceneLoaded(scene));
                        this.modified = !0;
                    }, Game;
                }();
                g.Game = Game;
            }(g || (g = {}));
            var g;
            !function(g) {
                var Camera2D = function(_super) {
                    function Camera2D(param) {
                        var _this = _super.call(this, param) || this;
                        return _this.game = param.game, _this.local = !!param.local, _this.name = param.name, 
                        _this._modifiedCount = 0, _this.width = param.game.width, _this.height = param.game.height, 
                        _this.id = _this.local ? void 0 : _this.game._cameraIdx++, _this;
                    }
                    return __extends(Camera2D, _super), Camera2D.deserialize = function(ser, game) {
                        var s = ser;
                        s.param.game = game;
                        var ret = new Camera2D(s.param);
                        return ret.id = s.id, ret;
                    }, Camera2D.prototype.modified = function() {
                        this._modifiedCount = (this._modifiedCount + 1) % 32768, this._matrix && (this._matrix._modified = !0), 
                        this.game.modified = !0;
                    }, Camera2D.prototype.serialize = function() {
                        var ser = {
                            id: this.id,
                            param: {
                                game: void 0,
                                local: this.local,
                                name: this.name,
                                x: this.x,
                                y: this.y,
                                width: this.width,
                                height: this.height,
                                opacity: this.opacity,
                                scaleX: this.scaleX,
                                scaleY: this.scaleY,
                                angle: this.angle,
                                compositeOperation: this.compositeOperation
                            }
                        };
                        return ser;
                    }, Camera2D.prototype._applyTransformToRenderer = function(renderer) {
                        this.angle || 1 !== this.scaleX || 1 !== this.scaleY ? renderer.transform(this.getMatrix()._matrix) : renderer.translate(-this.x, -this.y), 
                        1 !== this.opacity && renderer.opacity(this.opacity);
                    }, Camera2D.prototype._updateMatrix = function() {
                        this.angle || 1 !== this.scaleX || 1 !== this.scaleY ? this._matrix.updateByInverse(this.width, this.height, this.scaleX, this.scaleY, this.angle, this.x, this.y) : this._matrix.reset(-this.x, -this.y);
                    }, Camera2D;
                }(g.Object2D);
                g.Camera2D = Camera2D;
            }(g || (g = {}));
            var g;
            !function(g) {
                var Renderer = function() {
                    function Renderer() {}
                    return Renderer.prototype.draw = function(game, camera) {
                        var scene = game.scene();
                        if (scene) {
                            this.begin(), this.save(), this.clear(), camera && (this.save(), camera._applyTransformToRenderer(this));
                            for (var children = scene.children, i = 0; i < children.length; ++i) children[i].render(this, camera);
                            camera && this.restore(), this.restore(), this.end();
                        }
                    }, Renderer.prototype.begin = function() {}, Renderer.prototype.end = function() {}, 
                    Renderer;
                }();
                g.Renderer = Renderer;
            }(g || (g = {}));
            var g;
            !function(g) {
                var Surface = function() {
                    function Surface(width, height, drawable, isDynamic) {
                        if (void 0 === isDynamic && (isDynamic = !1), width % 1 !== 0 || height % 1 !== 0) throw g.ExceptionFactory.createAssertionError("Surface#constructor: width and height must be integers");
                        this.width = width, this.height = height, drawable && (this._drawable = drawable), 
                        this.isDynamic = isDynamic, this.isDynamic ? (this.animatingStarted = new g.Trigger(), 
                        this.animatingStopped = new g.Trigger()) : (this.animatingStarted = void 0, this.animatingStopped = void 0);
                    }
                    return Surface.prototype.destroy = function() {
                        this.animatingStarted && this.animatingStarted.destroy(), this.animatingStopped && this.animatingStopped.destroy(), 
                        this._destroyed = !0;
                    }, Surface.prototype.destroyed = function() {
                        return !!this._destroyed;
                    }, Surface;
                }();
                g.Surface = Surface;
            }(g || (g = {}));
            var g;
            !function(g) {
                var Label = function(_super) {
                    function Label(param) {
                        var _this = _super.call(this, param) || this;
                        return _this.text = param.text, _this.font = param.font, _this.textAlign = "textAlign" in param ? param.textAlign : g.TextAlign.Left, 
                        _this.glyphs = new Array(param.text.length), _this.fontSize = param.fontSize, _this.maxWidth = param.maxWidth, 
                        _this.widthAutoAdjust = "widthAutoAdjust" in param ? param.widthAutoAdjust : !0, 
                        _this.textColor = param.textColor, _this._textWidth = 0, _this._game = void 0, _this._overhangLeft = 0, 
                        _this._overhangRight = 0, _this._invalidateSelf(), _this;
                    }
                    return __extends(Label, _super), Label.prototype.aligning = function(width, textAlign) {
                        this.width = width, this.widthAutoAdjust = !1, this.textAlign = textAlign;
                    }, Label.prototype.invalidate = function() {
                        this._invalidateSelf(), _super.prototype.invalidate.call(this);
                    }, Label.prototype.renderSelfFromCache = function(renderer) {
                        var destOffsetX;
                        switch (this.textAlign) {
                          case g.TextAlign.Center:
                            destOffsetX = this.widthAutoAdjust ? this._overhangLeft : 0;
                            break;

                          case g.TextAlign.Right:
                            destOffsetX = this.widthAutoAdjust ? this._overhangLeft : this._overhangRight;
                            break;

                          default:
                            destOffsetX = this._overhangLeft;
                        }
                        renderer.drawImage(this._cache, 0, 0, this._cacheSize.width + g.CacheableE.PADDING, this._cacheSize.height + g.CacheableE.PADDING, destOffsetX, 0);
                    }, Label.prototype.renderCache = function(renderer) {
                        if (!(!this.fontSize || this.height <= 0 || this._textWidth <= 0)) {
                            var scale = this.maxWidth > 0 && this.maxWidth < this._textWidth ? this.maxWidth / this._textWidth : 1, offsetX = 0;
                            switch (this.textAlign) {
                              case g.TextAlign.Center:
                                offsetX = this.width / 2 - (this._textWidth + this._overhangLeft) / 2 * scale;
                                break;

                              case g.TextAlign.Right:
                                offsetX = this.width - (this._textWidth - this._overhangRight) * scale;
                                break;

                              default:
                                offsetX -= this._overhangLeft;
                            }
                            renderer.translate(offsetX, 0), 1 !== scale && renderer.transform([ scale, 0, 0, 1, 0, 0 ]), 
                            renderer.save();
                            for (var glyphScale = this.fontSize / this.font.size, i = 0; i < this.glyphs.length; ++i) {
                                var glyph = this.glyphs[i], glyphWidth = glyph.advanceWidth * glyphScale, code = glyph.code;
                                glyph.isSurfaceValid || (glyph = this.font.glyphForCharacter(code)) ? (glyph.surface && (renderer.save(), 
                                renderer.transform([ glyphScale, 0, 0, glyphScale, 0, 0 ]), renderer.drawImage(glyph.surface, glyph.x, glyph.y, glyph.width, glyph.height, glyph.offsetX, glyph.offsetY), 
                                renderer.restore()), renderer.translate(glyphWidth, 0)) : this._outputOfWarnLogWithNoGlyph(code, "renderCache()");
                            }
                            renderer.restore(), renderer.save(), this.textColor && (renderer.setCompositeOperation(g.CompositeOperation.SourceAtop), 
                            renderer.fillRect(0, 0, this._textWidth, this.height, this.textColor)), renderer.restore();
                        }
                    }, Label.prototype.destroy = function() {
                        _super.prototype.destroy.call(this);
                    }, Label.prototype._invalidateSelf = function() {
                        if (this.glyphs.length = 0, this._textWidth = 0, !this.fontSize) return void (this.height = 0);
                        for (var effectiveTextLastIndex = this.text.length - 1, i = this.text.length - 1; i >= 0; --i) {
                            var code = g.Util.charCodeAt(this.text, i);
                            if (code) {
                                var glyph = this.font.glyphForCharacter(code);
                                if (glyph && 0 !== glyph.width && 0 !== glyph.advanceWidth) {
                                    effectiveTextLastIndex = i;
                                    break;
                                }
                            }
                        }
                        for (var maxHeight = 0, glyphScale = this.font.size > 0 ? this.fontSize / this.font.size : 0, i = 0; effectiveTextLastIndex >= i; ++i) {
                            var code_1 = g.Util.charCodeAt(this.text, i);
                            if (code_1) {
                                var glyph = this.font.glyphForCharacter(code_1);
                                if (glyph) {
                                    if (!(glyph.width < 0 || glyph.height < 0 || glyph.x < 0 || glyph.y < 0)) {
                                        this.glyphs.push(glyph);
                                        var overhang = 0;
                                        0 === i && (this._overhangLeft = Math.min(glyph.offsetX, 0), overhang = -this._overhangLeft), 
                                        i === effectiveTextLastIndex && (this._overhangRight = Math.max(glyph.width + glyph.offsetX - glyph.advanceWidth, 0), 
                                        overhang += this._overhangRight), this._textWidth += (glyph.advanceWidth + overhang) * glyphScale;
                                        var height = glyph.offsetY + glyph.height;
                                        height > maxHeight && (maxHeight = height);
                                    }
                                } else this._outputOfWarnLogWithNoGlyph(code_1, "_invalidateSelf()");
                            }
                        }
                        this.widthAutoAdjust && (this.width = this._textWidth), this.height = maxHeight * glyphScale;
                    }, Label.prototype._outputOfWarnLogWithNoGlyph = function(code, functionName) {
                        var str = 4294901760 & code ? String.fromCharCode((4294901760 & code) >>> 16, 65535 & code) : String.fromCharCode(code);
                        this.game().logger.warn("Label#" + functionName + ": failed to get a glyph for '" + str + "' (BitmapFont might not have the glyph or DynamicFont might create a glyph larger than its atlas).");
                    }, Label;
                }(g.CacheableE);
                g.Label = Label;
            }(g || (g = {}));
            var g;
            !function(g) {
                var Glyph = function() {
                    function Glyph(code, x, y, width, height, offsetX, offsetY, advanceWidth, surface, isSurfaceValid) {
                        void 0 === offsetX && (offsetX = 0), void 0 === offsetY && (offsetY = 0), void 0 === advanceWidth && (advanceWidth = width), 
                        void 0 === isSurfaceValid && (isSurfaceValid = !!surface), this.code = code, this.x = x, 
                        this.y = y, this.width = width, this.height = height, this.offsetX = offsetX, this.offsetY = offsetY, 
                        this.advanceWidth = advanceWidth, this.surface = surface, this.isSurfaceValid = isSurfaceValid, 
                        this._atlas = null;
                    }
                    return Glyph.prototype.renderingWidth = function(fontSize) {
                        return this.width && this.height ? fontSize / this.height * this.width : 0;
                    }, Glyph;
                }();
                g.Glyph = Glyph;
            }(g || (g = {}));
            var g;
            !function(g) {
                var FilledRect = function(_super) {
                    function FilledRect(param) {
                        var _this = _super.call(this, param) || this;
                        if ("string" != typeof param.cssColor) throw g.ExceptionFactory.createTypeMismatchError("ColorBox#constructor(cssColor)", "string", param.cssColor);
                        return _this.cssColor = param.cssColor, _this;
                    }
                    return __extends(FilledRect, _super), FilledRect.prototype.renderSelf = function(renderer) {
                        return renderer.fillRect(0, 0, this.width, this.height, this.cssColor), !0;
                    }, FilledRect;
                }(g.E);
                g.FilledRect = FilledRect;
            }(g || (g = {}));
            var g;
            !function(g) {
                var Pane = function(_super) {
                    function Pane(param) {
                        var _this = _super.call(this, param) || this;
                        return _this._oldWidth = param.width, _this._oldHeight = param.height, _this.backgroundImage = g.Util.asSurface(param.backgroundImage), 
                        _this.backgroundEffector = param.backgroundEffector, _this._shouldRenderChildren = !1, 
                        _this._padding = param.padding, _this._initialize(), _this._paddingChanged = !1, 
                        _this._bgSurface = void 0, _this._bgRenderer = void 0, _this;
                    }
                    return __extends(Pane, _super), Object.defineProperty(Pane.prototype, "padding", {
                        get: function() {
                            return this._padding;
                        },
                        set: function(padding) {
                            this._padding = padding, this._paddingChanged = !0;
                        },
                        enumerable: !0,
                        configurable: !0
                    }), Pane.prototype.modified = function(isBubbling) {
                        isBubbling && (this.state &= -3), _super.prototype.modified.call(this);
                    }, Pane.prototype.shouldFindChildrenByPoint = function(point) {
                        var p = this._normalizedPadding;
                        return p.left < point.x && this.width - p.right > point.x && p.top < point.y && this.height - p.bottom > point.y ? !0 : !1;
                    }, Pane.prototype.renderCache = function(renderer, camera) {
                        this.width <= 0 || this.height <= 0 || (this._renderBackground(), this._renderChildren(camera), 
                        this._bgSurface ? renderer.drawImage(this._bgSurface, 0, 0, this.width, this.height, 0, 0) : this.backgroundImage && renderer.drawImage(this.backgroundImage, 0, 0, this.width, this.height, 0, 0), 
                        this._childrenArea.width <= 0 || this._childrenArea.height <= 0 || (renderer.save(), 
                        (0 !== this._childrenArea.x || 0 !== this._childrenArea.y) && renderer.translate(this._childrenArea.x, this._childrenArea.y), 
                        renderer.drawImage(this._childrenSurface, 0, 0, this._childrenArea.width, this._childrenArea.height, 0, 0), 
                        renderer.restore()));
                    }, Pane.prototype.destroy = function(destroySurface) {
                        destroySurface && this.backgroundImage && !this.backgroundImage.destroyed() && this.backgroundImage.destroy(), 
                        this._bgSurface && !this._bgSurface.destroyed() && this._bgSurface.destroy(), this._childrenSurface && !this._childrenSurface.destroyed() && this._childrenSurface.destroy(), 
                        this.backgroundImage = void 0, this._bgSurface = void 0, this._childrenSurface = void 0, 
                        _super.prototype.destroy.call(this);
                    }, Pane.prototype._renderBackground = function() {
                        if (this.backgroundImage && this.backgroundEffector) {
                            var bgSurface = this.backgroundEffector.render(this.backgroundImage, this.width, this.height);
                            this._bgSurface !== bgSurface && (this._bgSurface && !this._bgSurface.destroyed() && this._bgSurface.destroy(), 
                            this._bgSurface = bgSurface);
                        } else this._bgSurface = void 0;
                    }, Pane.prototype._renderChildren = function(camera) {
                        var isNew = this._oldWidth !== this.width || this._oldHeight !== this.height || this._paddingChanged;
                        if (isNew && (this._initialize(), this._paddingChanged = !1, this._oldWidth = this.width, 
                        this._oldHeight = this.height), this._childrenRenderer.begin(), isNew || this._childrenRenderer.clear(), 
                        this.children) for (var children = this.children, i = 0; i < children.length; ++i) children[i].render(this._childrenRenderer, camera);
                        this._childrenRenderer.end();
                    }, Pane.prototype._initialize = function() {
                        var r, p = void 0 === this._padding ? 0 : this._padding;
                        r = "number" == typeof p ? {
                            top: p,
                            bottom: p,
                            left: p,
                            right: p
                        } : this._padding, this._childrenArea = {
                            x: r.left,
                            y: r.top,
                            width: this.width - r.left - r.right,
                            height: this.height - r.top - r.bottom
                        };
                        var resourceFactory = this.scene.game.resourceFactory;
                        this._childrenSurface && !this._childrenSurface.destroyed() && this._childrenSurface.destroy(), 
                        this._childrenSurface = resourceFactory.createSurface(Math.ceil(this._childrenArea.width), Math.ceil(this._childrenArea.height)), 
                        this._childrenRenderer = this._childrenSurface.renderer(), this._normalizedPadding = r;
                    }, Pane.prototype._calculateBoundingRect = function(m, c) {
                        var matrix = this.getMatrix();
                        if (m && (matrix = m.multiplyNew(matrix)), this.visible() && (!c || this._targetCameras && -1 !== this._targetCameras.indexOf(c))) {
                            for (var thisBoundingRect = {
                                left: 0,
                                right: this.width,
                                top: 0,
                                bottom: this.height
                            }, targetCoordinates = [ {
                                x: thisBoundingRect.left,
                                y: thisBoundingRect.top
                            }, {
                                x: thisBoundingRect.left,
                                y: thisBoundingRect.bottom
                            }, {
                                x: thisBoundingRect.right,
                                y: thisBoundingRect.top
                            }, {
                                x: thisBoundingRect.right,
                                y: thisBoundingRect.bottom
                            } ], convertedPoint = matrix.multiplyPoint(targetCoordinates[0]), result = {
                                left: convertedPoint.x,
                                right: convertedPoint.x,
                                top: convertedPoint.y,
                                bottom: convertedPoint.y
                            }, i = 1; i < targetCoordinates.length; ++i) convertedPoint = matrix.multiplyPoint(targetCoordinates[i]), 
                            result.left > convertedPoint.x && (result.left = convertedPoint.x), result.right < convertedPoint.x && (result.right = convertedPoint.x), 
                            result.top > convertedPoint.y && (result.top = convertedPoint.y), result.bottom < convertedPoint.y && (result.bottom = convertedPoint.y);
                            return result;
                        }
                    }, Pane;
                }(g.CacheableE);
                g.Pane = Pane;
            }(g || (g = {}));
            var g;
            !function(g) {
                var OperationHandler = function() {
                    function OperationHandler(code, owner, handler) {
                        this._code = code, this._handler = handler, this._handlerOwner = owner;
                    }
                    return OperationHandler.prototype.onOperation = function(op) {
                        var iop;
                        op instanceof Array ? iop = {
                            _code: this._code,
                            data: op
                        } : (iop = op, iop._code = this._code), this._handler.call(this._handlerOwner, iop);
                    }, OperationHandler;
                }(), OperationPluginManager = function() {
                    function OperationPluginManager(game, viewInfo, infos) {
                        this.operated = new g.Trigger(), this.plugins = {}, this._game = game, this._viewInfo = viewInfo, 
                        this._infos = infos, this._initialized = !1;
                    }
                    return OperationPluginManager.prototype.initialize = function() {
                        this._initialized || (this._initialized = !0, this._loadOperationPlugins()), this._doAutoStart();
                    }, OperationPluginManager.prototype.destroy = function() {
                        this.stopAll(), this.operated.destroy(), this.operated = void 0, this.plugins = void 0, 
                        this._game = void 0, this._viewInfo = void 0, this._infos = void 0;
                    }, OperationPluginManager.prototype.stopAll = function() {
                        if (this._initialized) for (var i = 0; i < this._infos.length; ++i) {
                            var info = this._infos[i];
                            info._plugin && info._plugin.stop();
                        }
                    }, OperationPluginManager.prototype._doAutoStart = function() {
                        for (var i = 0; i < this._infos.length; ++i) {
                            var info = this._infos[i];
                            !info.manualStart && info._plugin && info._plugin.start();
                        }
                    }, OperationPluginManager.prototype._loadOperationPlugins = function() {
                        for (var i = 0; i < this._infos.length; ++i) {
                            var info = this._infos[i];
                            if (info.script) {
                                var pluginClass = g._require(this._game, info.script);
                                if (pluginClass.isSupported()) {
                                    var plugin = new pluginClass(this._game, this._viewInfo, info.option), code = info.code;
                                    if (this.plugins[code]) throw new Error("Plugin#code conflicted for code: " + code);
                                    this.plugins[code] = plugin, info._plugin = plugin;
                                    var handler = new OperationHandler(code, this.operated, this.operated.fire);
                                    plugin.operationTrigger.handle(handler, handler.onOperation);
                                }
                            }
                        }
                    }, OperationPluginManager;
                }();
                g.OperationPluginManager = OperationPluginManager;
            }(g || (g = {}));
            var g;
            !function(g) {}(g || (g = {}));
            var g;
            !function(g) {
                var FontWeight;
                !function(FontWeight) {
                    FontWeight[FontWeight.Normal = 0] = "Normal", FontWeight[FontWeight.Bold = 1] = "Bold";
                }(FontWeight = g.FontWeight || (g.FontWeight = {}));
                var DynamicFont = function() {
                    function DynamicFont(param) {
                        if (this.fontFamily = param.fontFamily, this.size = param.size, this.hint = "hint" in param ? param.hint : {}, 
                        this.fontColor = "fontColor" in param ? param.fontColor : "black", this.fontWeight = "fontWeight" in param ? param.fontWeight : FontWeight.Normal, 
                        this.strokeWidth = "strokeWidth" in param ? param.strokeWidth : 0, this.strokeColor = "strokeColor" in param ? param.strokeColor : "black", 
                        this.strokeOnly = "strokeOnly" in param ? param.strokeOnly : !1, this._resourceFactory = param.game.resourceFactory, 
                        this._glyphFactory = this._resourceFactory.createGlyphFactory(this.fontFamily, this.size, this.hint.baselineHeight, this.fontColor, this.strokeWidth, this.strokeColor, this.strokeOnly, this.fontWeight), 
                        this._glyphs = {}, this._destroyed = !1, this._isSurfaceAtlasSetOwner = !1, param.surfaceAtlasSet ? this._atlasSet = param.surfaceAtlasSet : param.hint ? (this._isSurfaceAtlasSetOwner = !0, 
                        this._atlasSet = new g.SurfaceAtlasSet({
                            game: param.game,
                            hint: this.hint
                        })) : this._atlasSet = param.game.surfaceAtlasSet, this._atlasSet.addAtlas(), this.hint.presetChars) for (var i = 0, len = this.hint.presetChars.length; len > i; i++) {
                            var code = g.Util.charCodeAt(this.hint.presetChars, i);
                            code && this.glyphForCharacter(code);
                        }
                    }
                    return DynamicFont.prototype.glyphForCharacter = function(code) {
                        var glyph = this._glyphs[code];
                        if (!glyph || !glyph.isSurfaceValid) {
                            if (glyph = this._glyphFactory.create(code), glyph.surface) {
                                var atlas_1 = this._atlasSet.addGlyph(glyph);
                                if (!atlas_1) return null;
                                glyph._atlas = atlas_1, glyph._atlas._accessScore += 1;
                            }
                            this._glyphs[code] = glyph;
                        }
                        for (var i = 0; i < this._atlasSet.getAtlasNum(); i++) {
                            var atlas = this._atlasSet.getAtlas(i);
                            atlas._accessScore /= 2;
                        }
                        return glyph;
                    }, DynamicFont.prototype.asBitmapFont = function(missingGlyphChar) {
                        var _this = this;
                        if (1 !== this._atlasSet.getAtlasNum()) return null;
                        var missingGlyphCharCodePoint;
                        missingGlyphChar && (missingGlyphCharCodePoint = g.Util.charCodeAt(missingGlyphChar, 0), 
                        this.glyphForCharacter(missingGlyphCharCodePoint));
                        var glyphAreaMap = {};
                        Object.keys(this._glyphs).forEach(function(_key) {
                            var key = Number(_key), glyph = _this._glyphs[key], glyphArea = {
                                x: glyph.x,
                                y: glyph.y,
                                width: glyph.width,
                                height: glyph.height,
                                offsetX: glyph.offsetX,
                                offsetY: glyph.offsetY,
                                advanceWidth: glyph.advanceWidth
                            };
                            glyphAreaMap[key] = glyphArea;
                        });
                        var missingGlyph = glyphAreaMap[missingGlyphCharCodePoint], surface = this._atlasSet.getAtlas(0).duplicateSurface(this._resourceFactory), bitmapFont = new g.BitmapFont({
                            src: surface,
                            map: glyphAreaMap,
                            defaultGlyphWidth: 0,
                            defaultGlyphHeight: this.size,
                            missingGlyph: missingGlyph
                        });
                        return bitmapFont;
                    }, DynamicFont.prototype.destroy = function() {
                        this._isSurfaceAtlasSetOwner && this._atlasSet.destroy(), this._glyphs = null, this._glyphFactory = null, 
                        this._destroyed = !0;
                    }, DynamicFont.prototype.destroyed = function() {
                        return this._destroyed;
                    }, DynamicFont;
                }();
                g.DynamicFont = DynamicFont;
            }(g || (g = {}));
            var g;
            !function(g) {
                function getSurfaceAtlasSlot(slot, width, height) {
                    for (;slot; ) {
                        if (slot.width >= width && slot.height >= height) return slot;
                        slot = slot.next;
                    }
                    return null;
                }
                function calcAtlasSize(hint) {
                    var width = Math.ceil(Math.min(hint.initialAtlasWidth, hint.maxAtlasWidth)), height = Math.ceil(Math.min(hint.initialAtlasHeight, hint.maxAtlasHeight));
                    return {
                        width: width,
                        height: height
                    };
                }
                var SurfaceAtlasSlot = function() {
                    function SurfaceAtlasSlot(x, y, width, height) {
                        this.x = x, this.y = y, this.width = width, this.height = height, this.prev = null, 
                        this.next = null;
                    }
                    return SurfaceAtlasSlot;
                }();
                g.SurfaceAtlasSlot = SurfaceAtlasSlot;
                var SurfaceAtlas = function() {
                    function SurfaceAtlas(surface) {
                        this._surface = surface, this._emptySurfaceAtlasSlotHead = new SurfaceAtlasSlot(0, 0, this._surface.width, this._surface.height), 
                        this._accessScore = 0, this._usedRectangleAreaSize = {
                            width: 0,
                            height: 0
                        };
                    }
                    return SurfaceAtlas.prototype._acquireSurfaceAtlasSlot = function(width, height) {
                        width += 1, height += 1;
                        var slot = getSurfaceAtlasSlot(this._emptySurfaceAtlasSlotHead, width, height);
                        if (!slot) return null;
                        var left, right, remainWidth = slot.width - width, remainHeight = slot.height - height;
                        remainHeight >= remainWidth ? (left = new SurfaceAtlasSlot(slot.x + width, slot.y, remainWidth, height), 
                        right = new SurfaceAtlasSlot(slot.x, slot.y + height, slot.width, remainHeight)) : (left = new SurfaceAtlasSlot(slot.x, slot.y + height, width, remainHeight), 
                        right = new SurfaceAtlasSlot(slot.x + width, slot.y, remainWidth, slot.height)), 
                        left.prev = slot.prev, left.next = right, null === left.prev ? this._emptySurfaceAtlasSlotHead = left : left.prev.next = left, 
                        right.prev = left, right.next = slot.next, right.next && (right.next.prev = right);
                        var acquiredSlot = new SurfaceAtlasSlot(slot.x, slot.y, width, height);
                        return this._updateUsedRectangleAreaSize(acquiredSlot), acquiredSlot;
                    }, SurfaceAtlas.prototype._updateUsedRectangleAreaSize = function(slot) {
                        var slotRight = slot.x + slot.width, slotBottom = slot.y + slot.height;
                        slotRight > this._usedRectangleAreaSize.width && (this._usedRectangleAreaSize.width = slotRight), 
                        slotBottom > this._usedRectangleAreaSize.height && (this._usedRectangleAreaSize.height = slotBottom);
                    }, SurfaceAtlas.prototype.addSurface = function(glyph) {
                        var slot = this._acquireSurfaceAtlasSlot(glyph.width, glyph.height);
                        if (!slot) return null;
                        var renderer = this._surface.renderer();
                        return renderer.begin(), renderer.drawImage(glyph.surface, glyph.x, glyph.y, glyph.width, glyph.height, slot.x, slot.y), 
                        renderer.end(), slot;
                    }, SurfaceAtlas.prototype.destroy = function() {
                        this._surface.destroy();
                    }, SurfaceAtlas.prototype.destroyed = function() {
                        return this._surface.destroyed();
                    }, SurfaceAtlas.prototype.duplicateSurface = function(resourceFactory) {
                        var src = this._surface, dst = resourceFactory.createSurface(this._usedRectangleAreaSize.width, this._usedRectangleAreaSize.height), renderer = dst.renderer();
                        return renderer.begin(), renderer.drawImage(src, 0, 0, this._usedRectangleAreaSize.width, this._usedRectangleAreaSize.height, 0, 0), 
                        renderer.end(), dst;
                    }, SurfaceAtlas;
                }();
                g.SurfaceAtlas = SurfaceAtlas;
                var SurfaceAtlasSet = function() {
                    function SurfaceAtlasSet(params) {
                        this._surfaceAtlases = [], this._atlasGlyphsTable = [], this._resourceFactory = params.game.resourceFactory, 
                        this._currentAtlasIndex = 0;
                        var hint = params.hint ? params.hint : {};
                        this._maxAtlasNum = hint.maxAtlasNum ? hint.maxAtlasNum : SurfaceAtlasSet.INITIAL_MAX_SURFACEATLAS_NUM, 
                        hint.initialAtlasWidth = hint.initialAtlasWidth ? hint.initialAtlasWidth : 512, 
                        hint.initialAtlasHeight = hint.initialAtlasHeight ? hint.initialAtlasHeight : 512, 
                        hint.maxAtlasWidth = hint.maxAtlasWidth ? hint.maxAtlasWidth : 512, hint.maxAtlasHeight = hint.maxAtlasHeight ? hint.maxAtlasHeight : 512, 
                        this._atlasSize = calcAtlasSize(hint);
                    }
                    return SurfaceAtlasSet.prototype._deleteAtlas = function(delteNum) {
                        for (var removedObject = this._removeLeastFrequentlyUsedAtlas(delteNum), removedAtlases = removedObject.surfaceAtlases, i = 0; i < removedAtlases.length; ++i) removedAtlases[i].destroy();
                    }, SurfaceAtlasSet.prototype._removeLeastFrequentlyUsedAtlas = function(removedNum) {
                        for (var removedAtlases = [], removedGlyphs = [], n = 0; removedNum > n; ++n) {
                            for (var minScore = Number.MAX_VALUE, lowScoreAtlasIndex = -1, i = 0; i < this._surfaceAtlases.length; ++i) this._surfaceAtlases[i]._accessScore <= minScore && (minScore = this._surfaceAtlases[i]._accessScore, 
                            lowScoreAtlasIndex = i);
                            var removedAtlas = this._surfaceAtlases.splice(lowScoreAtlasIndex, 1)[0];
                            removedAtlases.push(removedAtlas), removedGlyphs.push(this._atlasGlyphsTable.splice(lowScoreAtlasIndex, 1)[0]);
                        }
                        return {
                            surfaceAtlases: removedAtlases,
                            glyphs: removedGlyphs
                        };
                    }, SurfaceAtlasSet.prototype._moveGlyphSurface = function(glyph) {
                        for (var i = 0; i < this._surfaceAtlases.length; ++i) {
                            var index = (this._currentAtlasIndex + i) % this._surfaceAtlases.length, atlas = this._surfaceAtlases[index], slot = atlas.addSurface(glyph);
                            if (slot) return this._currentAtlasIndex = index, glyph.surface.destroy(), glyph.surface = atlas._surface, 
                            glyph.x = slot.x, glyph.y = slot.y, this._atlasGlyphsTable[index] || (this._atlasGlyphsTable[index] = []), 
                            this._atlasGlyphsTable[index].push(glyph), atlas;
                        }
                        return null;
                    }, SurfaceAtlasSet.prototype._reallocateAtlas = function() {
                        if (this._surfaceAtlases.length >= this._maxAtlasNum) {
                            for (var removedObject = this._removeLeastFrequentlyUsedAtlas(1), atlas = removedObject.surfaceAtlases[0], glyphs = removedObject.glyphs[0], i = 0; i < glyphs.length; i++) {
                                var glyph = glyphs[i];
                                glyph.surface = null, glyph.isSurfaceValid = !1, glyph._atlas = null;
                            }
                            atlas.destroy();
                        }
                        this._surfaceAtlases.push(this._resourceFactory.createSurfaceAtlas(this._atlasSize.width, this._atlasSize.height)), 
                        this._currentAtlasIndex = this._surfaceAtlases.length - 1;
                    }, SurfaceAtlasSet.prototype.addAtlas = function() {
                        this._surfaceAtlases.length >= this._maxAtlasNum && this._deleteAtlas(1), this._surfaceAtlases.push(this._resourceFactory.createSurfaceAtlas(this._atlasSize.width, this._atlasSize.height));
                    }, SurfaceAtlasSet.prototype.getAtlas = function(index) {
                        return this._surfaceAtlases[index];
                    }, SurfaceAtlasSet.prototype.getAtlasNum = function() {
                        return this._surfaceAtlases.length;
                    }, SurfaceAtlasSet.prototype.getMaxAtlasNum = function() {
                        return this._maxAtlasNum;
                    }, SurfaceAtlasSet.prototype.changeMaxAtlasNum = function(value) {
                        if (this._maxAtlasNum = value, this._surfaceAtlases.length > this._maxAtlasNum) {
                            var diff = this._surfaceAtlases.length - this._maxAtlasNum;
                            this._deleteAtlas(diff);
                        }
                    }, SurfaceAtlasSet.prototype.getAtlasSize = function() {
                        return this._atlasSize;
                    }, SurfaceAtlasSet.prototype.addGlyph = function(glyph) {
                        if (glyph.width > this._atlasSize.width || glyph.height > this._atlasSize.height) return null;
                        var atlas = this._moveGlyphSurface(glyph);
                        return atlas || (this._reallocateAtlas(), atlas = this._moveGlyphSurface(glyph)), 
                        atlas;
                    }, SurfaceAtlasSet.prototype.destroy = function() {
                        for (var i = 0; i < this._surfaceAtlases.length; ++i) this._surfaceAtlases[i].destroy();
                        this._surfaceAtlases = void 0, this._resourceFactory = void 0, this._atlasGlyphsTable = void 0;
                    }, SurfaceAtlasSet.prototype.destroyed = function() {
                        return void 0 === this._surfaceAtlases;
                    }, SurfaceAtlasSet;
                }();
                SurfaceAtlasSet.INITIAL_MAX_SURFACEATLAS_NUM = 10, g.SurfaceAtlasSet = SurfaceAtlasSet;
            }(g || (g = {}));
            var g;
            !function(g) {
                var AudioSystemManager = function() {
                    function AudioSystemManager(game) {
                        this._game = game, this._muted = !1, this._playbackRate = 1;
                    }
                    return AudioSystemManager.prototype._reset = function() {
                        this._muted = !1, this._playbackRate = 1;
                        var systems = this._game.audio;
                        for (var id in systems) systems.hasOwnProperty(id) && systems[id]._reset();
                    }, AudioSystemManager.prototype._setMuted = function(muted) {
                        if (this._muted !== muted) {
                            this._muted = muted;
                            var systems = this._game.audio;
                            for (var id in systems) systems.hasOwnProperty(id) && systems[id]._setMuted(muted);
                        }
                    }, AudioSystemManager.prototype._setPlaybackRate = function(rate) {
                        if (this._playbackRate !== rate) {
                            this._playbackRate = rate;
                            var systems = this._game.audio;
                            for (var id in systems) systems.hasOwnProperty(id) && systems[id]._setPlaybackRate(rate);
                        }
                    }, AudioSystemManager;
                }();
                g.AudioSystemManager = AudioSystemManager;
            }(g || (g = {}));
            var g;
            !function(g) {
                var CompositeOperation;
                !function(CompositeOperation) {
                    CompositeOperation[CompositeOperation.SourceOver = 0] = "SourceOver", CompositeOperation[CompositeOperation.SourceAtop = 1] = "SourceAtop", 
                    CompositeOperation[CompositeOperation.Lighter = 2] = "Lighter", CompositeOperation[CompositeOperation.Copy = 3] = "Copy", 
                    CompositeOperation[CompositeOperation.ExperimentalSourceIn = 4] = "ExperimentalSourceIn", 
                    CompositeOperation[CompositeOperation.ExperimentalSourceOut = 5] = "ExperimentalSourceOut", 
                    CompositeOperation[CompositeOperation.ExperimentalDestinationAtop = 6] = "ExperimentalDestinationAtop", 
                    CompositeOperation[CompositeOperation.ExperimentalDestinationIn = 7] = "ExperimentalDestinationIn", 
                    CompositeOperation[CompositeOperation.DestinationOut = 8] = "DestinationOut", CompositeOperation[CompositeOperation.DestinationOver = 9] = "DestinationOver", 
                    CompositeOperation[CompositeOperation.Xor = 10] = "Xor";
                }(CompositeOperation = g.CompositeOperation || (g.CompositeOperation = {}));
            }(g || (g = {}));
            var g;
            !function(g) {
                var GlyphFactory = function() {
                    function GlyphFactory(fontFamily, fontSize, baselineHeight, fontColor, strokeWidth, strokeColor, strokeOnly, fontWeight) {
                        void 0 === baselineHeight && (baselineHeight = fontSize), void 0 === fontColor && (fontColor = "black"), 
                        void 0 === strokeWidth && (strokeWidth = 0), void 0 === strokeColor && (strokeColor = "black"), 
                        void 0 === strokeOnly && (strokeOnly = !1), void 0 === fontWeight && (fontWeight = g.FontWeight.Normal), 
                        this.fontFamily = fontFamily, this.fontSize = fontSize, this.fontWeight = fontWeight, 
                        this.baselineHeight = baselineHeight, this.fontColor = fontColor, this.strokeWidth = strokeWidth, 
                        this.strokeColor = strokeColor, this.strokeOnly = strokeOnly;
                    }
                    return GlyphFactory;
                }();
                g.GlyphFactory = GlyphFactory;
            }(g || (g = {}));
            var g;
            !function(g) {
                var LocalTickMode;
                !function(LocalTickMode) {
                    LocalTickMode[LocalTickMode.NonLocal = 0] = "NonLocal", LocalTickMode[LocalTickMode.FullLocal = 1] = "FullLocal", 
                    LocalTickMode[LocalTickMode.InterpolateLocal = 2] = "InterpolateLocal";
                }(LocalTickMode = g.LocalTickMode || (g.LocalTickMode = {}));
            }(g || (g = {}));
            var g;
            !function(g) {
                var NinePatchSurfaceEffector = function() {
                    function NinePatchSurfaceEffector(game, borderWidth) {
                        void 0 === borderWidth && (borderWidth = 4), this.game = game, "number" == typeof borderWidth ? this.borderWidth = {
                            top: borderWidth,
                            bottom: borderWidth,
                            left: borderWidth,
                            right: borderWidth
                        } : this.borderWidth = borderWidth;
                    }
                    return NinePatchSurfaceEffector.prototype.render = function(srcSurface, width, height) {
                        var isCreateSurface = !0;
                        this._surface && this._surface.width === width && this._surface.height === height && this._beforeSrcSurface === srcSurface ? isCreateSurface = !1 : (this._surface = this.game.resourceFactory.createSurface(Math.ceil(width), Math.ceil(height)), 
                        this._beforeSrcSurface = srcSurface);
                        var renderer = this._surface.renderer();
                        renderer.begin(), isCreateSurface || renderer.clear();
                        var sx1 = this.borderWidth.left, sx2 = srcSurface.width - this.borderWidth.right, sy1 = this.borderWidth.top, sy2 = srcSurface.height - this.borderWidth.bottom, dx1 = this.borderWidth.left, dx2 = width - this.borderWidth.right, dy1 = this.borderWidth.top, dy2 = height - this.borderWidth.bottom, srcCorners = [ {
                            x: 0,
                            y: 0,
                            width: this.borderWidth.left,
                            height: this.borderWidth.top
                        }, {
                            x: sx2,
                            y: 0,
                            width: this.borderWidth.right,
                            height: this.borderWidth.top
                        }, {
                            x: 0,
                            y: sy2,
                            width: this.borderWidth.left,
                            height: this.borderWidth.bottom
                        }, {
                            x: sx2,
                            y: sy2,
                            width: this.borderWidth.right,
                            height: this.borderWidth.bottom
                        } ], destCorners = [ {
                            x: 0,
                            y: 0
                        }, {
                            x: dx2,
                            y: 0
                        }, {
                            x: 0,
                            y: dy2
                        }, {
                            x: dx2,
                            y: dy2
                        } ], i = 0;
                        for (i = 0; i < srcCorners.length; ++i) {
                            var c = srcCorners[i];
                            renderer.save(), renderer.translate(destCorners[i].x, destCorners[i].y), renderer.drawImage(srcSurface, c.x, c.y, c.width, c.height, 0, 0), 
                            renderer.restore();
                        }
                        var srcBorders = [ {
                            x: sx1,
                            y: 0,
                            width: sx2 - sx1,
                            height: this.borderWidth.top
                        }, {
                            x: 0,
                            y: sy1,
                            width: this.borderWidth.left,
                            height: sy2 - sy1
                        }, {
                            x: sx2,
                            y: sy1,
                            width: this.borderWidth.right,
                            height: sy2 - sy1
                        }, {
                            x: sx1,
                            y: sy2,
                            width: sx2 - sx1,
                            height: this.borderWidth.bottom
                        } ], destBorders = [ {
                            x: dx1,
                            y: 0,
                            width: dx2 - dx1,
                            height: this.borderWidth.top
                        }, {
                            x: 0,
                            y: dy1,
                            width: this.borderWidth.left,
                            height: dy2 - dy1
                        }, {
                            x: dx2,
                            y: dy1,
                            width: this.borderWidth.right,
                            height: dy2 - dy1
                        }, {
                            x: dx1,
                            y: dy2,
                            width: dx2 - dx1,
                            height: this.borderWidth.bottom
                        } ];
                        for (i = 0; i < srcBorders.length; ++i) {
                            var s = srcBorders[i], d = destBorders[i];
                            renderer.save(), renderer.translate(d.x, d.y), renderer.transform([ d.width / s.width, 0, 0, d.height / s.height, 0, 0 ]), 
                            renderer.drawImage(srcSurface, s.x, s.y, s.width, s.height, 0, 0), renderer.restore();
                        }
                        var sw = sx2 - sx1, sh = sy2 - sy1, dw = dx2 - dx1, dh = dy2 - dy1;
                        return renderer.save(), renderer.translate(dx1, dy1), renderer.transform([ dw / sw, 0, 0, dh / sh, 0, 0 ]), 
                        renderer.drawImage(srcSurface, sx1, sy1, sw, sh, 0, 0), renderer.restore(), renderer.end(), 
                        this._surface;
                    }, NinePatchSurfaceEffector;
                }();
                g.NinePatchSurfaceEffector = NinePatchSurfaceEffector;
            }(g || (g = {}));
            var g;
            !function(g) {
                var PathUtil;
                !function(PathUtil) {
                    function resolvePath(base, path) {
                        function split(str) {
                            var ret = str.split("/");
                            return "" === ret[ret.length - 1] && ret.pop(), ret;
                        }
                        if ("" === path) return base;
                        for (var baseComponents = PathUtil.splitPath(base), parts = split(baseComponents.path).concat(split(path)), resolved = [], i = 0; i < parts.length; ++i) {
                            var part = parts[i];
                            switch (part) {
                              case "..":
                                var popped = resolved.pop();
                                if (void 0 === popped || "" === popped || "." === popped) throw g.ExceptionFactory.createAssertionError("PathUtil.resolvePath: invalid arguments");
                                break;

                              case ".":
                                0 === resolved.length && resolved.push(".");
                                break;

                              case "":
                                resolved = [ "" ];
                                break;

                              default:
                                resolved.push(part);
                            }
                        }
                        return baseComponents.host + resolved.join("/");
                    }
                    function resolveDirname(path) {
                        var index = path.lastIndexOf("/");
                        return -1 === index ? path : path.substr(0, index);
                    }
                    function resolveExtname(path) {
                        for (var i = path.length - 1; i >= 0; --i) {
                            var c = path.charAt(i);
                            if ("." === c) return path.substr(i);
                            if ("/" === c) return "";
                        }
                        return "";
                    }
                    function makeNodeModulePaths(path) {
                        var pathComponents = PathUtil.splitPath(path), host = pathComponents.host;
                        path = pathComponents.path, "/" === path[path.length - 1] && (path = path.slice(0, path.length - 1));
                        for (var parts = path.split("/"), firstDir = parts.indexOf("node_modules"), root = firstDir > 0 ? firstDir - 1 : 0, dirs = [], i = parts.length - 1; i >= root; --i) if ("node_modules" !== parts[i]) {
                            var dirParts = parts.slice(0, i + 1);
                            dirParts.push("node_modules");
                            var dir = dirParts.join("/");
                            dirs.push(host + dir);
                        }
                        return dirs;
                    }
                    function addExtname(path, ext) {
                        var index = path.indexOf("?");
                        return -1 === index ? path + "." + ext : path.substring(0, index) + "." + ext + path.substring(index, path.length);
                    }
                    function splitPath(path) {
                        var host = "", doubleSlashIndex = path.indexOf("//");
                        if (doubleSlashIndex >= 0) {
                            var hostSlashIndex = path.indexOf("/", doubleSlashIndex + 2);
                            hostSlashIndex >= 0 ? (host = path.slice(0, hostSlashIndex), path = path.slice(hostSlashIndex)) : (host = path, 
                            path = "/");
                        } else host = "";
                        return {
                            host: host,
                            path: path
                        };
                    }
                    PathUtil.resolvePath = resolvePath, PathUtil.resolveDirname = resolveDirname, PathUtil.resolveExtname = resolveExtname, 
                    PathUtil.makeNodeModulePaths = makeNodeModulePaths, PathUtil.addExtname = addExtname, 
                    PathUtil.splitPath = splitPath;
                }(PathUtil = g.PathUtil || (g.PathUtil = {}));
            }(g || (g = {}));
            var g;
            !function(g) {
                var FontFamily;
                !function(FontFamily) {
                    FontFamily[FontFamily.SansSerif = 0] = "SansSerif", FontFamily[FontFamily.Serif = 1] = "Serif", 
                    FontFamily[FontFamily.Monospace = 2] = "Monospace";
                }(FontFamily = g.FontFamily || (g.FontFamily = {}));
            }(g || (g = {}));
            var g;
            !function(g_1) {
                var BitmapFont = function() {
                    function BitmapFont(param) {
                        this.surface = g_1.Util.asSurface(param.src), this.map = param.map, this.defaultGlyphWidth = param.defaultGlyphWidth, 
                        this.defaultGlyphHeight = param.defaultGlyphHeight, this.missingGlyph = param.missingGlyph, 
                        this.size = param.defaultGlyphHeight;
                    }
                    return BitmapFont.prototype.glyphForCharacter = function(code) {
                        var g = this.map[code] || this.missingGlyph;
                        if (!g) return null;
                        var w = void 0 === g.width ? this.defaultGlyphWidth : g.width, h = void 0 === g.height ? this.defaultGlyphHeight : g.height, offsetX = g.offsetX || 0, offsetY = g.offsetY || 0, advanceWidth = void 0 === g.advanceWidth ? w : g.advanceWidth, surface = 0 === w || 0 === h ? void 0 : this.surface;
                        return new g_1.Glyph(code, g.x, g.y, w, h, offsetX, offsetY, advanceWidth, surface, !0);
                    }, BitmapFont.prototype.destroy = function() {
                        this.surface && !this.surface.destroyed() && this.surface.destroy(), this.map = void 0;
                    }, BitmapFont.prototype.destroyed = function() {
                        return !this.map;
                    }, BitmapFont;
                }();
                g_1.BitmapFont = BitmapFont;
            }(g || (g = {}));
            var g;
            !function(g) {
                var TextBaseline;
                !function(TextBaseline) {
                    TextBaseline[TextBaseline.Top = 0] = "Top", TextBaseline[TextBaseline.Middle = 1] = "Middle", 
                    TextBaseline[TextBaseline.Alphabetic = 2] = "Alphabetic", TextBaseline[TextBaseline.Bottom = 3] = "Bottom";
                }(TextBaseline = g.TextBaseline || (g.TextBaseline = {}));
                var SystemLabel = function(_super) {
                    function SystemLabel(param) {
                        var _this = _super.call(this, param) || this;
                        return _this.text = param.text, _this.fontSize = param.fontSize, _this.textAlign = "textAlign" in param ? param.textAlign : g.TextAlign.Left, 
                        _this.textBaseline = "textBaseline" in param ? param.textBaseline : TextBaseline.Alphabetic, 
                        _this.maxWidth = param.maxWidth, _this.textColor = "textColor" in param ? param.textColor : "black", 
                        _this.fontFamily = "fontFamily" in param ? param.fontFamily : g.FontFamily.SansSerif, 
                        _this.strokeWidth = "strokeWidth" in param ? param.strokeWidth : 0, _this.strokeColor = "strokeColor" in param ? param.strokeColor : "black", 
                        _this.strokeOnly = "strokeOnly" in param ? param.strokeOnly : !1, _this;
                    }
                    return __extends(SystemLabel, _super), SystemLabel.prototype.renderSelf = function(renderer, camera) {
                        if (this.text) {
                            var offsetX;
                            switch (this.textAlign) {
                              case g.TextAlign.Right:
                                offsetX = this.width;
                                break;

                              case g.TextAlign.Center:
                                offsetX = this.width / 2;
                                break;

                              default:
                                offsetX = 0;
                            }
                            renderer.drawSystemText(this.text, offsetX, 0, this.maxWidth, this.fontSize, this.textAlign, this.textBaseline, this.textColor, this.fontFamily, this.strokeWidth, this.strokeColor, this.strokeOnly);
                        }
                        return !0;
                    }, SystemLabel;
                }(g.E);
                g.SystemLabel = SystemLabel;
            }(g || (g = {}));
            var g;
            !function(g) {
                var TextAlign;
                !function(TextAlign) {
                    TextAlign[TextAlign.Left = 0] = "Left", TextAlign[TextAlign.Center = 1] = "Center", 
                    TextAlign[TextAlign.Right = 2] = "Right";
                }(TextAlign = g.TextAlign || (g.TextAlign = {}));
            }(g || (g = {}));
            var g;
            !function(g) {
                var TickGenerationMode;
                !function(TickGenerationMode) {
                    TickGenerationMode[TickGenerationMode.ByClock = 0] = "ByClock", TickGenerationMode[TickGenerationMode.Manual = 1] = "Manual";
                }(TickGenerationMode = g.TickGenerationMode || (g.TickGenerationMode = {}));
            }(g || (g = {}));
            var g;
            !function(g) {
                // Copyright (c) 2014 Andreas Madsen & Emil Bay
                // From https://github.com/AndreasMadsen/xorshift
                // https://github.com/AndreasMadsen/xorshift/blob/master/LICENSE.md
                // Arranged by DWANGO Co., Ltd.
                var Xorshift = function() {
                    function Xorshift(seed) {
                        this.initState(seed);
                    }
                    return Xorshift.deserialize = function(ser) {
                        var ret = new Xorshift(0);
                        return ret._state0U = ser._state0U, ret._state0L = ser._state0L, ret._state1U = ser._state1U, 
                        ret._state1L = ser._state1L, ret;
                    }, Xorshift.prototype.initState = function(seed) {
                        var factor = 1812433253;
                        seed = factor * (seed ^ seed >> 30) + 1, this._state0U = seed, seed = factor * (seed ^ seed >> 30) + 2, 
                        this._state0L = seed, seed = factor * (seed ^ seed >> 30) + 3, this._state1U = seed, 
                        seed = factor * (seed ^ seed >> 30) + 4, this._state1L = seed;
                    }, Xorshift.prototype.randomInt = function() {
                        var s1U = this._state0U, s1L = this._state0L, s0U = this._state1U, s0L = this._state1L;
                        this._state0U = s0U, this._state0L = s0L;
                        var t1U = 0, t1L = 0, t2U = 0, t2L = 0, a1 = 23, m1 = 4294967295 << 32 - a1;
                        t1U = s1U << a1 | (s1L & m1) >>> 32 - a1, t1L = s1L << a1, s1U ^= t1U, s1L ^= t1L, 
                        t1U = s1U ^ s0U, t1L = s1L ^ s0L;
                        var a2 = 17, m2 = 4294967295 >>> 32 - a2;
                        t2U = s1U >>> a2, t2L = s1L >>> a2 | (s1U & m2) << 32 - a2, t1U ^= t2U, t1L ^= t2L;
                        var a3 = 26, m3 = 4294967295 >>> 32 - a3;
                        t2U = s0U >>> a3, t2L = s0L >>> a3 | (s0U & m3) << 32 - a3, t1U ^= t2U, t1L ^= t2L, 
                        this._state1U = t1U, this._state1L = t1L;
                        var sumL = (t1L >>> 0) + (s0L >>> 0);
                        return t2U = t1U + s0U + (sumL / 2 >>> 31) >>> 0, t2L = sumL >>> 0, [ t2U, t2L ];
                    }, Xorshift.prototype.random = function() {
                        var t2 = this.randomInt();
                        return (4294967296 * t2[0] + t2[1]) / 0x10000000000000000;
                    }, Xorshift.prototype.nextInt = function(min, sup) {
                        return Math.floor(min + this.random() * (sup - min));
                    }, Xorshift.prototype.serialize = function() {
                        return {
                            _state0U: this._state0U,
                            _state0L: this._state0L,
                            _state1U: this._state1U,
                            _state1L: this._state1L
                        };
                    }, Xorshift;
                }();
                g.Xorshift = Xorshift;
            }(g || (g = {}));
            var g;
            !function(g) {
                var XorshiftRandomGenerator = function(_super) {
                    function XorshiftRandomGenerator(seed, xorshift) {
                        var _this = this;
                        if (void 0 === seed) throw g.ExceptionFactory.createAssertionError("XorshiftRandomGenerator#constructor: seed is undefined");
                        return _this = _super.call(this, seed) || this, xorshift ? _this._xorshift = g.Xorshift.deserialize(xorshift) : _this._xorshift = new g.Xorshift(seed), 
                        _this;
                    }
                    return __extends(XorshiftRandomGenerator, _super), XorshiftRandomGenerator.deserialize = function(ser) {
                        return new XorshiftRandomGenerator(ser._seed, ser._xorshift);
                    }, XorshiftRandomGenerator.prototype.get = function(min, max) {
                        return this._xorshift.nextInt(min, max + 1);
                    }, XorshiftRandomGenerator.prototype.serialize = function() {
                        return {
                            _seed: this.seed,
                            _xorshift: this._xorshift.serialize()
                        };
                    }, XorshiftRandomGenerator;
                }(g.RandomGenerator);
                g.XorshiftRandomGenerator = XorshiftRandomGenerator;
            }(g || (g = {}));
            var g;
            !function(g) {
                var ShaderProgram = function() {
                    function ShaderProgram(param) {
                        this.fragmentShader = param.fragmentShader, this.uniforms = param.uniforms;
                    }
                    return ShaderProgram;
                }();
                g.ShaderProgram = ShaderProgram;
            }(g || (g = {})), module.exports = g;
        }).call(this);
    }, {} ]
}, {}, []);