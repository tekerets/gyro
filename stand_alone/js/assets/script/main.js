window.gLocalAssetContainer["main"] = function(g) { (function(exports, require, module, __filename, __dirname) {
// このコード内には一部、以下のライセンス/著作権のコードを含みます
//// The MIT License (MIT)

//// Copyright (c) 2018 DWANGO Co., Ltd.

//// Permission is hereby granted, free of charge, to any person obtaining a copy
//// of this software and associated documentation files (the "Software"), to deal
//// in the Software without restriction, including without limitation the rights
//// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//// copies of the Software, and to permit persons to whom the Software is
//// furnished to do so, subject to the following conditions:

//// The above copyright notice and this permission notice shall be included in all
//// copies or substantial portions of the Software.

//// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//// SOFTWARE.


//具体的には

// var gameTimeLimit = 60;
// var frameCount = 0;
// var game_end_flag = 0;
// scene.message.add(function(msg){
//   if (msg.data && msg.data.type === "start" && msg.data.parameters && msg.data.parameters.totalTimeLimit){
//     gameTimeLimit = msg.data.parameters.totalTimeLimit - 7;
//   }
// })

// と、

// function updateHandler(){
//   ++frameCount;
//   time_remaining = Math.floor(gameTimeLimit - frameCount / g.game.fps ) + 1
//   if (time_remaining <= 0){
//     scene.update.remove(updateHandler);
//   }
//   var text = time_remaining.toString(10)
//   if (timerLabel.text != text){
//     timerLabel.text = text;
//     timerLabel.invalidate();
//   }
// }
// scene.update.add(updateHandler)

// と、

// function display_score_board(){
//   if (score_board_counter == 30){
//     if (typeof window !== "undefined" && window.RPGAtsumaru) {
//         window.RPGAtsumaru.experimental.scoreboards.setRecord(
//             1, // ボードIDとして固定で1を指定
//             g.game.vars.gameState.score // スコアを送信
//         ).then(function () {
//             // ok
//         }).catch(function (e) {
//             // ng
//         });
//     window.RPGAtsumaru.experimental.scoreboards.display(1)
//     }
//   }
// }

// という部分です。
// 該当部分はコメントでも示してあります。




//タイトルとルールを表示するシーン
function main(param){
  //素材読み込み
  var scene = new g.Scene({
    game: g.game,
    assetIds: [
      "title",
      "rule",
      "ohige3"
    ]
  });
  
  // if (typeof window !== "undefined" && window.RPGAtsumaru) {
  // }
  
  scene.loaded.add(function (){
    rule_time_counter = 0
    var title_board = new g.Sprite({
      scene: scene,
      src: scene.assets["title"]
    })
    var rule_board = new g.Sprite({
      scene: scene,
      src: scene.assets["rule"]
    })
    
    scene.append(title_board)
    scene.assets["ohige3"].play();
    rule_board.update.add(function(e) {
      rule_time_counter++
      if (rule_time_counter == 100){
        title_board.destroy()
	      scene.assets["ohige3"].stop();
        scene.append(rule_board)
      }
      if (rule_time_counter > 200){
        g.game.pushScene(main_game(param));
      }
    })
  })
  
	g.game.pushScene(scene);
}

//メインのゲームをプレイするシーン
function main_game(param) {
  //素材の読み込み
  var scene = new g.Scene({
    game: g.game,
    assetIds: [
      //システム系
      "font16_2",
      "glyph_area_16",
      
      //背景,主要なもの
      "gyro_sky",
      "gyro",
      "jerryfish_head",
      "jerryfish_8arms2",
      "jerryfish_9arms2",
      "ground",
      "gyro",
      "score",
      "timeup",
      
      //空を流れるもの
      "cloud",
      "plane",
      "bird",
      "himawari",
      "oumuamua",
      
      //地面を流れるもの
      "solar",
      "castle",
      "hospital",
      "drugstore",
      "aquarium",
      "tower",
      "carrot_field",
      "clocktower",
      "store",
      
      //特殊効果
      "scan",
      "laser",
      
      //アイテム
      "laser_pointer",
      "mri",
      "car",
      "baum",
      "cookie",
      "pie",
      "donut",
      "ice",
      "cake",
      
      //効果音&BGM
      "button06",
      "laputa2",
      "ohige3",
      "whistle",
      "laser5"
    ]
  });
  
  //以下の8行は Copyright (c) 2018 DWANGO Co., Ltd./MITライセンス なコードの改変です。詳しくはLICENCE.txtを御覧ください。
  var gameTimeLimit = 60;
  var frameCount = 0;
  var game_end_flag = 0;
  scene.message.add(function(msg){
    if (msg.data && msg.data.type === "start" && msg.data.parameters && msg.data.parameters.totalTimeLimit){
      gameTimeLimit = msg.data.parameters.totalTimeLimit - 7;
    }
  })
  //ここまで Copyright (c) 2018 DWANGO Co., Ltd./MITライセンス の改変です。
  
  scene.loaded.add(function(){
    var gyro_sky = new g.Sprite({
      scene: scene,
      src: scene.assets["gyro_sky"]
    })
    var ground = new g.Sprite({
      scene: scene,
      src: scene.assets["ground"]
    })
    var jerryfish_hands1 = new g.Sprite({
      scene: scene,
      src: scene.assets["jerryfish_9arms2"],
      x: 230,
      y: -5
    })
    var jerryfish_hands2 = new g.Sprite({
      scene: scene,
      src: scene.assets["jerryfish_8arms2"],
      x: 230,
      y: -5
    })
    var jerryfish_head = new g.Sprite({
      scene: scene,
      src: scene.assets["jerryfish_head"],
      scaleX: 0.8,
      scaleY: 0.8,
      x: 254,
      y: 31
    })
    var gyro = new g.Sprite({
      scene: scene,
      src: scene.assets["gyro"],
      x: 10,
      y: 10,
      scaleX: 0.9,
      scaleY: 0.9
    })
      
    //タイムカウント用のフォントの設定
    var glyph = JSON.parse(scene.assets["glyph_area_16"].data);
    var font = new g.BitmapFont({
      src: scene.assets["font16_2"],
      map: glyph,
      defaultGlyphWidth: 16,
      defaultGlyphHeight: 16
    })
    var timerLabel = new g.Label({
      scene: scene,
      font: font,
      fontSize: 16,
      text: "",
      x: 2,
      y: 2
      // y: 344
    })
    
    //もろもろの汚い残骸
		var time_counter = 0
		var sky_flow_counter = 1
		var before_before_ground_item_number = -1
		var before_ground_item_number = -1
		var ground_item_number = -1
    
    var point = 0
    var pointLabel = new g.Label({
      scene: scene,
      font: font,
      fontSize: 16,
      text: "",
      x: 2,
      y: 342
      // y: 344
    })
    var plus_pointLabel = new g.Label({
      scene: scene,
      font: font,
      fontSize: 16,
      text: "",
      x: 2,
      y: 324
      // y: 344
    })
    
    scene.assets["ohige3"].play();
    
    //経過時間を数える処理
    var game_end_time = 0
    //以下の13行は Copyright (c) 2018 DWANGO Co., Ltd./MITライセンス なコードの改変です。詳しくはLICENCE.txtを御覧ください。
    function updateHandler(){
      ++frameCount;
      time_remaining = Math.floor(gameTimeLimit - frameCount / g.game.fps ) + 1
      if (time_remaining <= 0){
        scene.update.remove(updateHandler);
      }
      var text = time_remaining.toString(10)
      if (timerLabel.text != text){
        timerLabel.text = text;
        timerLabel.invalidate();
      }
    }
    scene.update.add(updateHandler)
    //ここまで Copyright (c) 2018 DWANGO Co., Ltd./MITライセンス の改変です。
    
    //全体の流れ
    //ゲーム開始から終了までの時間
    var game_end_time = 60;
    function flow(){
		  //クラゲの回転処理
		  rotate_jerryfish()
		  //空に物を流す処理
  		if (time_counter % 300 == 0){
  		  if (sky_flow_counter % 3 == 2){
  		    // n = Math.floor(Math.random()*4);
  		    n = g.game.random.get(0, 3)
  		    // n = 3;
      		eval(sky_flow_array[n])();
  		  }
  		  else {
  		    flow_cloud()
  		  }
  		  sky_flow_counter++
  		}
      //地面に物を流す処理
		  if (time_counter % 200 == 0){
		    to_ground_flow_random()
  	  	eval(ground_flow_array[ground_item_number])();
		  }
		  
		  //レーザーポインタを流すタイミング
		  if (time_counter == 30 * 10){
		    flow_laser_pointer()
		  }
		  //mriを流すタイミング
		  if (time_counter == 30 * 19){
		    flow_mri()
		  }
		  //車を流すタイミング
		  if ((time_counter + 60) % 165 == 0){
		    flow_car()
		  }
		  
		  //アイテムを流すタイミング
		  if ((time_counter + 0) % 60 == 0){
		    flow_cookie()
		  }
		  if ((time_counter + 23) % 60 == 0 && time_counter > 250){
		    flow_baum()
		  }
		  if ((time_counter + 46) % 60 == 0 && time_counter > 500){
		    flow_pie()
		  }
		  if ((time_counter + 35) % 60 == 0 && time_counter > 750){
		    flow_donut()
		  }
		  if ((time_counter + 58) % 60 == 0 && time_counter > 1000){
		    flow_ice()
		  }
		  if ((time_counter + 70) % 98 == 0 && time_counter > 1220){
		    flow_cake()
		  }
		  
      
      //ニコニコ新市場用コード
      g.game.vars.gameState = { score: 0 };
      g.game.vars.gameState.score = point;
      g.game.vars.gameState.playThreshold = 1000;
		  
		  //ゲーム終了に関わるタイミング
		  if (time_counter == 30 * game_end_time){
        scene.assets["ohige3"].stop();
        game_end_flag = 1
        scene.assets["whistle"].play();
		    flow_timeup()
		  }
		  if (time_counter == 30 * (game_end_time) + 105 ){
        g.game.replaceScene(score_scene(point));
		  }
		  
		  time_counter++
		  re_draw()
    }
    scene.update.add(flow)
    
    function point_add(add_point){
      point = point + add_point
      plus_point(add_point)
    }
    
    //合計ポイントの表示
    function pointcalc(){
      pointLabel.text = point + "pt";
      pointLabel.invalidate();
    }
    scene.update.add(pointcalc)
    
    //獲得アイテムのポイント表示
    function plus_point(add_point){
      var plus_pointLabel = new g.Label({
        scene: scene,
        font: font,
        fontSize: 16,
        text: "",
        x: 2,
        y: 324
      })
      plus_pointLabel.text =  "+" + String(add_point) + "pt"
      scene.append(plus_pointLabel)
      plus_pointLabel.tag ={
        counter: 0
      } 
      plus_pointLabel.update.add(function(e){
        plus_pointLabel.tag.counter++
        plus_pointLabel.opacity = (30 - (plus_pointLabel.tag.counter))/30
        plus_pointLabel.invalidate();
        if(plus_pointLabel.tag.counter > 30){
          plus_pointLabel.destroy()
        }
      })
    }
    
    scene.append(gyro_sky);
    scene.append(ground);
    
    //クラゲの回転
		function rotate_jerryfish(){
			jerryfish_hands1.angle = jerryfish_hands1.angle - 2;
			jerryfish_hands2.angle = jerryfish_hands2.angle + 2;
			jerryfish_hands1.modified();
			jerryfish_hands2.modified();
			jerryfish_head.modified();
		}  
    
    //空を流れるものら
		function flow_himawari(){
      var himawari = new g.Sprite({
        scene: scene,
        src: scene.assets["himawari"],
        scaleX: 0.7,
        scaleY: 0.7,
        x: 650,
        y: -5
      })
      scene.append(himawari)
  		himawari.update.add(function(e){
  		  himawari.x--;
  		  himawari.modified();
  		  if(himawari.x < -100){
  		    himawari.destroy()
  		  }
  		})
		}
		function flow_cloud(){
		  var cloud = new g.Sprite({
        scene: scene,
        src: scene.assets["cloud"],
        x: 650,
        y: 0
      })
      scene.append(cloud)
  		cloud.update.add(function(e){
  		  cloud.x--;
  		  cloud.modified();
  		  if(cloud.x < -100){
  		    cloud.destroy()
  		  }
  		})
		}
		function flow_plane(){
		  var plane = new g.Sprite({
        scene: scene,
        src: scene.assets["plane"],
        x: 650,
        y: -50,
        scaleX: 0.6
      })
      scene.append(plane)
      counter = 0
  		plane.update.add(function(e){
  		  plane.x--;
  		  plane.y = plane.y + 0.07
  		  plane.modified();
  		  if(plane.x < -100){
  		    plane.destroy()
  		  }
  		})
		}
		function flow_bird(){
		  var bird = new g.Sprite({
        scene: scene,
        src: scene.assets["bird"],
        x: 650,
        y: -10,
        scaleX: 0.8,
        scaleY: 0.8
      })
      scene.append(bird)
      counter = 0
  		bird.update.add(function(e){
  		  bird.x--;
  		  // bird.y = bird.y + 0.07
  		  bird.modified();
  		  if(bird.x < -100){
  		    bird.destroy()
  		  }
  		})
		}
		function flow_oumuamua(){
		  var oumuamua = new g.Sprite({
        scene: scene,
        src: scene.assets["oumuamua"],
        x: 650,
        y: -60,
        scaleX: 0.2,
        scaleY: 0.2
      })
      scene.append(oumuamua)
      counter = 0
  		oumuamua.update.add(function(e){
  		  oumuamua.x--;
  		  oumuamua.y = oumuamua.y + 0.07
  		  // bird.y = bird.y + 0.07
  		  oumuamua.modified();
  		  if(oumuamua.x < -100){
  		    oumuamua.destroy()
  		  }
  		})
		}
		sky_flow_array = ["flow_himawari","flow_plane","flow_bird","flow_oumuamua"];
		
    //地面を流れるものら
		function flow_solar(){
      var solar = new g.Sprite({
        scene: scene,
        src: scene.assets["solar"],
        scaleX: 1.1,
        scaleY: 1.1,
        x: 650,
        y: 260
      })
      scene.append(solar)
  		solar.update.add(function(e){
  		  solar.x--;
  		  solar.x--;
  		  solar.modified();
  		  if(solar.x < -150){
  		    solar.destroy()
  		  }
  		})
		}
		function flow_store(){
      var store = new g.Sprite({
        scene: scene,
        src: scene.assets["store"],
        scaleX: 0.7,
        scaleY: 0.7,
        x: 670,
        y: 210
      })
      scene.append(store)
  		store.update.add(function(e){
  		  store.x--;
  		  store.x--;
  		  store.modified();
  		  if(store.x < -150){
  		    store.destroy()
  		  }
  		})
		}
		function flow_drugstore(){
      var drugstore = new g.Sprite({
        scene: scene,
        src: scene.assets["drugstore"],
        scaleX: 0.9,
        scaleY: 0.9,
        x: 670,
        y: 260
      })
      scene.append(drugstore)
  		drugstore.update.add(function(e){
  		  drugstore.x--;
  		  drugstore.x--;
  		  drugstore.modified();
  		  if(drugstore.x < -150){
  		    drugstore.destroy()
  		  }
  		})
		}
		function flow_castle(){
      var castle = new g.Sprite({
        scene: scene,
        src: scene.assets["castle"],
        scaleX: 0.9,
        scaleY: 0.9,
        x: 670,
        y: 260
      })
      scene.append(castle)
  		castle.update.add(function(e){
  		  castle.x--;
  		  castle.x--;
  		  castle.modified();
  		  if(castle.x < -150){
  		    castle.destroy()
  		  }
  		})
		}
		function flow_hospital(){
      var hospital = new g.Sprite({
        scene: scene,
        src: scene.assets["hospital"],
        scaleX: 0.9,
        scaleY: 0.9,
        x: 670,
        y: 260
      })
      scene.append(hospital)
  		hospital.update.add(function(e){
  		  hospital.x--;
  		  hospital.x--;
  		  hospital.modified();
  		  if(hospital.x < -150){
  		    hospital.destroy()
  		  }
  		})
		}
		function flow_tower(){
      var tower = new g.Sprite({
        scene: scene,
        src: scene.assets["tower"],
        scaleX: 0.5,
        scaleY: 0.5,
        x: 670,
        y: 213
      })
      scene.append(tower)
  		tower.update.add(function(e){
  		  tower.x--;
  		  tower.x--;
  		  tower.modified();
  		  if(tower.x < -150){
  		    tower.destroy()
  		  }
  		})
		}
		function flow_aquarium(){
      var aquarium = new g.Sprite({
        scene: scene,
        src: scene.assets["aquarium"],
        scaleX: 0.6,
        scaleY: 0.6,
        x: 670,
        y: 215
      })
      scene.append(aquarium)
  		aquarium.update.add(function(e){
  		  aquarium.x--;
  		  aquarium.x--;
  		  aquarium.modified();
  		  if(aquarium.x < -150){
  		    aquarium.destroy()
  		  }
  		})
		}
		function flow_carrot_field(){
      var carrot_field = new g.Sprite({
        scene: scene,
        src: scene.assets["carrot_field"],
        scaleX: 1,
        scaleY: 0.7,
        x: 670,
        y: 205
      })
      scene.append(carrot_field)
  		carrot_field.update.add(function(e){
  		  carrot_field.x--;
  		  carrot_field.x--;
  		  carrot_field.modified();
  		  if(carrot_field.x < -150){
  		    carrot_field.destroy()
  		  }
  		})
		}
		function flow_clocktower(){
      var clocktower = new g.Sprite({
        scene: scene,
        src: scene.assets["clocktower"],
        scaleX: 0.5,
        scaleY: 0.5,
        x: 670,
        y: 213
      })
      scene.append(clocktower)
  		clocktower.update.add(function(e){
  		  clocktower.x--;
  		  clocktower.x--;
  		  clocktower.modified();
  		  if(clocktower.x < -150){
  		    clocktower.destroy()
  		  }
  		})
		}
		ground_flow_array = ["flow_solar","flow_store","flow_castle","flow_drugstore","flow_hospital","flow_tower","flow_aquarium","flow_carrot_field","flow_clocktower"];
		
		//アイテムの出現座標の設定
	  function get_item_appear_point(){
      item_appear_x = jerryfish_head.x + 50
	    item_appear_y = Math.floor(g.game.random.get(0, 200)) -10;
	  }
	  
		//MRIの処理
		mri_point = 2000
		function flow_mri(){
		  get_item_appear_point()
      var mri = new g.Sprite({
        scene: scene,
        src: scene.assets["mri"],
        scaleX: 0.1,
        scaleY: 0.1,
        x: item_appear_x,
        y: item_appear_y
      })
  		function flow_scan() {
        var scan = new g.Sprite({
          scene: scene,
          src: scene.assets["scan"]
        })
        scan.moveTo(-360, 0)
    		scan.update.add(function(e){
    		  scan.x = scan.x + 12;
    		  scene.append(scan)
    		})
  		      // scene.assets["laputa"].play();
  		      scene.assets["laputa2"].play();
  		}
  		function scan_jerryfish(){
  		  scan_counter = 0
  		  jerryfish_hands1.update.add(function(e){
          if (jerryfish_hands1.opacity >= 0.3){
    		    if(scan_counter > 50){
      		    jerryfish_hands1.opacity = ((jerryfish_hands1.opacity * 100) - 2) / 100
      		    jerryfish_hands2.opacity = ((jerryfish_hands1.opacity * 100) - 2) / 100
    		    }
          }
          scan_counter++
  		  })
  		}
      scene.append(mri);
      
  		mri.tag = {
  		  counter: 0
  		}
  		mri.update.add(function(e){
  		  mri.tag.counter++;
  		  if (mri.tag.counter < 30){
  		    mri.scaleX = mri.scaleX + 0.02
  		    mri.scaleY = mri.scaleY + 0.02
  		    mri.modified()
  		  }
  		  if (mri.tag.counter > 30){
  		    mri.x--
  		    mri.x--
  		    mri.modified()
  		  }
  		  if (mri.x < -150){
  		    mri.destroy()
  		  }
  		  
  		  // gyro(function(entity){
  		    // if(g.Collision.intersectAreas(catfish, gyro) && catfish.x < 90){
  		    if( ( (gyro.y - mri.y) < 60 && (gyro.y - mri.y) > -20 ) && mri.x < 55 && mri.x > 40 && game_end_flag == 0){
  		      mri.destroy()
  		      scene.assets["button06"].play();
  		      flow_scan()
  		      scan_jerryfish()
  		      point_add(mri_point)
  		    }
  		  // })
  		})
		}
		//レーザーポインターの処理
		laser_pointer_point = 2000
		function flow_laser_pointer(){
		  get_item_appear_point()
      var laser_pointer = new g.Sprite({
        scene: scene,
        src: scene.assets["laser_pointer"],
        scaleX: 0.1,
        scaleY: 0.1,
        x: item_appear_x,
        y: item_appear_y
      })
  		function shot_laser(){
        var laser = new g.Sprite({
          scene: scene,
          src: scene.assets["laser"]
        })
  		  laser.moveTo(-460, -50)
  		  laser_times = 0
  		  laser_limit_times = 5
  		  laser.update.add(function(e){
    		  scene.append(laser)
    		  
    		  if(laser_times <= laser_limit_times && laser_times >= 0){
    		  laser.x = laser.x + 110
    		  
      		  if(laser.x > 660){
  		        // scene.assets["shoot5"].play();
  		        scene.assets["laser5"].play();
      		    laser.moveTo(-360, -50)
      		    laser_times++
      		  }
      		  if (laser_times == laser_limit_times){
      		    laser_times = -1
      		  }
    		  }
  		  })
  		}
  		function jerryfish_back(){
  		  back_counter = 0
  		  jerryfish_head.update.add(function(e){
    		  if (back_counter < 55){
    		    if (back_counter > 6){
      		    jerryfish_hands1.x = jerryfish_hands1.x + 5
      		    jerryfish_hands2.x = jerryfish_hands2.x + 5
      		    jerryfish_head.x = jerryfish_head.x + 5
    		    }
    		    back_counter++
    		  }
  		  })
  		}
      scene.append(laser_pointer);
  		laser_pointer.tag = {
  		  counter: 0
  		}
  		laser_pointer.update.add(function(e){
  		  laser_pointer.tag.counter++;
  		  if (laser_pointer.tag.counter < 30){
  		    laser_pointer.scaleX = laser_pointer.scaleX + 0.02
  		    laser_pointer.scaleY = laser_pointer.scaleY + 0.02
  		    laser_pointer.modified()
  		  }
  		  if (laser_pointer.tag.counter > 30){
  		    laser_pointer.x--
  		    laser_pointer.x--
  		    laser_pointer.modified()
  		  }
  		  if (laser_pointer.x < -150){
  		    laser_pointer.destroy()
  		  }
  		  
  		    if( ( (gyro.y - laser_pointer.y) < 50 && (gyro.y - laser_pointer.y) > -35 ) && laser_pointer.x < 55 && laser_pointer.x > 40 && gyro.x > -3 && game_end_flag == 0){
  		      shot_laser()
  		      laser_pointer.destroy()
  		      scene.assets["button06"].play();
  		      point_add(laser_pointer_point)
  		      jerryfish_back()
  		    }
  		})
		}
		//車の処理
    function flow_car() {
		  get_item_appear_point()
      var car = new g.Sprite({
        scene: scene,
        src: scene.assets["car"],
        scaleX: 0.1,
        scaleY: 0.1,
        x: item_appear_x,
        y: item_appear_y
      })
      scene.append(car);
		  car.tag = { counter: 0 }
		  car.update.add(function(e){
		  car.tag.counter++;
		  if (car.tag.counter < 30){
		    car.scaleX = car.scaleX + 0.02
		    car.scaleY = car.scaleY + 0.02
		    car.modified()
		  }
		  if (car.tag.counter > 30){
		    car.x--
		    car.x--
		    car.modified()
		  }
		  if (car.x < -150){
		    car.destroy()
		  }
		  
	    if( ( (gyro.y - car.y) < 60 && (gyro.y - car.y) > -40 ) && car.x < 65 && car.x > 55 && game_end_flag == 0){
	      gyro.tag.car = 1
	     //car.destroy()
	    }
		})
    }
		//クッキーの処理
		cookie_point = 200
		function flow_cookie(){
      get_item_appear_point()
      var cookie = new g.Sprite({
        scene: scene,
        src: scene.assets["cookie"],
        scaleX: 0.01,
        scaleY: 0.01,
        x: item_appear_x,
        y: item_appear_y
      })
  		cookie.tag = {
  		  counter: 0
  		}
      scene.append(cookie);
  		cookie.update.add(function(e){
  		  cookie.tag.counter++;
  		  if (cookie.tag.counter < 30){
  		    cookie.scaleX = cookie.scaleX + 0.01
  		    cookie.scaleY = cookie.scaleY + 0.01
  		    cookie.modified()
  		  }
  		  if (cookie.tag.counter > 30){
  		    cookie.x--
  		    cookie.x--
  		    cookie.modified()
  		  }
  		  if (cookie.x < -150){
  		    cookie.destroy()
  		  }
  		  item_collision(cookie, cookie_point, -20, 60, 40, 50)
  		})
		}
		//アイスの処理
		ice_point = 400
		function flow_ice(){
		  get_item_appear_point()
      var ice = new g.Sprite({
        scene: scene,
        src: scene.assets["ice"],
        scaleX: 0.01,
        scaleY: 0.01,
        // x: 330,
        x: item_appear_x,
        y: item_appear_y
      })
  		ice.tag = {
  		  counter: 0
  		}
      scene.append(ice);
  		ice.update.add(function(e){
  		  ice.tag.counter++;
  		  if (ice.tag.counter < 30){
  		    ice.scaleX = ice.scaleX + 0.018
  		    ice.scaleY = ice.scaleY + 0.018
  		    ice.modified()
  		  }
  		  if (ice.tag.counter > 30){
  		    ice.x--
  		    ice.x--
  		    ice.modified()
  		  }
  		  if (ice.x < -150){
  		    ice.destroy()
  		  }
  		  item_collision(ice, ice_point, -60, 25, 65, 75)
  		})
		}
		//ドーナツの処理
		donut_point = 600
		function flow_donut(){
		  get_item_appear_point()
      var donut = new g.Sprite({
        scene: scene,
        src: scene.assets["donut"],
        scaleX: 0.01,
        scaleY: 0.01,
        // x: 330,
        x: item_appear_x,
        y: item_appear_y
      })
  		donut.tag = {
  		  counter: 0
  		}
      scene.append(donut);
  		donut.update.add(function(e){
  		  donut.tag.counter++;
  		  if (donut.tag.counter < 30){
  		    donut.scaleX = donut.scaleX + 0.018
  		    donut.scaleY = donut.scaleY + 0.018
  		    donut.modified()
  		  }
  		  if (donut.tag.counter > 30){
  		    donut.x--
  		    donut.x--
  		    donut.modified()
  		  }
  		  if (donut.x < -150){
  		    donut.destroy()
  		  }
  		  item_collision(donut, donut_point, -40, 30, 55, 80)
  		})
		}
		//バームクーヘンの処理
		baum_point = 1000
		function flow_baum(){
		  get_item_appear_point()
      var baum = new g.Sprite({
        scene: scene,
        src: scene.assets["baum"],
        scaleX: 0.1,
        scaleY: 0.1,
        x: item_appear_x,
        y: item_appear_y
      })
  		baum.tag = {
  		  counter: 0
  		}
      scene.append(baum);
  		baum.update.add(function(e){
  		  baum.tag.counter++;
  		  if (baum.tag.counter < 30){
  		    baum.scaleX = baum.scaleX + 0.01
  		    baum.scaleY = baum.scaleY + 0.01
  		    baum.modified()
  		  }
  		  if (baum.tag.counter > 30){
  		    baum.x--
  		    baum.x--
  		    baum.modified()
  		  }
  		  if (baum.x < -150){
  		    baum.destroy()
  		  }
  		  item_collision(baum, baum_point, -30, 55, 45, 55)
  		})
		}
		//パイの処理
		pie_point = 2000
		function flow_pie(){
		  get_item_appear_point()
      var pie = new g.Sprite({
        scene: scene,
        src: scene.assets["pie"],
        scaleX: 0.01,
        scaleY: 0.01,
        // x: 330,
        x: item_appear_x,
        y: item_appear_y
      })
  		pie.tag = {
  		  counter: 0
  		}
      scene.append(pie);
  		pie.update.add(function(e){
  		  pie.tag.counter++;
  		  if (pie.tag.counter < 30){
  		    pie.scaleX = pie.scaleX + 0.018
  		    pie.scaleY = pie.scaleY + 0.018
  		    pie.modified()
  		  }
  		  if (pie.tag.counter > 30){
  		    pie.x--
  		    pie.x--
  		    pie.modified()
  		  }
  		  if (pie.x < -150){
  		    pie.destroy()
  		  }
  		  
  		  item_collision(pie, pie_point, -55, 30, 67, 77)
  		})
		}
		//ケーキの処理
		cake_point = 3000
		function flow_cake(){
		  get_item_appear_point()
      var cake = new g.Sprite({
        scene: scene,
        src: scene.assets["cake"],
        scaleX: 0.01,
        scaleY: 0.01,
        // x: 330,
        x: item_appear_x,
        y: item_appear_y
      })
  		cake.tag = {
  		  counter: 0
  		}
      scene.append(cake);
  		cake.update.add(function(e){
  		  cake.tag.counter++;
  		  if (cake.tag.counter < 30){
  		    cake.scaleX = cake.scaleX + 0.012
  		    cake.scaleY = cake.scaleY + 0.012
  		    cake.modified()
  		  }
  		  if (cake.tag.counter > 30){
  		    cake.x--
  		    cake.x--
  		    cake.modified()
  		  }
  		  if (cake.x < -150){
  		    cake.destroy()
  		  }
  		  
  		  item_collision(cake, cake_point, -20, 55, 40, 55)
  		})
		}
    
    //当たり判定処理
    function item_collision(item, item_point, y1, y2, x1, x2){
	    if( ( (gyro.y - item.y) > y1 && (gyro.y - item.y) < y2) && item.x > x1 && item.x < x2 && gyro.x > -3 && game_end_flag == 0){
	      item.destroy()
	      scene.assets["button06"].play();
	      point_add(item_point)
	      plus_point(item_point)
	    }
    }
    
		//タイムアップの文字を流す処理
		function flow_timeup(){
      var timeup = new g.Sprite({
        scene: scene,
        src: scene.assets["timeup"],
        x: 630,
        y: 0
      })
      scene.append(timeup)
  		timeup.update.add(function(e){
  		  timeup.x = timeup.x -28
  		  scene.append(timeup)
  		})
		}
    
		//表示順序(重なりの順序)の都合で再描写する処理
		function re_draw(){
  	  scene.append(jerryfish_hands1)
  	  scene.append(jerryfish_hands2)
  	  scene.append(jerryfish_head)
  	  scene.append(gyro)
  	  scene.append(timerLabel)
  	  scene.append(pointLabel)
  	  scene.append(plus_pointLabel)
		}

		//地面を流れるものが連続しない(連続した3つの地表物が必ず違うものになる)ようにする処理
		function to_ground_flow_random(){
	    if (ground_item_number > -1){
	      if(before_ground_item_number > -1){
  		    before_before_ground_item_number = before_ground_item_number
  		    before_ground_item_number = ground_item_number
  		    ground_item_number = g.game.random.get(0, ground_flow_array.length -1)
	      }
	      else {
  		    before_ground_item_number = ground_item_number
  		    ground_item_number = g.game.random.get(0, ground_flow_array.length -1)
	      }
	    }
	    else {
		    ground_item_number = g.game.random.get(0, ground_flow_array.length -1)
	    }
	    if (ground_item_number > -1){
		    if(ground_item_number == before_ground_item_number || ground_item_number == before_before_ground_item_number){
          var ground_item_number_array = new Array(ground_flow_array.length);
          var array_number = 1;
          for (var i = 0; i < ground_item_number_array.length; i++){
          	ground_item_number_array[i] = array_number;
          	array_number++;
          }
          if(before_before_ground_item_number > -1){
            if(before_before_ground_item_number < before_ground_item_number){
    		      ground_item_number_array.splice(before_ground_item_number, 1);
    		      ground_item_number_array.splice(before_before_ground_item_number, 1);
            }
            else {
    		      ground_item_number_array.splice(before_before_ground_item_number, 1);
    		      ground_item_number_array.splice(before_ground_item_number, 1);
            }
          }
		      ground_item_number_array.splice(before_ground_item_number, 1);
          ground_item_number = ground_item_number_array[g.game.random.get(0, ground_flow_array.length -1)];
		    }
	    }
		}
		
		gyro.tag = {
		  car: 0
		}
		var car_counter = 0
		var gyro_situation = "high"
    gyro.update.add(function(e){
      if (gyro.tag.car == 1 ){
        if(car_counter < 90){
        gyro.x = gyro.x - 2
        car_counter++
        }
        if(car_counter >= 90){
          gyro.x = gyro.x + 2
        car_counter++
        }
        if(car_counter > 180){
          car_counter=0
          gyro.tag.car = 0
        }
      }
      if (gyro.tag.car == 0){
        //オートジャイロが最上部に留まっている状況での処理
        if (gyro_situation == "high"){
          scene.pointDownCapture.add(function() {
            gyro_situation = "downing"
          })
        }
        //オートジャイロが下がっている最中の処理
        if (gyro_situation == "downing"){
          gyro.y = gyro.y + 2.3
          if(gyro.y > 180){
            gyro_situation = "low"
          }
        }
        //オートジャイロが上がっている最中の処理
        if (gyro_situation == "upping"){
          gyro.y = gyro.y - 2.3
          if(gyro.y < 10){
            gyro_situation = "high"
          }
        }
        //オートジャイロが最下部に留まっている状況での処理
        if (gyro_situation == "low"){
          scene.pointDownCapture.add(function() {
            gyro_situation = "upping"
          })
        }
        
      }
      gyro.modified()
    });
		
  })
	g.game.pushScene(scene);
	return scene
}

//スコアを表示するシーン
function score_scene(point){
  var scene = new g.Scene({
    game: g.game,
    assetIds: [
      "score",
      "my_font2",
      "my_font_data"
    ]
  });
  
  scene.loaded.add(function () {
    var score_board_counter =  0
    var glyph2 = JSON.parse(scene.assets["my_font_data"].data);
    var font2 = new g.BitmapFont({
      src: scene.assets["my_font2"],
      map: glyph2,
      defaultGlyphWidth: 57,
      defaultGlyphHeight: 72
    })
      
    var pointLabel = new g.Label({
      scene: scene,
      font: font2,
      fontSize: 60,
      text: "",
      x: 80,
      y: 150
    })
    
    var score_board = new g.Sprite({
      scene: scene,
      src: scene.assets["score"],
      x:0,
      y:0
    })
    scene.append(score_board);
      
    function pointcalc(){
      pointLabel.text = point + "pt";
      pointLabel.invalidate();
    }
    
    //RPGアツマールのスコアボード機能を利用するための処理
    //以下の15行は Copyright (c) 2018 DWANGO Co., Ltd./MITライセンス なコードの改変です。詳しくはLICENCE.txtを御覧ください。
    function display_score_board(){
      if (score_board_counter == 30){
        if (typeof window !== "undefined" && window.RPGAtsumaru) {
            window.RPGAtsumaru.experimental.scoreboards.setRecord(
                1, // ボードIDとして固定で1を指定
                g.game.vars.gameState.score // スコアを送信
            ).then(function () {
                // ok
            }).catch(function (e) {
                // ng
            });
        window.RPGAtsumaru.experimental.scoreboards.display(1)
        }
      }
    }
    //ここまで Copyright (c) 2018 DWANGO Co., Ltd./MITライセンス なコードの改変です。
    
    function score_board_counter_plus(){
      score_board_counter++
      display_score_board()
    }
    scene.update.add(score_board_counter_plus)
    
    scene.update.add(pointcalc)
  	scene.append(pointLabel)
  });
  return scene;
}

module.exports = main;
})(g.module.exports, g.module.require, g.module, g.filename, g.dirname);
}